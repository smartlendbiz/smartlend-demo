<table class="table table-sm table-bordered dataTable">
	<thead>
		<tr>
			<th>Date</th>
			<th>Member</th>
			<th>Performed By</th>
			<th>Amount</th>
			<th>Type</th>
			<th>Method</th>
			<th>Notes</th>
			@if($page=='all')
			<th>Status</th>
			@endif
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($balance_lists as $balance)
		<tr>
			<td>{{ $balance->created_at }}</td>
			<td>{{ $balance->user->name }}</td>
			<td>{{ $balance->performedBy->name }}</td>
			<td>{{ $balance->amount }}</td>
			<td>{{ $balance->typeLabel() }}</td>
			<td>{{ $balance->method }}</td>
			<td>{{ $balance->notes }}</td>
			@if($page=='all')
			<td>{{ $balance->status }}</td>
			@endif			
			<td>
				@if(in_array($balance->status, ['Pending']))
					{!! Form::open(['url' => '/cash_transactions/'.$balance->id."?hash=$page&action=confirmed", 'method' => 'PATCH', 'class' => 'd-inline']) !!}
					<button type="button" class="btn btn-xs btn-success" onclick="confirm($(this).parent(), 'Confirm')">Confirm</button>
					{!! Form::close() !!}
				@endif
				@if(in_array($balance->status, ['Pending']))			
					{!! Form::open(['url' => '/cash_transactions/'.$balance->id."?hash=$page&action=declined", 'method' => 'PATCH', 'class' => 'd-inline']) !!}
					<button type="button" class="btn btn-xs btn-warning" onclick="confirm($(this).parent(), 'Decline')">Decline</button>
					{!! Form::close() !!}
				@endif

				@if(in_array($balance->status, ['Pending', 'Declined', 'Confirmed']))
					{!! Form::open(['url' => '/cash_transactions/'.$balance->id."?hash=$page", 'method' => 'DELETE', 'class' => 'd-inline']) !!}
					<button type="button" class="btn btn-xs btn-danger" onclick="deleteConfirm($(this).parent())">Delete</button>
					{!! Form::close() !!}
				@endif							
			</td>
		</tr>
		@endforeach
	</tbody>
</table>