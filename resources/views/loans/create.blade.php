<div class="form-group">
	<label for="amount">Amount</label>
	<input type="number" class="form-control" id="amount" name="amount" placeholder="Enter an amount (multiple by {{ $group->amount_per_share }})" required value="{{ old('amount') }}">
</div>
<div class="form-group">
	<label for="notes">Notes <small class="font-italic">(Not required)</small></label>
	<textarea class="form-control" name="notes">{{ old('notes') }}</textarea>
</div>