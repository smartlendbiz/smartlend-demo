<div class="card">
	<div class="card-body p-0">
	    <!-- Nav tabs -->
	    <ul class="nav md-tabs nav-justified pt-1 pb-1 m-0 bg-primary mb-1" role="tablist">
	      <li class="nav-item waves-effect waves-light" 
	      	  onclick="loadContent('/loans/pending', '.tab-content')">
	        <a class="nav-link" data-toggle="tab" href="#pending" role="tab">
	        	<span class="d-md-inline d-lg-inline">Pending Loan</span>
	        </a>
	      </li>	    	
	      <li class="nav-item waves-effect waves-light" 
	      	  onclick="loadContent('/loans/reservation', '.tab-content')">
	        <a class="nav-link" data-toggle="tab" href="#reservation" role="tab">
	        	<span class="d-md-inline d-lg-inline">Reservations</span>
	        </a>
	      </li>
	      <li class="nav-item waves-effect waves-light" 
	          onclick="loadContent('/loans/guarantor_await', '.tab-content')">
	        <a class="nav-link" data-toggle="tab" href="#guarantor_await" role="tab">
	        	<span class="d-md-inline d-lg-inline">Guarantor Await</span>
	        </a>
	      </li>
	    </ul>
	    <!-- Tab panels -->
	    <div class="tab-content pt-3 text-center">
	      <!--/.Panel 3-->
	    </div>		
	</div>
</div>
