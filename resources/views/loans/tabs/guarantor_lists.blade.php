{!! Form::open(['url' => '/loans/'.$loan->id, 'method' => 'PATCH']) !!}
	{!! Form::hidden('a', 'guarantor') !!}
	<p class="text-center">
		Total filled amount:
		<b>{{ dn($loan->totalGuarantorAmount()) }} / {{ dn($loan->amount)  }}</b>
	</p>
	@component('components.table', ['dt' => false])
		@slot('thead')
			<tr>
				<th>Member</th>
				<th width="35%">Amount</th>
			</tr>
		@endslot
		@slot('tbody')
			<tr>
				<td>{{ user()->name }}</td>
				<td>
					<input type="number" name="amount" 
					       class="form-control" value="{{ $loan->guarantorAmount(user()->id) }}" 
					       placeholder="Must be multiple by the amount per share">
				</td>
			</tr>
			@foreach($loan->loanGuarantors as $guarantor)
				@if($guarantor->user_id != user()->id)
				<tr>
					<td>{{ $guarantor->user->name }}</td>
					<td>
						{{ dn($guarantor->amount) }}
					</td>
				</tr>
				@endif
			@endforeach
		@endslot
	@endcomponent
	<div class="action text-center">
		<button type="button" class="btn btn-secondary btn-md" data-dismiss="modal">Close</button>	
		<button type="submit" class="btn btn-success btn-md">Ok</button>	
	</div>
{!! Form::close() !!}	