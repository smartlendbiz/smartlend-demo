<div class="table-responsive">
	@component('components.table', ['dt' => false])
		@slot('thead')
			<tr>
				<th>#</th>
				<th>Type</th>
				<th>Member</th>
				<th>Amount</th>
				<th class="text-center">Ready</th>
				<th class="text-center">Action</th>
			</tr>
		@endslot
		@slot('tbody')
			@forelse($loans as $loan)
			<tr>
				<td>{{ $loan->id }}</td>
				<td>{{ $loan->type }}</td>
				<td>
					{{ $loan->user->name }}
					@if($loan->notes)
					<i class="fas fa-info-circle text-info" data-toggle="tooltip"
					   data-placement="right" title="{{ $loan->notes }}"></i>
					@endif
				</td>
				<td>{{ dn($loan->amount) }}</td>
				<td class="text-center">
					@if($loan->ready)
						<i class="fas fa-check-circle text-success"></i>
					@else
						<i class="fas fa-spinner"></i>
					@endif
				</td>
				<td class="text-center">
					@if($loan->ready)
						{!! Form::open(['url' => '/loans/'.$loan->id.'?a=release', 'method' => 'PATCH', 'class' => 'd-inline']) !!}
							<button type="button" class="btn btn-xs btn-success" onclick="confirm($(this).parent(), 'to release your loan')">
								Release
							</button>
						{!! Form::close() !!}		
					@endif
					@component('components.action.delete', ['url' => '/loans/'.$loan->id])			
						<button type="button" class="btn btn-xs btn-danger" onclick="deleteConfirm($(this).parent())">
							Delete
						</button>
					@endcomponent
				</td>
			</tr>
			@empty
			<tr><td colspan="7" class="text-center">No reserved loans</td></tr>	
			@endforelse
		@endslot
	@endcomponent
</div>
<script type="text/javascript">
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})		
</script>