{!! Form::open(['url' => '/loans/'.$loan->id, 'method' => 'PATCH']) !!}
	{!! Form::hidden('a', 'pay') !!}
	<div class="d-flex justify-content-between">
		<p class="text-center">
			Loan Balance: <b>{{ dn($loan->totalBalance()) }}</b>
		</p>
		<p>
			Total Paid: <b>{{ dn($loan->totalPaid()) }}</b>
		</p>
	</div>
	@component('components.table', ['dt' => false])
		@slot('thead')
			<tr>
				<th>Date Paid</th>
				<th width="35%">Amount</th>
			</tr>
		@endslot
		@slot('tbody')
			@if($loan->totalBalance() != 0)
			<tr>
				<td class="align-middle">{{ dateToday() }}</td>
				<td>
					<input type="number" name="amount" class="form-control" min="1">
				</td>
			</tr>
			@endif
			@foreach($loan->loanPayments as $payment)
				<tr>
					<td class="align-middle">{{ df($payment->created_at) }}</td>
					<td>
						{{ dn($payment->amount) }}
					</td>
				</tr>
			@endforeach
		@endslot
	@endcomponent
	<div class="action text-center">
		<button type="button" class="btn btn-secondary btn-md" data-dismiss="modal">Close</button>	
		@if($loan->totalBalance() != 0)
		<button type="submit" class="btn btn-success btn-md">Ok</button>	
		@endif
	</div>
{!! Form::close() !!}	