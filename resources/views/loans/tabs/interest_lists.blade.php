<div class="d-flex justify-content-between mb-1 border p-2">
	<span>
		<b class="font-weight-bold">{{ $loan->released_date }}</b>
		<small class="d-block">Released date</small>
	</span>
	<span>
		<b class="font-weight-bold">{{ dn($loan->totalBalance()) }}</b>
		<small class="d-block">Loan Balance</small>
	</span>
	<span>
		<b class="font-weight-bold">{{ dn($loan->totalPaid()) }}</b>
		<small class="d-block">Total Paid</small>
	</span>	
	<span>
		<b class="font-weight-bold">{{ $loan->totalInterest() }}</b>
		<small class="d-block">Total Interest</small>		
	</span>
</div>
@component('components.table', ['dt' => false])
	@slot('thead')
		<tr>
			<th>Date Added</th>
			<th>Until Date</th>
			<th width="35%">Amount</th>
		</tr>
	@endslot
	@slot('tbody')
		@forelse($loan->loanInterests as $interest)
			<tr>
				<td>{{ df($interest->date_added) }}</td>
				<td>{{ df($interest->until_date) }}</td>
				<td>{{ dn($interest->amount) }}</td>
			</tr>
		@empty
			<tr><td colspan="2" class="text-center">No interest yet</td></tr>
		@endforelse
	@endslot
@endcomponent
<div class="action text-center">
	<button type="button" class="btn btn-secondary btn-md" data-dismiss="modal">Close</button>	
</div>