<div class="table-responsive">
	<div class="d-flex justify-content-center mb-2">
		<span class="badge badge-dark">Group(3%) + System(1%) + Rebate(1%) = Total Rate(5%)</span>		
	</div>
	@component('components.table', ['dt' => false])
		@slot('thead')
			<tr>
				<th>#</th>
				<th>Type</th>
				<th class="text-left">Member</th>
				<th>Amount</th>
				<th class="text-center">Ready</th>
			</tr>
		@endslot
		@slot('tbody')
			@forelse($loans as $loan)
			<tr>
				<td>{{ $loan->id }}</td>
				<td>{{ $loan->type }}</td>
				<td class="text-left">{{ $loan->user->name }}</td>
				<td>{{ dn($loan->amount) }}</td>
				<td class="text-center">
					@if($loan->ready)
						<i class="fas fa-check-circle text-success"></i>
					@else
						<i class="fas fa-spinner"></i>
					@endif
				</td>
			</tr>
			@empty
			<tr><td colspan="7" class="text-center">No reserved loans</td></tr>	
			@endforelse
		@endslot
	@endcomponent
</div>