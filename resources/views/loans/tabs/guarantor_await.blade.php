<div class="table-responsive">
	<div class="d-flex justify-content-center mb-2">
		<span class="badge badge-dark">Group(2%) + System(1%) + Guarantor(2%) = Total Rate(5%)</span>		
	</div>	
	@component('components.table', ['dt' => false])
		@slot('thead')
			<tr>
				<th>#</th>
				<th>Member</th>
				<th>Amount</th>
				<th>Filled</th>
				<th class="text-center">Action</th>
			</tr>
		@endslot
		@slot('tbody')
			@forelse($loans as $loan)
			<tr>
				<td>{{ $loan->id }}</td>
				<td>{{ $loan->user->name }}</td>
				<td>{{ dn($loan->amount) }}</td>
				<td>{{ dn($loan->totalGuarantorAmount()) }}</td>
				<td class="text-center">
					@if(user()->id != $loan->user_id)
						<button type="button" class="btn btn-xs btn-info" 
								data-href="/loans/guarantor_lists?lid={{ $loan->id }}"
								onclick="loadNoSubmitmodal($(this), 'guarantor_lists')">
							Guarantors
						</button>
					@endif
				</td>
			</tr>
			@empty
			<tr><td colspan="5" class="text-center">No reserved loans</td></tr>	
			@endforelse
		@endslot
	@endcomponent
</div>
@include('components.modal.nosubmit', ['title' => 'Guarantor Lists', 'trigger' => 'guarantor_lists'])