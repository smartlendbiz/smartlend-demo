@component('components.card', ['title' => 'My Loans', 'class' => 'mb-3'])
	@slot('header')
		<a class="btn btn-xs btn-warning float-right openAdd" data-href="/loans/create">Reserve a loan</a>
		{{-- <a class="btn btn-xs btn-warning float-right disabled">Reserve a loan</a> --}}
	@endslot
	<div class="table-responsive">
	@component('components.table', ['dt' => false])
		@slot('thead')
			<tr>
				<th>#</th>
				<th>Released</th>
				<th>Amount</th>
				<th>Interest</th>
				<th>Paid</th>
				<th>Balance</th>
				<th class="text-center">Action</th>
			</tr>
		@endslot
		@slot('tbody')
			@forelse($loans as $loan)
			<tr>
				<td>{{ $loan->id }}</td>
				<td>{{ $loan->released_date }}</td>
				<td>{{ $loan->amount }}</td>
				<td>{{ $loan->totalInterest() }}</td>
				<td>{{ $loan->totalPaid() }}</td>
				<td>{{ $loan->totalBalance() }}</td>
				<td class="text-center">
					@if(!$loan->isPaid())
					<button type="button" class="btn btn-xs btn-success pay_loan"
							data-href="/loans/pay_loan?lid={{ $loan->id }}">
						Payments / Pay
					</button>
					@endif
					<button type="button" class="btn btn-xs btn-cyan interest_lists"
							data-href="/loans/interest_lists?lid={{ $loan->id }}">
						Interest History
					</button>
					@if($loan->notes)
					<button type="button" class="btn btn-xs btn-light p-1">
						<i class="fas fa-info-circle text-info m-0" data-toggle="tooltip"
						   data-placement="right" title="{{ $loan->notes }}"></i>
					</button>
					@endif
				</td>
			</tr>
			@empty
			<tr><td colspan="7" class="text-center">No loan records</td></tr>
			@endforelse
		@endslot
	@endcomponent
	</div>
@endcomponent
