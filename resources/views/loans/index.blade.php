@extends('layouts.app')
@section('title', 'Loans')

@section('style')
@include('css.styles', ['styles' => ['datatable']])
@endsection

@include('components.group')

@section('tile')

@endsection

@section('content')
	<div class="row">	
		<div class="col-md-3 mb-3">
		    @component('components.tile')
	            <h4 class="font-weight-bold m-0">{{ dn($available_balance) }}</h4>
	            <p class="font-small text-success font-weight-bold m-0">Available Balance</p>
		    @endcomponent
		</div>				

		<div class="col-md-3 mb-3">
		    @component('components.tile')
	            <h4 class="font-weight-bold m-0">{{ dn($loan_guarantor_limit) }}</h4>
	            <p class="font-small text-success font-weight-bold m-0">Loan / Guarantor Limit</p>
		    @endcomponent
		</div>	

		<div class="col-md-3 mb-3">
		    @component('components.tile')
	            <h4 class="font-weight-bold m-0">{{ dn($loan_balance) }} / {{ dn($guarantor_balance) }}</h4>
	            <p class="font-small text-success font-weight-bold m-0">Loan /Guarantor Balance</p>
		    @endcomponent
		</div>	

		<div class="col-md-3 mb-3">
		    @component('components.tile')
	            <h4 class="font-weight-bold m-0">{{ dn($amount_for_loan) }}</h4>
	            <p class="font-small text-success font-weight-bold m-0">Amount for Loan</p>
		    @endcomponent
		</div>		
	</div>
	<div class="row">
		<div class="col-md-12">
			@include('components.alert')
		</div>
		<div class="col-md-7 col-sm-12">
			@include('loans.list')
		</div>
		<div class="col-md-5 col-sm-12">
			@include('loans.details')
		</div>	
	</div>
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['datatable','bootbbox', 'validate']])
<script type="text/javascript">
	$(document).ready(function() {
		let hash = window.location.hash || '#pending'
		$('a[href="'+hash+'"]').click()
	    $('.nav-item').on('click', function() {
	      window.location.hash = $(this).find('a').attr('href')
	    })       
	})
</script>
@include('components.modal.add', ['title' => 'Reserve Loan', 'url' => '/loans'])
@include('components.modal.nosubmit', ['title' => 'Pay Loan', 'trigger' => 'pay_loan'])
@include('components.modal.nosubmit', ['title' => 'Interest History', 'trigger' => 'interest_lists'])
@endsection
