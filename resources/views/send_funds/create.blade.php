{!! Form::open(['url' => '/send_funds', 'method' => 'POST', 'class' => 'form-validate','id'=>'form']) !!}
    <div class="row">
    	<div class="col-md-12">
            <div class="form-group">
                <label for="previous_receiver">Receiver</label>
                <select name="receiver_id" id="previous_receiver" class="form-control"
                onchange="javascript:location.href = '/send_funds?selected='+this.options[this.selectedIndex].value">
                    <option value="">Select</option>
                    <option value="0" {{ input('selected')=='0'?'selected':'' }}>New Receiver</option>
                    @foreach ($receivers as $receiver)
                    <option value="{{ $receiver->receiver_id }}" {{ input('selected')==$receiver->receiver_id?'selected':'' }}>
                        {{ $receiver->user->name }}
                    </option>
                    @endforeach
                </select>
            </div>
            @if (input('selected')=='0')
                <div class="form-group">
                    <label for="email">Receiver's email (The registered one)</label>
                    <input type="email" id="email" name="email" class="form-control" value="{{ old('email') }}" required>
                </div>
            @endif
            <div class="form-group">
                <label for="amount">Amount</label>
            <input type="number" id="amount" name="amount" class="form-control"  value="{{ old('amount') }}" required>
            </div>
            <div class="form-group">
                <button class="btn btn-primary btn-block" type="button"
                onclick="confirmAlert($('#form'),'Continue sending?')">Send</button>
            </div>
        </div>    	
    </div>
{!! Form::close() !!}