@extends('layouts.app')
@section('title', 'Send Funds')

@section('style')
@include('css.styles', ['styles' => ['datatable']])
@endsection
@section('content')
@include('components.alert')
<div class="row">
	<div class="mb-3 col-md-3">
	    @component('components.tile')
            <h4 class="font-weight-bold m-0">
            	{{ dn(user()->balance()) }}
           	</h4>
            <p class="font-small text-success font-weight-bold m-0">Available Balance</p>
	    @endcomponent
	</div>
</div>
<div class="row">
	<div class="col-md-5 mb-2">
		@component('components.card', ['title' => 'Send Funds Form'])
			@include('send_funds.create')
		@endcomponent
	</div>

	<div class="col-md-7">
		@component('components.card', ['title' => 'Send Funds History'])
		<div class="row">
			<div class="col-md-12 table-responsive">
				<table class="table table-sm dataTable table-bordered" width="100%">
					<thead>
						<tr>
							<th>Date</th>
							<th>Amount</th>
							<th>Type</th>
							<th>Method</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($send_funds_list as $fund)
							<tr>
								<td>{{ $fund->created_at }}</td>
								<td>{{ dn($fund->amount) }}</td>
								<td>{{ $fund->typeLabel() }}</td>
								<td>{{ $fund->method }}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		@endcomponent
	</div>
</div>
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['datatable', 'validate', 'bootbbox']])
<script type="text/javascript">
</script>
@endsection
