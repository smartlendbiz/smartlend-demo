@extends('layouts.app')
@section('title', 'Groups')

@section('style')
@include('css.styles', ['styles' => ['datatable']])
@endsection

@section('content')
	@component('components.card', ['title' => 'Group List'])
	<div class="row">
		<div class="col-md-12" style="overflow-x:auto;">
			@include('components.alert')
			<a class="btn btn-success btn-sm openAdd float-right" href="/groups/create"><i class="fas fa-plus"></i></a>
			<table class="table table-striped dataTable table-sm table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Amount per share</th>
						<th>Grace period</th>
						<th>Year</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($groups as $group)
						<tr>
							<td>{{ $group->id }}</td>
							<td>{{ $group->name }}</td>
							<td>{{ $group->amount_per_share }}</td>
							<td>{{ $group->grace_period }}</td>
							<td>{{ $group->year }}</td>
							<td>{{ $group->status }}</td>
							<td>
								<a class="text-warning p-2 openEdit" href="/groups/{{ $group->id }}/edit">
									<i class="fas fa-edit"></i>
								</a>
								@component('components.action.delete', ['url' => '/groups/'.$group->id])
									<a class="text-danger p-2 onDelete"><i class="fas fa-trash-alt"></i></a>
								@endcomponent
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>	
	@endcomponent	
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['datatable', 'validate', 'bootbbox']])
@endsection