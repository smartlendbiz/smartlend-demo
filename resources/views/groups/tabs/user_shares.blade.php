<form action="/groups/{{ $group->id }}" method="POST" class="form-validate">
	@csrf
	@method('PATCH')	
	<input type="hidden" name="type" value="user_shares">
	<table class="table table-sm table-bordered">
		<thead>
			<tr>
				<th>Members</th>
				<th>No of shares</th>
			</tr>
		</thead>
		<tbody>
			@foreach($group->users as $user)
			<tr>
				<td>{{ $user->name }}</td>
				<td>
					<input type="number" class="form-control" style="width:80px" name="user_shares[{{ $user->id }}]" value="{{ $user->share($group->id) }}">
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>

	<div class="step-actions mt-3 text-center">
		<a href="/groups" class="btn btn-sm btn-default">Back</a>
		<button type="submit" class="btn btn-sm btn-primary">Save</button>
	</div>
</form>
<script type="text/javascript">
$.extend( true, $.fn.dataTable.defaults, { paging: false, ordering: false });	
</script>