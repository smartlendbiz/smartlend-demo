<a class="btn btn-success btn-sm openAdd float-right" data-href="/payment_schedules/create"><i class="fas fa-plus"></i></a>
<table class="table table-bordered table-sm dataTable">
	<thead>
		<tr>
			<th>#</th>
			<th>Payment Date</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($group->paymentSchedules as $payment_schedule)
		<tr>
			<td>{{ $payment_schedule->id }}</td>
			<td>{{ $payment_schedule->payment_date }}</td>
			<td>
				@component('components.action.delete', ['url' => '/payment_schedules/'.$payment_schedule->id])
					<input type="hidden" name="type" value="payment_schedules">
					<a class="text-danger p-2" onclick="deleteConfirm($(this).parent('form'))"><i class="fas fa-trash-alt"></i></a>
				@endcomponent
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
<div class="step-actions mt-3 text-center">
	<a href="/groups" class="btn btn-sm btn-default">Back</a>
</div>	
@include('components.modal.add', ['title' => 'New payment schedule', 
                                  'url' => '/groups/'.$group->id, 
                                  'callback' => 'setDatePicker()'])