<form action="/groups/{{ $group->id }}" method="POST" class="form-validate">
	@csrf
	@method('PATCH')
	<input type="hidden" name="type" value="group_info">
	<div class="row">
		<div class="form-group col-md-6">
			<label for="name">Group name</label>
			<input type="text" class="form-control" id="name" name="name" placeholder="Enter group name" required value="{{ $group->name }}">
		</div>  
		<div class="form-group col-md-3">
			<label for="amount_per_share">Amount per share (value of 1 share)</label>
			<input type="number" class="form-control" id="amount_per_share" name="amount_per_share" placeholder="Enter a whole number" required value="{{ $group->amount_per_share }}">
		</div>  
		<div class="form-group col-md-3">
			<label for="year">Year</label>
			<input type="number" class="form-control" id="year" name="year" placeholder="Enter year (ex: 2020)" required value="{{ $group->year }}">
		</div>  
		<div class="form-group col-md-6">
			<label for="status">Status</label>
			<select class="form-control" id="status" name="status" required>
				<option value="Active" {{ $group->status=='Active'?'selected':'' }}>Active</option>
				<option value="Inactive" {{ $group->status=='Inactive'?'selected':'' }}>Inactive</option>
			</select>
		</div>    	
		<div class="form-group col-md-3">
			<label for="grace_period">Grace period (penalty basis - in days)</label>
			<input type="number" class="form-control" id="grace_period" name="grace_period" placeholder="Enter a whole number" required value="{{ $group->grace_period }}">
		</div>  
		<div class="form-group col-md-3">
			<label for="limit">Loan / Guarantor Limit (in %)</label>
			<input type="number" class="form-control" id="limit" name="limit" placeholder="Enter a whole number" required value="{{ $group->limit }}">
		</div> 		
	</div>
	<div class="step-actions mt-3 text-center">
		<a href="/groups" class="btn btn-sm btn-default">Back</a>
		<button type="submit" class="btn btn-sm btn-primary">Save</button>
	</div>
</form>