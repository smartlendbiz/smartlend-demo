<form method="POST" action="/groups/{{ $group->id }}">
	@csrf
	@method('PATCH')
	<div class="row">
		<div class="col-md-6">
			<h5>Members</h5>
				<input type="hidden" name="type" value="users">
				<select multiple="multiple" size="10" name="users[]" id="users" class="dual_list">
					@foreach($users as $user)
					<option value="{{ $user->id }}" {{ $group->isMember($user->id)? 'selected':'' }}>
						{{ $user->name }}
					</option>
					@endforeach
				</select>
		</div>
		<div class="col-md-6">
			<h5>Member Role</h5>
			<table class="table table-sm table-bordered dataTable">
				<thead>
					<tr>
						<th>Member</th>
						@foreach($roles as $role)
						<th class="text-center">{{ $role->name }}</th>
						@endforeach
					</tr>
				</thead>
				<tbody>
					@foreach($users as $user)
						@if($user->hasRole('Admin'))
							<tr>
								<td>{{ $user->name }}</td>
								@foreach($roles as $role)
								<td class="text-center">
									<div class="switch">
									  <label>
									    <input type="checkbox" 
									    	   name="group_user_roles[{{ $user->id }}][]" 
									    	   value="{{ $role->id }}"
									    	   {{ in_array($role->id, $group->roles($user->id))? 'checked': '' }}
									    	   >
									    <span class="lever"></span>
									  </label>
									</div>						
								</td>
								@endforeach
							</tr>
						@endif
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="step-actions mt-3 text-center">
		<a href="/groups" class="btn btn-sm btn-default">Back</a>
		<button type="submit" class="btn btn-sm btn-primary">Save</button>
	</div>		
</form>


