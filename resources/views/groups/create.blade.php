@extends('layouts.app')
@section('title', 'Group - create')

@section('content')
  @component('components.card', ['title' => 'New Group'])
  <div class="pl-3 pr-3">
    @include('components.alert')
  </div>
  <!-- Nav tabs -->
  <ul class="nav md-tabs nav-justified" role="tablist">
    <li class="nav-item waves-effect waves-light">
      <a class="nav-link active show" data-toggle="tab" href="#group_info" role="tab">
        <i class="fas fa-info"></i> Group Info
      </a>
    </li>
    <li class="nav-item waves-effect waves-light">
      <a class="nav-link disabled" data-toggle="tab" href="#users" role="tab">
        <i class="far fa-user"></i> Users
      </a>
    </li>
      <li class="nav-item waves-effect waves-light">
        <a class="nav-link disabled" data-toggle="tab" href="#user_shares" role="tab">
          <i class="fas fa-share-alt-square"></i> <span class="d-none d-md-inline d-lg-inline">User Shares</span>
        </a>
      </li>      
    <li class="nav-item waves-effect waves-light">
      <a class="nav-link disabled" data-toggle="tab" href="#payment_schedules" role="tab">
        <i class="fas fa-calendar-alt"></i> Payment Schedules
      </a>
    </li>
  </ul>
  <!-- Tab panels -->
  <div class="tab-content">
      <form action="/groups" method="POST" class="form-validate">
        @csrf
        <div class="row">
          <div class="form-group col-md-6">
            <label for="name">Group name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Enter group name" required>
          </div>  
          <div class="form-group col-md-3">
            <label for="amount_per_share">Amount per share (value of 1 share)</label>
            <input type="number" class="form-control" id="amount_per_share" name="amount_per_share" placeholder="Enter a whole number" required value="1000">
          </div>  
          <div class="form-group col-md-3">
            <label for="year">Year</label>
            <input type="number" class="form-control" id="year" name="year" placeholder="Enter year (ex: 2020)" required value="{{ date("Y") }}">
          </div>  
          <div class="form-group col-md-6">
            <label for="status">Status</label>
            <select class="form-control" id="status" name="status" required>
              <option value="Active">Active</option>
              <option value="Inactive">Inactive</option>
            </select>
          </div>      
          <div class="form-group col-md-3">
            <label for="grace_period">Grace period (penalty basis - in days)</label>
            <input type="number" class="form-control" id="grace_period" name="grace_period" placeholder="Enter a whole number" required value="5">
          </div>  
          <div class="form-group col-md-3">
            <label for="limit">Loan / Guarantor Limit (in %)</label>
            <input type="number" class="form-control" id="limit" name="limit" placeholder="Enter a whole number" required value="70">
          </div>            
        </div>
        <div class="step-actions mt-3 text-center">
          <a href="/groups" class="btn btn-sm btn-default">Back</a>
          <button type="submit" class="btn btn-sm btn-primary">Next</button>
        </div>
      </form>
  </div>
  @endcomponent
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['validate']])
<script type="text/javascript">
	$(document).ready(function() {
		$('.form-validate').validate()
	})
</script>
@endsection