@extends('layouts.app')
@section('title', 'Edit Group')

@section('style')
@include('css.styles', ['styles' => ['bootstrap-duallistbox','datatable', 'bootstrap-datepicker']])
@endsection

@section('content')
  @component('components.card', ['title' => 'Edit Group'])
    <div class="pl-3 pr-3">
    	@include('components.alert')
    </div>
    <!-- Nav tabs -->
    <ul class="nav md-tabs nav-justified" role="tablist">
      <li class="nav-item waves-effect waves-light" onclick="loadContent('/groups/group_info?gid={{$group->id}}', '.tab-content')">
        <a class="nav-link" data-toggle="tab" href="#group_info" role="tab">
        	<i class="fas fa-info"></i> <span class="d-none d-md-inline d-lg-inline">Group Info</span>
        </a>
      </li>
      <li class="nav-item waves-effect waves-light" onclick="loadContent('/groups/users?gid={{$group->id}}', '.tab-content')">
        <a class="nav-link" data-toggle="tab" href="#users" role="tab">
        	<i class="far fa-user"></i> <span class="d-none d-md-inline d-lg-inline">Users</span>
        </a>
      </li>
      <li class="nav-item waves-effect waves-light" onclick="loadContent('/groups/user_shares?gid={{$group->id}}', '.tab-content')">
        <a class="nav-link" data-toggle="tab" href="#user_shares" role="tab">
        	<i class="fas fa-share-alt-square"></i> <span class="d-none d-md-inline d-lg-inline">User Shares</span>
        </a>
      </li>  
      <li class="nav-item waves-effect waves-light" onclick="loadContent('/groups/payment_schedules?gid={{$group->id}}', '.tab-content')">
        <a class="nav-link" data-toggle="tab" href="#payment_schedules" role="tab">
        	<i class="fas fa-calendar-alt"></i> <span class="d-none d-md-inline d-lg-inline">Payment Schedules</span>
        </a>
      </li>
    </ul>
    <!-- Tab panels -->
    <div class="tab-content">
      <!--/.Panel 3-->
    </div>
  @endcomponent
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['datatable','validate', 'bootstrap-duallistbox', 'bootstrap-datepicker', 'bootbbox']])
<script type="text/javascript">
	$(document).ready(function() {
		let hash = window.location.hash || '#group_info'
		$('a[href="'+hash+'"]').click()
	})

</script>
@endsection