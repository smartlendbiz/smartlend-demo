@extends('layouts.app')
@section('title', 'Generate Member Invitation')

@section('content')
	@component('components.card', ['title' => 'Generate Member Invitation'])
	{!! Form::open(['url' => '/register_members/generate', 'method' => 'POST', 'class' => 'd-inline']) !!}
		{!! Form::hidden('token', $token) !!}
		<div class="input-group mb-3">
			<div class="input-group-prepend">
			<button type="submit" class="input-group-text">Token</button>
			</div>
			<input type="text" class="form-control" readonly value="{{ $link? $link: '' }}">
			<div class="input-group-append">
			<button type="button" class="input-group-text"><i class="fas fa-copy"></i></button>
			</div>			
		</div>
		<div class="text-center">	
			<button type="submit" class="btn btn-success">Generate New</button>
		</div>
	{!! Form::close() !!}
	@endcomponent
@endsection
