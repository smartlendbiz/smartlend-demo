@extends('layouts.home')

@section('content')
@if($valid)
{!! Form::open(['url' => 'register_members', 'method' => 'POST', 'class' => 'form-validate']) !!}
{!! Form::hidden('token', $token) !!}
  <!-- Heading -->
  @include('components.alert')
  <h3 class="dark-grey-text text-center">
    <strong>Member Registration Form</strong>
  </h3>
  <hr>

  <div class="form-group">
	<label for="name">Full Name</label>
	<input type="text" class="form-control" id="name" name="name" required value="{{ old('name') }}">
  </div>

  <div class="form-group">
	<label for="email">Email address</label>
	<input type="email" class="form-control" id="email" name="email" required value="{{ old('email') }}">
  </div>

  <div class="form-group">
	<label for="contact_no">Contact Number</label>
	<input type="text" class="form-control" id="contact_no" name="contact_no" required value="{{ old('contact_no') }}">
  </div>

  <div class="form-group">
	<label for="gender">Gender</label>
	<select class="form-control" id="gender" name="gender" required>
		<option value="">Select</option>
		<option value="Male" {{ old('gender')=='Male'?'selected':'' }}>Male</option>
		<option value="Female" {{ old('gender')=='Female'?'selected':'' }}>Female</option>
  </select>
  </div>

  <div class="form-group">
  <label for="name">Password</label>
  <input type="password" class="form-control" id="password" name="password" required>
  </div>

  <div class="form-group">
  <label for="name">Confirm Password</label>
  <input type="password" class="form-control" id="confirm_password" name="confirm_password" required>
  </div>

  <div class="text-center">
    <button type="submit" class="btn btn-indigo btn-block">Submit</button>
  </div>
{!! Form::close() !!}
@else
<div class="alert alert-warning" role="alert">
  The link is expired. Please request for a new one.
</div>
@endif
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['datatable', 'validate', 'bootbbox']])
<script type="text/javascript">
$(document).ready(function() {
	$('.form-validate').validate()
})
</script>
@endsection