{!! Form::open(['url' => '/cash_in', 'method' => 'POST', 'class' => 'form-validate', 'files'=>'true']) !!}
    <div class="row">
    	<div class="col-md-12">
			<div class="form-group">
				<label for="amount">Amount</label>
				<input type="number" class="form-control" id="amount" name="amount" placeholder="Enter an amount" required value="{{ old('amount') }}">
			</div>
    	</div>
    	<div class="col-md-12">
			<div class="form-group">
				<label for="name">Method</label>
				<select class="form-control" name="method" required>
					{{-- <option value="">Select</option> --}}
					{{-- <option value="Cash" {{ old('method')=='Cash'? 'selected':'' }}>Cash</option> --}}
					<option value="Fund Transfer" {{ old('method')=='Fund Transfer'? 'selected':'' }}>
						Fund Transfer
					</option>
				</select>
			</div>
    	</div>	
    	<div class="col-md-12">
			<div class="form-group">
				<label for="name">Bank / Source</label>
				<select class="form-control" name="bank_detail_id" required id="bank_detail_id">
					<option value="">Select</option>
					@foreach(currentUserGroup()->bankDetails as $bank)
						@if($bank->status == 'Active')
							<option value="{{ $bank->id }}" {{ old('bank_detail_id')==$bank->id?'selected':'' }}>
								({{ $bank->source }} - {{ $bank->account_no }}) {{ $bank->holder }}
							</option>
						@endif
					@endforeach
				</select>
				<a href="/images/bpi_qrcode.jpg" id="bpi_qrcode" class="d-none" target="_blank">QR Code (click here)</a>
			</div>
		</div>	 
		<div class="col-md-12">
			<div class="form-group">
				<label for="name">Purpose</label>
				<select class="form-control" name="purpose" required>
					<option value="">Select</option>
					<option value="contributionPayment" {{ old('purpose')=='contributionPayment'?'selected':'' }}>Contribution Payment</option>
					<option value="topUp" {{ old('purpose')=='topUp'?'selected':'' }}>TopUp Balance</option>
				</select>
			</div>
    	</div>	  
    	<div class="col-md-12 mb-2">
			<small class="text-warning">Amount should match in the screenshot</small>
			<div class="panel-body">
			  <div class="input-group">
			    <input id="uploadFile" class="form-control" placeholder="Choose Screenshot" disabled="disabled">
			    <div class="input-group-btn">
			      <div class="fileUpload btn btn-success btn-md">
			        <span><i class="fas fa-upload"></i> Upload</span>
			        <input id="uploadBtn" type="file" class="upload" name="screenshot" />
			      </div>
			    </div>
			  </div>
			</div>
    	</div>
    	<div class="col-12">
			<div class="form-group">
				<label for="notes">Notes <small class="font-italic">(Not required)</small></label>
				<textarea class="form-control" name="notes">{{ old('notes') }}</textarea>
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-sm btn-success load-trigger">Submit</button>
			</div>
    	</div>	    	
    </div>
{!! Form::close() !!}