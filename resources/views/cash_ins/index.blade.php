@extends('layouts.app')
@section('title', 'Cash In')

@section('style')
@include('css.styles', ['styles' => ['datatable']])
<style type="text/css">
.fileUpload {
    position: relative;
    overflow: hidden;
    top: -6px;
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}	
</style>
@endsection
@section('content')
@include('components.alert')
<div class="row">	
	<div class="col-md-6 mb-3">
	    @component('components.tile')
            <h4 class="font-weight-bold m-0">
            	{{ dn(user()->balance()) }}
           	</h4>
            <p class="font-small text-success font-weight-bold m-0">Available Balance</p>
	    @endcomponent
	</div>				
	<div class="col-md-6 mb-3">
	    @component('components.tile')
            <h4 class="font-weight-bold m-0">
            	{{ dn(user()->getPendingCash(1)) }}
           	</h4>
            <p class="font-small text-success font-weight-bold m-0">Pending Cash In</p>
	    @endcomponent
	</div>					
</div>
<div class="row">
	<div class="col-md-5 mb-2">
		@component('components.card', ['title' => 'Cash In Form'])
			@include('cash_ins.create')
		@endcomponent
	</div>

	<div class="col-md-7">
		@component('components.card', ['title' => 'Cash In History'])
		<div class="row">
			<div class="col-md-12 table-responsive">
				<table class="table table-sm dataTable table-bordered" width="100%">
					<thead>
						<tr>
							<th>Date</th>
							<th>Amount</th>
							<th>Bank / Source</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						@foreach($cash_ins as $cash_in)
						<tr>
							<td>{{ $cash_in->created_at }}</td>
							<td>{{ $cash_in->amount }}</td>
							<td>
								{{ $cash_in->source() }}
								@if($cash_in->notes)
								<i class="fas fa-info-circle text-info" data-toggle="tooltip"
								   data-placement="right" title="{{ $cash_in->notes }}"></i>
								@endif	
								@if(isset($cash_in->file))
								<i class="fas fa-paperclip text-primary pointer screenshot" 
								   data-href="/cash_in/{{ $cash_in->id }}"></i>						
								@endif
							</td>
							<td>
								{{ $cash_in->status }}
								@if($cash_in->status == 'Pending')
									@component('components.action.delete', ['url' => '/cash_in/'.$cash_in->id])
										<a class="text-danger p-2 onDelete"><i class="fas fa-trash-alt"></i></a>
									@endcomponent					
								@endif	
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		@endcomponent
	</div>
</div>
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['datatable', 'validate', 'bootbbox']])
@include('components.modal.nosubmit', ['title' => 'Screenshot', 'trigger' => 'screenshot'])
<script type="text/javascript">
	$(document).ready(function() {
		// $('[name="method"]').on('change', function() {
		// 	let bank_detail = $('[name="bank_detail_id"]')
		// 	let method = $(this).find('option:selected')
		// 	if(method.val() == 'Fund Transfer') {
		// 		bank_detail.prop('disabled', false)
		// 	} else {
		// 		bank_detail.val('').change()
		// 		bank_detail.prop('disabled',true)
		// 	}
		// })

		$('#bank_detail_id').on('change', function() {
			if(this.value === '1') {
				$('#bpi_qrcode').removeClass('d-none')
			} else {
				$('#bpi_qrcode').addClass('d-none')
			}
		});

		$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
		})		

		document.getElementById("uploadBtn").onchange = function () {
			document.getElementById("uploadFile").value = this.value.substring(12);
		};
	})	
</script>
@endsection

