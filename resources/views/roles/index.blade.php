@extends('layouts.app')
@section('title', 'Roles')

@section('style')
@include('css.styles', ['styles' => ['datatable']])
@endsection

@section('content')
@component('components.card', ['title' => 'Role List'])
	<div class="row">
		<div class="col-md-12">
			@include('components.alert')
			<a class="btn btn-success btn-sm openAdd float-right" data-href="/roles/create"><i class="fas fa-plus"></i></a>
			<table class="table table-striped dataTable table-sm table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($roles as $role)
						<tr>
							<td>{{ $role->id }}</td>
							<td>{{ $role->name }}</td>
							<td>
								<a class="text-warning p-2 openEdit" data-href="/roles/{{ $role->id }}/edit">
									<i class="fas fa-edit"></i>
								</a>
								@component('components.action.delete', ['url' => '/roles/'.$role->id])
									<a class="text-danger p-2 onDelete"><i class="fas fa-trash-alt"></i></a>
								@endcomponent
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>	
@endcomponent
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['datatable', 'validate', 'bootbbox']])
@include('components.modal.edit', ['title' => 'Edit Role', 'url' => '/roles/0'])
@include('components.modal.add', ['title' => 'New Role', 'url' => '/roles'])
@endsection