<input type="hidden" name="id" value="{{ $role->id }}">
<div class="form-group">
	<label for="name">Role Name</label>
	<input type="text" class="form-control" id="name" name="name" placeholder="Enter name" required value="{{ $role->name }}">
</div>