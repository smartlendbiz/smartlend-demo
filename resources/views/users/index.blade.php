@extends('layouts.app')
@section('title', 'Users')

@section('style')
@include('css.styles', ['styles' => ['datatable']])
@endsection

@section('content')
@component('components.card', ['title' => 'User List'])
	<div class="row">
		<div class="col-md-12 table-overflow">
			@include('components.alert')
			<a class="btn btn-success btn-sm openAdd float-right" data-href="/users/create"><i class="fas fa-plus"></i></a>
			<div >
				<table class="table table-striped dataTable table-sm table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Email</th>
							<th>Contact No.</th>
							<th>Gender</th>
							<th>Role</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($users as $user)
							<tr>
								<td>{{ $user->id }}</td>
								<td>{{ $user->name }}</td>
								<td>{{ $user->email }}</td>
								<td>{{ $user->contact_no }}</td>
								<td>{{ $user->gender }}</td>
								<td>{{ $user->displayRoles() }}</td>
								<td>{{ $user->status }}</td>
								<td>
									<a class="text-warning p-2 openEdit" data-href="/users/{{ $user->id }}/edit">
										<i class="fas fa-edit"></i>
									</a>
									@component('components.action.delete', ['url' => '/users/'.$user->id])
										<a class="text-danger p-2 onDelete"><i class="fas fa-trash-alt"></i></a>
									@endcomponent
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>	
@endcomponent	
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['datatable', 'validate', 'bootbbox']])
@include('components.modal.edit', ['title' => 'Edit User', 'url' => '/users/0'])
@include('components.modal.add', ['title' => 'New User', 'url' => '/users'])
@endsection