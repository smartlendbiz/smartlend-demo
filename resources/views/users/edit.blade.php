<input type="hidden" name="id" value="{{ $user->id }}">
<div class="form-group">
	<label for="name">Full Name</label>
	<input type="text" class="form-control" id="name" name="name" placeholder="Enter name" required value="{{ $user->name }}">
</div>

<div class="form-group">
	<label for="email">Email address</label>
	<input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required value="{{ $user->email }}">
</div>

<div class="form-group">
	<label for="contact_no">Contact Number</label>
	<input type="text" class="form-control" id="contact_no" name="contact_no" placeholder="Enter contact no" required value="{{ $user->contact_no }}">
</div>

<div class="form-group">
	<label for="gender">Gender</label>
	<select class="form-control" id="gender" name="gender" required>
		<option value="Male" {{ $user->gender == 'Male'? 'selected':'' }}>Male</option>
		<option value="Female" {{ $user->gender == 'Female'? 'selected':'' }}>Female</option>
	</select>
</div>

<div class="form-group">
	<label for="role">Role</label>
	<select class="form-control" name="role_id" required>
		@foreach($roles as $role)
			<option value="{{ $role->id }}" {{ $user->hasRole($role->name)?'selected':'' }}>{{ $role->name }}</option>
		@endforeach
	</select>
</div>

<div class="form-group">
	<label for="status">Status</label>
	<select class="form-control" id="status" name="status" required>
		<option value="Active" {{ $user->status == 'Active'? 'selected':'' }}>Active</option>
		<option value="Inactive" {{ $user->status == 'Inactive'? 'selected':'' }}>Inactive</option>
	</select>
</div>
