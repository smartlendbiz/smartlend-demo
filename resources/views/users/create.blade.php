<div class="form-group">
	<label for="name">Full Name</label>
	<input type="text" class="form-control" id="name" name="name" placeholder="Enter name" required>
</div>

<div class="form-group">
	<label for="email">Email address</label>
	<input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required>
</div>

<div class="form-group">
	<label for="contact_no">Contact Number</label>
	<input type="text" class="form-control" id="contact_no" name="contact_no" placeholder="Enter contact no" required>
</div>

<div class="form-group">
	<label for="gender">Gender</label>
	<select class="form-control" id="gender" name="gender" placeholder="Enter contact no" required>
		<option value="">Select</option>
		<option value="Male">Male</option>
		<option value="Female">Female</option>
	</select>
</div>

<div class="form-group">
	<label for="role">Role</label>
	<select class="form-control" name="role_id" required>
		@foreach($roles as $role)
			<option value="{{ $role->id }}" {{ $role->name=='Member'?'selected':'' }}>{{ $role->name }}</option>
		@endforeach
	</select>
</div>