@php
$script_list = [
	'datatable'       => 'js/addons/datatables.min.js',
	'validate'        => 'js/addons/jquery.validate.min.js',
	'bootbbox'        => 'js/addons/bootbox.min.js',
	'jquery.transfer' => 'js/addons/jquery.transfer.js',
	'bootstrap-duallistbox' => 'js/addons/bootstrap-duallistbox.min.js',
	'bootstrap-datepicker' => 'js/addons/bootstrap-datepicker.min.js',
];
@endphp

@foreach($scripts as $script)
<script type="text/javascript" src="{{ asset($script_list[$script]) }}"></script>
@endforeach