@extends('layouts.app')
@section('title', 'Contributions')

@section('style')
@include('css.styles', ['styles' => ['datatable']])
@endsection

@include('components.group')

@section('content')
	<div class="row">
		<div class="col-md-12">
			@include('components.alert')
		</div>		
		<div class="col-md-6 mb-3">
		    @component('components.tile')
		            <h4 class="font-weight-bold m-0">{{ dn(auth()->user()->balance()) }}</h4>
		            <p class="font-small grey-text m-0">Available Balance</p>
		    @endcomponent
		</div>
		<div class="col-md-6 mb-3">
		    @component('components.tile')
		            <h4 class="font-weight-bold m-0">{{ dn($total_active_contributions) }}</h4>
		            <p class="font-small grey-text m-0">Total Paid Contributions</p>
		    @endcomponent
		</div>				
	</div>
	<div class="row">
		<div class="col-md-6 table-overflow">
			@include('contributions.payment')
		</div>
		<div class="col-md-6 table-overflow">
			@include('contributions.paid')
		</div>
	</div>
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['datatable','bootbbox','validate']])
<script type="text/javascript">
$.extend( true, $.fn.dataTable.defaults, { ordering: false });
</script>
@endsection