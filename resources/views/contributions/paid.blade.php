@component('components.card', ['title' => 'Paid Contributions'])
	@component('components.table')
		@slot('thead')
			<tr>
				<th>Date Paid</th>
				<th>Amount</th>
			</tr>
		@endslot
		@slot('tbody')
			@foreach($paid_contributions as $contribution)
				<tr>
					<td>{{ $contribution->created_at }}</td>
					<td>{{ dn($contribution->amount) }}</td>
				</tr>
			@endforeach
		@endslot
	@endcomponent
@endcomponent