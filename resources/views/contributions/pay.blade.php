@if(user()->balance() >= $payment_schedule->amount_to_pay)
	{!! Form::open(['url' => '/contributions', 'method' => 'POST']) !!}
	<div>
		<p>Your current available balance is <strong>{{ dn(user()->balance()) }}</strong></p>
		<p>You are about to deduct <b>{{ dn($payment_schedule->amount_to_pay) }}</b> from you available balance.</p>
		<p>Do you want to continue?</p>
	</div>
	<div class="action text-center">
		<button type="button" class="btn btn-secondary btn-md" data-dismiss="modal">Close</button>	
		<button type="submit" class="btn btn-success btn-md">Ok</button>	
	</div>
	{!! Form::close() !!}
@else
	<div>
		<p>Your current available balance is <strong>{{ dn(user()->balance()) }}</strong></p>
		<p>Sorry you don't have enough balance to pay your <b>{{ dn($payment_schedule->amount_to_pay) }}</b> contribution.</p>
		<p>Navigate to Cash In tab to deposit an amount.</p>
	</div>
	<div class="action text-center">
		<button type="button" class="btn btn-secondary btn-md" data-dismiss="modal">Close</button>	
	</div>
@endif