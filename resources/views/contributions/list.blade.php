@component('components.card', ['title' => 'List of Contributions'])
	@component('components.table', ['class' => 'table-striped'])
		@slot('thead')
			<tr>
				<th>Payment Date</th>
				<th>Due Date</th>
				<th>Amount to pay</th>
				<th class="text-center">Action</th>
			</tr>
		@endslot
		@slot('tbody')
			@php $pay = true; @endphp
			@foreach($payment_schedules as $schedule)
				<tr>
					<td>{{ $schedule->payment_date }}</td>
					<td>{{ $schedule->due_date }}</td>
					<td>{{ dn($schedule->amount_to_pay) }}</td>
					<td class="text-center">
						@if($pay)
						<button class="btn btn-xs btn-success pay" data-href="/contributions/pay">Pay</button>
						@endif
					</td>
				</tr>
				@php $pay = false; @endphp
			@endforeach
		@endslot
	@endcomponent
@endcomponent