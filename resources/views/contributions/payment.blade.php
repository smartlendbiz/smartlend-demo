@component('components.card', ['title' => (!!$current_contribution?'Pay Contribution':'Set number of share'), 'class' => 'mb-3'])
@if ($current_contribution)
{!! Form::open(['url' => '/contributions', 'method' => 'POST', 'class' => 'form-validate', 'id' => 'contri']) !!}
    <div class="form-group">
        <label for="contribution">Contribution</label>
        <select name="current_contribution_id" id="contribution" class="form-control" required readonly>
            <option value="{{ $current_contribution->id }}">
                Year {{ $current_contribution->year }} - {{ $current_contribution->no_of_share }} Share(s) @ {{ dn($current_contribution->amount_per_share) }}
            </option>
            @foreach ($contribution_lists as $list)    
                @if ($list->id != $current_contribution->id)
                <option value="{{ $list->id }}">
                    Year {{ $list->year }} - {{ $list->no_of_share }} Share(s) @ {{ dn($list->amount_per_share) }}    
                </option>
                @endif
            @endforeach
        </select>
    </div>
        <div class="form-group">
            <label for="no_of_share">No of contribution to pay</label>
            <input type="number" id="noshare" name="no_of_share" class="form-control" required
            min="1" max="50" steps="1" value="1"
            oninput="$('#payable').html($('#noshare').val() * {{ $current_contribution->payPerShare() }})">
        </div>
        <div class="d-flex justify-content-between mb-2">
            <span>Unpaid Contribution</span>
            <strong>{{ dn($current_contribution->total_share - $current_contribution->totalPaid()) }}</strong>
        </div>
        <div class="d-flex justify-content-between mb-4">
            <strong class="text-info">Amount to Pay</strong>
            <strong class="text-info" id="payable">₱{{ $current_contribution->payPerShare() }}</strong>
        </div>
        <div class="text-right">
            <button class="btn btn-primary btn-block" type="button" 
            onclick="confirmAlert($('#contri'),'Continue paying '+($('#noshare').val() * {{ $current_contribution->payPerShare() }})+'?')">
                Pay
            </button>
        </div> 

{!! Form::close() !!}
@endif
@if (!!!$current_contribution) 
{!! Form::open(['url' => '/contributions?new=true', 'method' => 'POST', 'class' => 'form-validate mt-3','id'=>'nshare']) !!}
    <div class="form-group">
        <label for="no_of_share">How many share do you want?</label>
        @php 
            $amount = currentUserGroup()->amount_per_share;
        @endphp
        <script>
            function calculate() {
                var no = $('#no_of_share').val();
                $('#bimonthly').html(no * {{ $amount }});
                $('#monthly').html(no * {{ $amount }} * 2);
                $('#yearly').html(no * {{ $amount }} * 24);
            }
        </script>
        <input type="number" id="no_of_share" class="form-control" name="no_of_share"
            oninput="calculate()"
        />
        <small class="text-warning">Note: Changing the number of share cannot be changed after it has been submitted.</small>
    </div>
    <div class="d-flex justify-content-between my-2">
        <span>Bimonthly Contribution</span>
        <span id="bimonthly">0</span>
    </div>
    <div class="d-flex justify-content-between my-2">
        <span>Monthly Contribution</span>
        <span id="monthly">0</span>
    </div>
    <div class="d-flex justify-content-between my-2">
        <span>Total Contribution in a year</span>
        <span id="yearly">0</span>
    </div>
    <div class="form-group">
        <button class="btn btn-primary btn-block" type="button"
        onclick="confirmAlert($('#nshare'),'Confirm Number of share. Is <b>'+($('#no_of_share').val()||0)+'</b> correct?')">Submit</button>
    </div>
{!! Form::close() !!}
@endif
@endcomponent