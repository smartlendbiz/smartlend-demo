@extends('layouts.app')
@section('title', 'Balance History')

@section('style')
@include('css.styles', ['styles' => ['datatable']])
@endsection

@include('components.group')
@section('content')
@component('components.card', ['title' => 'Balance History'])
<div class="row">
	<div class="col-md-12 table-responsive">
		@component('components.table')
			@slot('thead')
				<tr>
					<th>Date</th>
					<th>Type</th>
					<th>Amount</th>
					<th>Status</th>
				</tr>
			@endslot
			@slot('tbody')
				@foreach($balances as $balance)
				<tr>
					<td>{{ $balance->created_at }}</td>
					<td>{{ $balance->balance_types[$balance->type] }}</td>
					<td>
						<span class="badge badge-{{ in_array($balance->type, [1,5])? 'success': 'danger' }}">
							{{ dn($balance->amount) }}
						</span>
					</td>
					<td>{{ $balance->status }}</td>
				</tr>
				@endforeach
			@endslot
		@endcomponent
	</div>
</div>	
@endcomponent 
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['datatable']])
<script type="text/javascript">
$.extend( true, $.fn.dataTable.defaults, { "order": [[ 0, "desc" ]] });		
</script>
@endsection