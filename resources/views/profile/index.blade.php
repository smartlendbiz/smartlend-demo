@extends('layouts.app')
@section('title', 'My Profile')

@section('content')
	@component('components.card', ['title' => 'My Profile'])
    @include('components.alert')
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
      <div class="text-center">
        <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar rounded-circle img-thumbnail mb-3" alt="avatar">
        <input type="file" class="text-center center-block file-upload">
      </div></hr><br>
      <ul class="list-group">
        <li class="list-group-item text-muted d-flex">
        	<span class="w-75 font-weight-bold">Group Details</span>
        	<span class="w-25 font-weight-bold">Shares</span>
        </li>
        @foreach($user->groups as $group)
        <li class="list-group-item text-muted d-flex">
        	<span class="w-75">{{ $group->name }}</span>
        	<span class="w-25">{{ $group->totalShares($user->id) }}</span>
        </li>   
        @endforeach     
      </ul> 

        </div><!--/col-3-->
    	<div class="col-sm-9">
	        {!! Form::open(['url' => '/profile/'.$user->id, 'method' => 'PUT']) !!}
				<div class="form-group">
				  <div class="col-xs-6">
				      <label for="first_name">Full Name</label>
				      <input type="text" class="form-control" name="name" id="name" placeholder="Full name" title="Enter your fullname" value="{{ $user->name }}">
				  </div>
				</div>

				<div class="form-group">
				  <div class="col-xs-6">
				      <label for="email">Email</label>
				      <input type="email" class="form-control" placeholder="your@email.com" title="enter your email." disabled value="{{ $user->email }}">
				  </div>
				</div>

				<div class="form-group">
				  <div class="col-xs-6">
				     <label for="mobile">Mobile number (09xxxxxxxxx or +639xxxxxxxxx)</label>
				      <input type="text" class="form-control" name="contact_no" placeholder="Enter mobile number" title="enter your mobile number" value="{{ $user->contact_no }}">
				  </div>
				</div>

				<div class="form-group">
				  <div class="col-xs-6">
				      <label for="password">Gender</label>
				      <select class="form-control" name="gender">
				      	<option value="Male" {{ $user->gender == 'Male'? 'selected': '' }}>Male</option>
				      	<option value="Female" {{ $user->gender == 'Female'? 'selected': '' }}>Female</option>
				      </select>
				  </div>
				</div>

				<div class="form-group">
				  <div class="col-xs-6">
				      <label for="password">Password</label>
				      <input type="password" class="form-control" name="password" id="password" placeholder="password" title="enter your password.">
				  </div>
				</div>

				<div class="form-group">
				  <div class="col-xs-6">
				    <label for="password2">Verify Password</label>
				      <input type="password" class="form-control" name="password_confirmation" id="password2" placeholder="Password Confirmation" title="enter your password2.">
				  </div>
				</div>
				<div class="form-group">
				   <div class="col-xs-12">
				        <br>
				      	<button class="btn btn-md btn-success" type="submit"><i class="fas fa-thumbs-up"></i> Save</button>
				    </div>
				</div>
	      	{!! Form::close() !!}	       
        </div><!--/col-9-->
    </div><!--/row-->
	@endcomponent
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['validate']])
@endsection