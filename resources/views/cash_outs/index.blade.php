@extends('layouts.app')
@section('title', 'Cash Out')

@section('style')
@include('css.styles', ['styles' => ['datatable']])
@endsection
@section('content')
@include('components.alert')
<div class="row">	
	<div class="col-md-6 mb-3">
	    @component('components.tile')
            <h4 class="font-weight-bold m-0">
            	{{ dn(user()->balance()) }}
           	</h4>
            <p class="font-small text-success font-weight-bold m-0">Available Balance</p>
	    @endcomponent
	</div>				
	<div class="col-md-6 mb-3">
	    @component('components.tile')
            <h4 class="font-weight-bold m-0">
            	{{ dn(user()->getPendingCash(2)) }}
           	</h4>
            <p class="font-small text-success font-weight-bold m-0">Pending Cash Out</p>
	    @endcomponent
	</div>					
</div>
<div class="row">
	<div class="col-md-5 mb-2">
		@component('components.card', ['title' => 'Cash Out Form'])
			{!! Form::open(['url' => '/cash_out', 'method' => 'POST', 'class' => 'form-validate']) !!}
			    <div class="row">
			    	<div class="col-12">
						<div class="form-group">
							<label for="amount" class="d-flex justify-content-between">Amount</label>
							<input type="number" class="form-control" id="amount" name="amount" placeholder="Enter an amount" required value="{{ old('amount') }}">
							<label class="d-flex justify-content-between">
								<strong> You will receive: <u class="receive">0</u> </strong>
								<strong>
									Processing fee: <span class="fee">50</span>
								</strong>
							</label>							
						</div>
			    	</div>
			    	<div class="col-12">
						<div class="form-group">
							<label for="name">Method</label>
							<select class="form-control" name="method" required>
								<option value="">Select</option>
								{{-- <option value="Cash" {{ old('method')=='Cash'? 'selected':'' }}>Cash</option>
								<option value="Fund Transfer" {{ old('method')=='Fund Transfer'? 'selected':'' }}>Fund Transfer</option> --}}
								<option value="BPI" {{ old('method')=='BPI'? 'selected':'' }}>BPI</option>
								<option value="Union Bank" {{ old('method')=='Union Bank'? 'selected':'' }}>Union Bank</option>
								<option value="Coins.ph" {{ old('method')=='Coins.ph'? 'selected':'' }}>Coins.ph</option>
								<option value="GCash" {{ old('method')=='GCash'? 'selected':'' }}>GCash</option>
								<option value="Paymaya" {{ old('method')=='Paymaya'? 'selected':'' }}>Paymaya</option>
								<option value="Other Bank" {{ old('method')=='Other Bank'? 'selected':'' }}>Other Bank</option>
							</select>
						</div>
			    	</div>	
			    	<div class="col-12">
						<div class="form-group">
							<label for="account_identifier">Account No<small>(Bank)</small> / Mobile&nbsp;No<small>(Coins.ph,GCash,Paymaya)</small></label>
							<input type="text" id="account_identifier" name="account_identifier" class="form-control" required value="{{ old('account_identifier') }}">
						</div>
						<div class="form-group">
							<label for="notes" class="strong">Notes <small>(Name of Receiver / If "Other Bank" specify the Bank Name)</small></label>
							<textarea class="form-control" name="notes" required>{{ old('notes') }}</textarea>
						</div>
						<div class="text-center">
							<button type="submit" class="btn btn-sm btn-success">Submit</button>
						</div>
			    	</div>	    	
			    </div>
			{!! Form::close() !!}
		@endcomponent
	</div>

	<div class="col-md-7">
		@component('components.card', ['title' => 'Cash Out History'])
		<div class="row">
			<div class="col-md-12 table-responsive">
				<table class="table table-sm dataTable table-bordered" width="100%">
					<thead>
						<tr>
							<th>Date</th>
							<th>Amount</th>
							<th>Processing Fee</th>
							<th>Amount Received</th>
							<th>Notes</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						@foreach($cash_outs as $cash_out)
						<tr>
							<td>{{ $cash_out->created_at }}</td>
							<td>{{ $cash_out->amount }}</td>
							<td>{{ $cash_out->getCashoutFee() }}</td>
							<td>{{ $cash_out->getReleasedAmount() }}</td>
							<td>{{ $cash_out->notes }}</td>
							<td>
								{{ $cash_out->status }}
								@if($cash_out->status == 'Pending')
									@component('components.action.delete', ['url' => '/cash_out/'.$cash_out->id])
										<a class="text-danger p-2 onDelete"><i class="fas fa-trash-alt"></i></a>
									@endcomponent					
								@endif	
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		@endcomponent
	</div>
</div>
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['datatable', 'validate', 'bootbbox']])
<script type="text/javascript">
$(document).ready(function() {
	$('#amount').on('input', function() {
		var percent = {{ currentUserGroup()->processing_fee }} / 100;
		var amount = $(this).val();
		var fee = 50;
		var receive = amount - fee;
		$('.receive').html(receive>0? receive: 0);
		$('.fee').html(fee||0);
	})
})
</script>
@endsection

