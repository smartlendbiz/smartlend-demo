<a class="btn btn-success btn-sm openAdd" data-href="/payment_schedules/create"><i class="fas fa-plus"></i></a>
<table class="table table-bordered table-sm dataTable">
	<thead>
		<tr>
			<th>#</th>
			<th>Payment Date</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<!-- <tr>
			<td></td>
		</tr> -->
	</tbody>
</table>
@include('components.modal.add', ['title' => 'New payment schedule', 
                                  'url' => '/groups/payment_schedules?gid='.$group->id, 
                                  'callback' => 'setDatePicker()'])