<div class="form-group">
	<input type="hidden" name="type" value="payment_schedules">
	@method('PUT')
	<label for="payment_date">Payment date</label>
	<input type="text" class="form-control datepicker" id="payment_date" name="payment_date" placeholder="Enter payment date" required autocomplete="off">
</div>