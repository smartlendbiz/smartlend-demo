<div class="form-group">
	<label for="source">Bank / Source</label>
	<input type="text" class="form-control" id="source" name="source" placeholder="Enter bank / source" required value="{{ old('source') }}">
</div>


<div class="form-group">
	<label for="account_no">Account No.</label>
	<input type="text" class="form-control" id="account_no" name="account_no" placeholder="Enter account no" required value="{{ old('account_no') }}">
</div>

<div class="form-group">
	<label for="holder">Account Holder Name</label>
	<input type="text" class="form-control" id="holder" name="holder" placeholder="Enter holder name" required value="{{ old('holder') }}">
</div>

<div class="form-group">
	<label for="status">Status</label>
	<select class="form-control" id="status" name="status" required>
		<option value="Active" {{ old('status') == 'Active'? 'selected':'' }}>Active</option>
		<option value="Inactive" {{ old('status') == 'Inactive'? 'selected':'' }}>Inactive</option>
	</select>
</div>