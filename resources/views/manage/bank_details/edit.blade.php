<input type="hidden" name="id" value="{{ $bank_detail->id }}">

<div class="form-group">
	<label for="source">Bank / Source</label>
	<input type="text" class="form-control" id="source" name="source" placeholder="Enter bank / source" required value="{{ $bank_detail->source }}">
</div>

<div class="form-group">
	<label for="account_no">Account No.</label>
	<input type="text" class="form-control" id="account_no" name="account_no" placeholder="Enter account no" required value="{{ $bank_detail->account_no }}">
</div>

<div class="form-group">
	<label for="holder">Account Holder Name</label>
	<input type="text" class="form-control" id="holder" name="holder" placeholder="Enter holder name" required value="{{ $bank_detail->holder }}">
</div>

<div class="form-group">
	<label for="status">Status</label>
	<select class="form-control" id="status" name="status" required>
		<option value="Active" {{ $bank_detail->status == 'Active'? 'selected':'' }}>Active</option>
		<option value="Inactive" {{ $bank_detail->status == 'Inactive'? 'selected':'' }}>Inactive</option>
	</select>
</div>