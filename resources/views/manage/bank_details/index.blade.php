@extends('layouts.app')
@section('title', 'Bank Details')

@section('style')
@include('css.styles', ['styles' => ['datatable']])
@endsection

@include('components.group')
@section('content')
@component('components.card', ['title' => 'Bank Details List'])
	<div class="row">
		<div class="col-md-12 table-responsive">
			@include('components.alert')
			<a class="btn btn-success btn-sm openAdd float-right" data-href="bank_details/create"><i class="fas fa-plus"></i></a>
			<table class="table table-striped dataTable table-sm table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Bank / Source</th>
						<th>Account No</th>
						<th>Account Holder</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($bank_details as $bank)
						<tr>
							<td>{{ $bank->source }}</td>
							<td>{{ $bank->account_no }}</td>
							<td>{{ $bank->holder }}</td>
							<td>{{ $bank->status }}</td>
							<td>
								<a class="text-warning p-2 openEdit" data-href="bank_details/{{ $bank->id }}/edit">
									<i class="fas fa-edit"></i>
								</a>
								@component('components.action.delete', ['url' => '/manage/bank_details/'.$bank->id])
									<a class="text-danger p-2 onDelete"><i class="fas fa-trash-alt"></i></a>
								@endcomponent
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>	
@endcomponent
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['datatable', 'validate', 'bootbbox']])
@include('components.modal.edit', ['title' => 'Edit Bank Detail', 'url' => 'bank_details/0'])
@include('components.modal.add', ['title' => 'New Bank Detail', 'url' => 'bank_details'])
@endsection
