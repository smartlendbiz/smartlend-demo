@extends('layouts.app')
@section('title', 'Cash Transactions')

@section('style')
@include('css.styles', ['styles' => ['datatable']])
@endsection

@include('components.group')
@section('content')
  @component('components.card', ['title' => 'Cash Transactions'])
    <div class="pl-3 pr-3">
    	@include('components.alert')
    </div>
    <!-- Nav tabs -->
    <ul class="nav md-tabs nav-justified" role="tablist">
      <li class="nav-item waves-effect waves-light" 
      	  onclick="loadContent('/manage/cash_transactions/cash_in?status={{ input('status') }}', '.tab-content')">
        <a class="nav-link" data-toggle="tab" href="#cash_in" role="tab">
        	<span class="d-md-inline d-lg-inline">Cash In</span>
        </a>
      </li>
      <li class="nav-item waves-effect waves-light" 
          onclick="loadContent('/manage/cash_transactions/cash_out?status={{ input('status') }}', '.tab-content')">
        <a class="nav-link" data-toggle="tab" href="#cash_out" role="tab">
        	<span class="d-md-inline d-lg-inline">Cash Out</span>
        </a>
      </li>
    </ul>
    <!-- Tab panels -->
    <div class="tab-content">
      <!--/.Panel 3-->
    </div>
  @endcomponent
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['datatable','bootbbox']])
<script type="text/javascript">
	$(document).ready(function() {
		let hash = window.location.hash || '#cash_in'
		$('a[href="'+hash+'"]').click()
    $('.nav-item').on('click', function() {
      window.location.hash = $(this).find('a').attr('href')
    })      
	})
</script>
@endsection