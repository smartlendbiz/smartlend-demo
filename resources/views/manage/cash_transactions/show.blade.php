<div class="row">
	<div class="col-md-12 table-responsive">
		<span class="float-right mt-2">
			{!! Form::open(['url' => '/manage/cash_transactions#'.$page, 'method' => 'GET', 'class' => 'd-inline']) !!}
			<select class="form-control form-control-sm" name="status" onchange="$(this).parent().submit()">
				<option value="Pending" {{ input('status')=='Pending'?'selected':'' }}>Pending</option>
				<option value="Confirmed" {{ input('status')=='Confirmed'?'selected':'' }}>Confirmed</option>
				<option value="Declined" {{ input('status')=='Declined'?'selected':'' }}>Declined</option>
				<option value="All" {{ input('status')=='All'?'selected':'' }}>All</option>
			</select>
			{!! Form::close() !!}
		</span>
		<table class="table table-sm table-bordered dataTable">
			<thead>
				<tr>
					<th>Date</th>
					<th>Member</th>
					<th>Performed By</th>
					@if($page == 'cash_out')
						<th>Processing Fee</th>
					@endif
					<th>Amount</th>
					@if($page == 'cash_in')
						<th>Bank / Source</th>
					@endif
					@if($page == 'cash_out')
						<th>Method</th>
						<th>Account No / Mobile No</th>
					@endif
					<th>Status</th>
					@if(!in_array(input('status'), ['Confirmed', 'Declined']))
						<th>Action</th>
					@endif
				</tr>
			</thead>
			<tbody>
				@foreach($balances as $balance)
				<tr>
					<td>{{ $balance->created_at }}</td>
					<td>{{ $balance->user->name }}</td>
					<td>{{ $balance->performedBy->name }}</td>
					@if($page == 'cash_out')
						<td>{{ dn($balance->getCashoutFee()) }}</td>
						<td>{{ dn($balance->getReleasedAmount())  }}</td>
					@else
						<td>{{ dn($balance->amount) }}</td>
					@endif
					@if($page == 'cash_in')
						<td>
							{{ $balance->source() }}
							@if($balance->notes)
							<i class="fas fa-info-circle text-info" data-toggle="tooltip"
							data-placement="right" title="{{ $balance->notes }}"></i>
							@endif	
							@if(isset($balance->file))
							<i class="fas fa-paperclip text-primary pointer screenshot" 
							data-href="/cash_in/{{ $balance->id }}"></i>						
							@endif
						</td>
					@endif
					@if($page == 'cash_out')
						<td>{{ $balance->method }}</td>
						<td>
							{{ $balance->account_identifier }}
							<i class="fas fa-info-circle text-info" data-toggle="tooltip"
							data-placement="right" title="{{ $balance->notes }}"></i>
						</td>
					@endif
					<td>
						@if($balance->status == 'Pending') <span class="badge grey">{{ $balance->status }}</span> @endif
						@if($balance->status == 'Confirmed') <span class="badge badge-success">{{ $balance->status }}</span> @endif
						@if($balance->status == 'Declined') <span class="badge badge-warning">{{ $balance->status }}</span> @endif
					</td>
					@if(!in_array(input('status'), ['Confirmed', 'Declined']))
					<td>
						@if($balance->status == 'Pending')
						{!! Form::open(['url' => '/manage/cash_transactions/'.$balance->id."?hash=$page&action=confirmed", 'method' => 'PATCH', 'class' => 'd-inline']) !!}
							<button type="button" class="btn btn-xs btn-success" onclick="confirm($(this).parent(), 'Confirm')">Confirm</button>
						{!! Form::close() !!}
						{!! Form::open(['url' => '/manage/cash_transactions/'.$balance->id."?hash=$page&action=declined", 'method' => 'PATCH', 'class' => 'd-inline']) !!}
							<button type="button" class="btn btn-xs btn-warning" onclick="confirm($(this).parent(), 'Decline')">Decline</button>
						{!! Form::close() !!}		
						@endif	
					</td>
					@endif
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@include('components.modal.nosubmit', ['title' => 'Screenshot', 'trigger' => 'screenshot'])
<script type="text/javascript">
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})		
</script>