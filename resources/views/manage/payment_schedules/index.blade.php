@extends('layouts.app')
@section('title', 'Payment Schedules')

@section('style')
@include('css.styles', ['styles' => ['datatable', 'bootstrap-datepicker']])
@endsection

@include('components.group')
@section('content')
	@include('components.alert')
	@component('components.card', ['title' => 'Payment Transactions'])
	<div class="row">		
		<div class="col-md-12 table-overflow">
			@if(!currentUserGroup()->started)
			<a class="btn btn-success btn-sm openAdd float-right" data-href="/manage/payment_schedules/create"><i class="fas fa-plus"></i></a>
			@endif
			<table class="table table-bordered table-sm dataTable">
				<thead>
					<tr>
						<th>Payment Date</th>
						<th>Expected Total Payment</th>
						<th>Total Payment</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($payment_schedules as $payment_schedule)
					<tr>
						<td>{{ $payment_schedule->payment_date }}</td>
						<td>{{ $payment_schedule->getExpectedTotalPayment() }}</td>
						<td>{{ $payment_schedule->getTotalPaidContribution() }}</td>
						<td>
							@if($payment_schedule->getTotalPaidContribution() == 0)
								@component('components.action.delete', ['url' => '/manage/payment_schedules/'.$payment_schedule->id])
									<a class="text-danger p-2 onDelete"><i class="fas fa-trash-alt"></i></a>
								@endcomponent
							@elseif($payment_schedule->getExpectedTotalPayment() == $payment_schedule->getTotalPaidContribution())								
								<span class="badge badge-success">Completed</span>
							@else
								<span class="badge badge-warning">Started</span>
							@endif
						</td>					
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	@endcomponent
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['datatable','validate', 'bootstrap-datepicker', 'bootbbox']])
@include('components.modal.add', ['title' => 'New payment schedules', 'url' => '/manage/payment_schedules', 'callback' => 'setDatePicker(true)'])	
@endsection