<div class="form-group">
	<div id="datepicker" class="d-flex justify-content-center"></div>
	<input type="text" id="payment_date" name="payment_date" required readonly hidden>	
	<div id="summary" style="display: none">
		<div>Dates Summary:</div>
		<div class="w-100" id="dates">

		</div>
	</div>	
</div>
<script type="text/javascript">
	var span = '<span class="badge badge-success m-1 d-inline-block"></span>'
	function changeValue() {
		var dates = ($('#payment_date').val().trim()).split(',')
		if(dates[0] == '') dates = []
		if(dates.length) {
			$('#summary').show()	
		} else {
			$('#summary').hide()
		}

		$('#dates').html('')
		dates.forEach(function(date) {
			$('#dates').append($(span).html(date))
		})
	}

	$( function() {
		$( "#datepicker" ).datepicker({ multidate: true, format: 'yyyy-mm-dd' });
		$('#datepicker').on('changeDate', function() {
		    $('#payment_date').val(
		        $('#datepicker').datepicker('getFormattedDate')
		    );
		    changeValue()
		});			
	} );	
</script>