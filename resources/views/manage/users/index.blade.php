@extends('layouts.app')
@section('title', 'Users')

@section('style')
@include('css.styles', ['styles' => ['datatable']])
@endsection

@include('components.group')
@section('content')
@component('components.card', ['title' => 'User List'])
	<div class="row">
		<div class="col-md-12 table-responsive">
			@include('components.alert')
			@if(!currentUserGroup()->started)
			{!! Form::open(['url' => '/register_members/generate', 'method' => 'POST', 'class' => 'd-inline']) !!}
				<div class="text-center">	
					<button type="submit" class="btn btn-success btn-sm float-right mr-0">
						Generate New Link
					</button>
				</div>
			{!! Form::close() !!}			
			@endif
			<div >
				<table class="table table-striped dataTable table-sm table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Email</th>
							<th>Contact No.</th>
							<th>Gender</th>
							<th>Role</th>
							<th>No of Share</th>
						</tr>
					</thead>
					<tbody>
						@foreach($users as $user)
							<tr>
								<td>{{ $user->id }}</td>
								<td>{{ $user->name }}</td>
								<td>{{ $user->email }}</td>
								<td>{{ $user->contact_no }}</td>
								<td>{{ $user->gender }}</td>
								<td>{{ $user->displayRoles() }}</td>
								<td>{{ $user->noOfshare(currentUserGroup()) }}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>	
@endcomponent	
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['datatable', 'validate', 'bootbbox']])
<script type="text/javascript">
function copy(element_id){
  var aux = document.createElement("div");
  aux.setAttribute("contentEditable", true);
  aux.innerHTML = document.getElementById(element_id).innerHTML;
  aux.setAttribute("onfocus", "document.execCommand('selectAll',false,null)"); 
  document.body.appendChild(aux);
  aux.focus();
  document.execCommand("copy");
  document.body.removeChild(aux);
  $('#'+element_id).css('color', 'white');
}
</script>
@endsection