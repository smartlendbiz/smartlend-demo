@extends('layouts.app')
@section('title', 'Group Settings')

@include('components.group')
@section('content')
	@component('components.card', ['title' => 'Group Settings'])
    @include('components.alert')
	{!! Form::open(['url' => '/manage/group_settings/update', 'method' => 'PUT', 'class' => 'form-validate']) !!}
    <div class="row">
      <div class="form-group col-md-6">
        <label for="name">Group name</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Enter group name" required value="{{ $group->name }}">
      </div>  
      <div class="form-group col-md-3">
        <label for="amount_per_share">Amount per share (value of 1 share)</label>
        <input type="number" class="form-control" id="amount_per_share" name="amount_per_share" placeholder="Enter a whole number" required value="1000" value="{{ $group->amount_per_share }}">
      </div>  
      <div class="form-group col-md-3">
        <label for="limit">Processing Fee (in %)</label>
        <input type="number" class="form-control" id="processing_fee" name="processing_fee" placeholder="Enter a whole number" required value="{{ $group->processing_fee }}">
      </div>       
		<div class="form-group col-md-3">
			<label for="status">Status</label>
			<select class="form-control" id="status" name="status" required>
				<option value="Active" {{ $group->status=='Active'?'selected':'' }}>Active</option>
				<option value="Inactive" {{ $group->status=='Inactive'?'selected':'' }}>Inactive</option>
			</select>
		</div>     
      <div class="form-group col-md-3">
        <label for="status">Started</label>
        <select class="form-control" id="status" name="started" required>
        <option value="0" {{ $group->started==0?'selected':'' }}>No</option>
        <option value="1" {{ $group->started==1?'selected':'' }}>Yes</option>
        </select>
      </div>  
      <div class="form-group col-md-3">
        <label for="grace_period">Grace period (penalty basis - in days)</label>
        <input type="number" class="form-control" id="grace_period" name="grace_period" placeholder="Enter a whole number" required value="{{ $group->grace_period }}">
      </div>  
      <div class="form-group col-md-3">
        <label for="limit">Loan / Guarantor Limit (in %)</label>
        <input type="number" class="form-control" id="limit" name="limit" placeholder="Enter a whole number" required value="{{ $group->limit }}">
      </div> 
      <div class="col-md-12 text-center">
      	<button class="btn btn-md btn-success" type="submit">Update</button>
      </div>           
    </div><!--/row-->
	{!! Form::close() !!}    		
	@endcomponent
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['validate']])
@endsection