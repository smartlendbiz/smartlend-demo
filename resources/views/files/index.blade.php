<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>File uploads</title>
	<style>
	  * {
	    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto,
	        "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji",
	        "Segoe UI Emoji", "Segoe UI Symbol";
	  }
	</style>
</head>
<body>
<!-- 	<form action="/files" enctype="multipart/form-data" method="POST">
	    <p>
	        <label for="photo">
	            <input type="file" name="photo" id="photo">
	        </label>
	    </p>
	    <button>Upload</button>
	    {{ csrf_field() }}
	</form> -->

         {!! Form::open(array('url' => '/files','files'=>'true')) !!}
         	Select the file to upload
         {!! Form::file('image') !!}
         {!! Form::submit('Upload File') !!}
         {!! Form::close() !!}
</body>
</html>