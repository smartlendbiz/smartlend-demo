@component('components.card', ['title' => 'Release History'])
	<div class="table-responsive">
		@component('components.table')
			@slot('thead')
				<tr>
					<th>Date</th>
                    <th>Released By</th>
                    <th>Amount</th>
				</tr>
			@endslot
			@slot('tbody')
				<tr>
                    <td>{{ $release_receivable->created_at }}</td>
                    <td>{{ $release_receivable->user->name }}</td>
                <td class="text-{{ $release_receivable->amount>0?'success':'danger' }}">{{ dn($release_receivable->amount) }}</td>
				</tr>
			@endslot
		@endcomponent
	</div>
@endcomponent