@extends('layouts.app')
@section('title', 'Dashboard')

@include('components.group')

@section('content')
@if(user()->hasGroup())
	<div class="row">
		<div class="col-md-3">
			<div class="card mb-3">
				<div class="card-body d-flex justify-content-between align-items-center">
					<span class="d-flex flex-column">
						<h6 class="text-muted">Available Balance</h6>
						<h4 class="float-right">{{ dn($available_balance) }}</h4>
					</span>
					<i class="fas fa-wallet fa-3x text-primary"></i>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card mb-3">
				<div class="card-body d-flex justify-content-between align-items-center">
					<span class="d-flex flex-column">
						<h6 class="text-muted">Active Contributions</h6>
						<h4 class="float-right">{{ dn($paid_contributions) }}</h4>
					</span>
					<i class="fas fa-donate fa-3x text-success"></i>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card mb-3">
				<div class="card-body d-flex justify-content-between align-items-center">
					<span class="d-flex flex-column">
						<h6 class="text-muted">Loan Balance</h6>
						<h4 class="float-right">{{ dn($loan_balance) }}</h4>
					</span>
					<i class="fas fa-hand-holding-usd fa-3x text-danger"></i>
				</div>
			</div>
		</div>
		<div class="col-md-3" data-toggle="tooltip" data-placement="bottom" data-html="true"
			title="
				<table>
					<tr>
						<td>Group</td>
						<td>{{ dn($group_dividend) }}</td>
					</tr>
					<tr>
						<td>Personal</td>
						<td>{{ dn($personal_dividend) }}</td>
					</tr>
					<tr>
						<td>Guarantor</td>
						<td>{{ dn($guarantor_dividend) }}</td>
					</tr>
				</table>
			">
			@php
				$total_dividend = $group_dividend + $personal_dividend + $guarantor_dividend;
			@endphp
			<div class="card mb-3">
				<div class="card-body d-flex justify-content-between align-items-center">
					<span class="d-flex flex-column">
						<h6 class="text-muted">Active Dividend <i class="fas fa-info text-info"></i></h6>
						<h4 class="float-right">{{ dn($total_dividend) }}</h4>
					</span>
					<i class="fas fa-chart-pie fa-3x text-warning"></i>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			@include('dashboard.recent_activities')
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-12">
					@include('dashboard.contribution_summaries')
				</div>
				<div class="col-md-12">
					@include('dashboard.loan_list_summaries')
				</div>
			</div>
		</div>
	</div>
@else
	<div class="row">
		<div class="col-md-4">
			<div class="card mb-3">
				<div class="card-body d-flex justify-content-between align-items-center">
					<span class="d-flex flex-column">
						<h6 class="text-muted">Available Balance</h6>
						<h4 class="float-right">{{ dn($available_balance) }}</h4>
					</span>
					<i class="fas fa-wallet fa-3x text-primary"></i>
				</div>
			</div>
		</div>
	</div>
@endif
@endsection

@section('scripts')
<script type="text/javascript">
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
$(function () {
  $('[data-toggle="popover"]').popover()
})
</script>
@endsection
