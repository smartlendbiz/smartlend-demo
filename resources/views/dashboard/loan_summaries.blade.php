@component('components.card', ['title' => 'Group Loan Summary'])
	<div class="table-responsive">
		@component('components.table')
			@slot('thead')
				<tr>
					<th>Loan Type</th>
					<th>Total Amount</th>
					<th>Total Interest</th>
					<th class="pointer" data-toggle="tooltip" title="{{ config('interest.group_rate') }}% of the Total Interest">
						Group Dividend
						<i class="fas fa-question-circle text-primary"></i>
					</th>
					<th class="pointer" data-toggle="tooltip" title="{{ config('interest.personal_rate') }}% of the Total Interest">
						Personal Dividend
						<i class="fas fa-question-circle text-primary"></i>
					</th>
					<th class="pointer" data-toggle="tooltip" title="{{ config('interest.group_rate_guarantor') }}% of the Total Interest">
						Guarantor Dividend
						<i class="fas fa-question-circle text-primary"></i>
					</th>
				</tr>
			@endslot
			@slot('tbody')
				@foreach($loan_details as $type => $detail)
					@php  $bold = $type == 'Total'? 'font-weight-bold': ''; @endphp
					<tr>
						<td class="{{ $bold }}">{{ $type }}</td>
						<td class="{{ $bold }}">{{ dn($detail['total_amount']) }}</td>
						<td class="{{ $bold }}">{{ dn($detail['total_interest']) }}</td>
						<td class="{{ $bold }}">{{ dn($detail['group_interest']) }}</td>
						<td class="{{ $bold }}">{{ dn($detail['personal_interest']) }}</td>
						<td class="{{ $bold }}">{{ dn($detail['guarantor_interest']) }}</td>
					</tr>
				@endforeach
			@endslot
		@endcomponent
	</div>
@endcomponent