<div class="card mb-3">
    <div class="card-header text-light d-flex justify-content-between">
        <span>Contribution Summary</span>
        <a href="/contributions" class="text-light">More</a>
    </div>
    <div class="card-body p-0">
        <table class="table">
            <thead>
                <tr class="transaction-title">
                    <th class="py-2 small border-0">Year</th>
                    <th class="py-2 small border-0">Share</th>
                    <th class="py-2 small border-0">Total Pay</th>
                    <th class="py-2 small border-0">Total Paid</th>
                    <th class="py-2 small border-0 text-center">Complete</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($contribution_lists as $list)
                    <tr>
                        <td class="border-bottom">{{ $list->year }}</td>
                        <td class="border-bottom">{{ $list->no_of_share }}</td>
                        <td class="border-bottom">{{ $list->total_share }}</td>
                        <td class="border-bottom">{{ $list->totalPaid() }}</td>
                        <td class="border-bottom text-center" data-toggle="tooltip" data-placement="top" title="{{ ucfirst($list->status) }}">
                            @switch($list->status)
                                @case('ongoing')
                                    <i class="fas fa-ellipsis-h text-warning"></i>
                                    @break
                                @case('released')
                                    <i class="fas fa-check-circle text-success"></i>
                                    @break
                                @case('cancelled')
                                    <i class="fas fa-times-circle text-danger"></i>
                                    @break
                                @default

                            @endswitch
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td class="border-bottom text-center" colspan="6">No record</td>
                    </tr>
                @endforelse

            </tbody>
        </table>
    </div>
</div>
