@component('components.card', ['title' => 'Account Summary'])
	<div class="table-responsive">
		@component('components.table')
			@slot('thead')
				<tr>
					<th>Year</th>
					<th>Total Contributions</th>
					<th>Group Dividend</th>
					<th>Personal Dividend</th>
					<th>Guarantor Dividend</th>
					<th>Loan Balance</th>
					<th>Guarantor Balance</th>
					<th>Total Receivables</th>
				</tr>
			@endslot
			@slot('tbody')
				<tr>
					<td>2019</td>
					<td class="text-success">{{ dn($paid_contributions) }}</td>
					<td class="text-success">{{ dn($group_dividend) }}</td>
					<td class="text-success">{{ dn($personal_dividend) }}</td>
					<td class="text-success">{{ dn($guarantor_dividend) }}</td>
					<td class="text-{{ ($loan_balance > 0? 'danger': 'success') }}">{{ ($loan_balance>0? '-': '').dn($loan_balance) }}</td>
					<td class="text-{{ ($guarantor_balance > 0? 'danger': 'success') }}">{{ ($guarantor_balance>0? '-': '').dn($guarantor_balance) }}</td>
					<td class="text-{{ ($total_receivables < 0? 'danger': 'success') }}">
						<b>{{ dn($total_receivables) }}</b>
					</td>
				</tr>
			@endslot
		@endcomponent
	</div>
@endcomponent