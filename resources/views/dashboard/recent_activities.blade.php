<div class="card mb-3">
    <div class="card-header text-light d-flex justify-content-between">
        <span>Recent Activities</span>
        <a href="/balance_history" class="text-light">More</a>
    </div>
    <div class="card-body p-0">
        <div class="transaction-title py-2 px-4">
            <div class="d-flex">
              <div class="date-format text-center small"><span class="">Date</span></div>
              <div class="col small">Description</div>
              <div class="col small text-right">Amount</div>
            </div>
        </div>
        <div class="transaction-list">
            @forelse ($transactions as $transaction)
                <div class="transaction-item px-4 py-3">
                    <div class="d-flex align-items-center">
                        <div class="text-center date-format" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
                            <span class="text-4 d-block font-weight-200">{{ $transaction->created_at->format('d') }}</span>
                            <span class="text-1 font-weight-200 text-uppercase">{{ $transaction->created_at->format('M') }}</span>
                        </div>
                        <div class="col-5">
                            <span class="d-block text-3 text-uppercase">{{ $transaction->typeLabel() }}</span>
                            <span class="text-muted small">{{ $transaction->method }}</span>
                        </div>
                        <div class="col text-right">
                            <span class="text-nowrap 
                            @if (!in_array($transaction->type, [1,5,6,8]))
                                text-danger
                            @else
                                text-success
                            @endif
                            ">
                                @if (!in_array($transaction->type, [1,5,6,8]))
                                    - 
                                @endif
                                {{ dn($transaction->amount) }}
                            </span>
                            <small class="d-block text-muted">
                                {{ $transaction->status }}
                            </small>
                        </div>
                    </div>
                </div>
            @empty
                <div class="transaction-item px-4 py-3 text-center text-3">
                    No record
                </div>
            @endforelse
        </div>
    </div>
</div>