<div class="card mb-3">
    <div class="card-header text-light d-flex justify-content-between">
        <span>Loan Summary</span>
        <a href="/loans" class="text-light">More</a>
    </div>
    <div class="card-body p-0">
        <table class="table">
            <thead>
                <tr class="transaction-title">
                    <th class="py-2 small border-0 text-center">Date</th>
                    <th class="py-2 small border-0">Amount</th>
                    <th class="py-2 small border-0">Balance</th>
                    <th class="py-2 small border-0">Total Paid</th>
                    <th class="py-2 small border-0 text-center">Status</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($loans as $loan)
                    <tr>
                        <td class="border-bottom text-center">
                            <span class="text-4 d-block font-weight-200">{{ $loan->created_at->format('d') }}</span>
                            <span class="text-1 font-weight-200 text-uppercase">{{ $loan->created_at->format('M') }}</span>
                        </td>
                        <td class="border-bottom align-middle">{{ dn($loan->amount) }}</td>
                        <td class="border-bottom align-middle">{{ dn($loan->totalBalance()) }}</td>
                        <td class="border-bottom align-middle">{{ dn($loan->totalPaid()) }}</td>
                        <td class="border-bottom align-middle text-center" data-toggle="tooltip" data-placement="top" title="{{ $loan->isPaid()?'Paid':'Unpaid' }}">
                            @if ($loan->isPaid())
                                <i class="fas fa-check-circle text-success"></i>
                            @else
                                <i class="fas fa-ellipsis-h text-warning"></i>
                            @endif
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td class="border-bottom text-center" colspan="5">No record</td>
                    </tr> 
                @endforelse
                
            </tbody>
        </table>
    </div>
</div>