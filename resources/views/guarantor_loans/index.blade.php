@extends('layouts.app')
@section('title', 'Guarantor Loans')

@section('style')
@include('css.styles', ['styles' => ['datatable']])
@endsection

@include('components.group')
@section('content')
@component('components.card', ['title' => 'Guarantor Loans'])
	<div class="row">
		<div class="col-md-12 table-responsive">
			<span class="float-right mt-3">
				<b>Total Guarantor Loan Balance: <span class="text-success">{{ dn($guarantor_loan_balance) }}</span></b>
			</span>
			<table class="table table-striped dataTable table-sm table-bordered table-hover" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Released Date</th>
						<th>Member</th>
						<th>Amount</th>
						<th>Shared Amount</th>
						<th>Shared Balance (+interest)</th>
						<th>Status</th>
					</tr>	
				</thead>
				<tbody>
					@foreach($guarantor_loans as $guarantor_loan)
					<tr>
						<td>{{ $guarantor_loan->loan->released_date  }}</td>
						<td>{{ $guarantor_loan->loan->user->name  }}</td>
						<td>{{ dn($guarantor_loan->loan->amount) }}</td>
						<td>{{ dn($guarantor_loan->amount) }}</td>
						<td>{{ dn($guarantor_loan->guarantorBalance()) }}</td>
						<th>
							{!!
								 $guarantor_loan->guarantorBalance() > 0 
								 ? '<span class="badge badge-warning">Unpaid</span>'
								 : '<span class="badge badge-success">Paid</span>'
							!!}
						</th>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>	
@endcomponent
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['datatable']])
@endsection