{!! Form::open(['url' => route('login'), 'method' => 'POST', 'class' => 'd-inline']) !!}
  <!-- Heading -->
  <h3 class="dark-grey-text text-center">
    <strong>Member Login</strong>
  </h3>
  <hr>

  <div class="form-group">
    <label for="email">Email address</label>
     <input id="email" type="email" placeholder="Email address" 
      class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" 
      name="email" value="{{ 'admin1@smartlend.biz' }}" required autofocus>

        @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
  </div>
  <div class="form-group">
    <label for="password">Password</label>
    <input id="password" type="password" placeholder="Password" value="user1234"
      class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" 
      name="password" required>

    @if ($errors->has('password'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
    @endif      
  </div>
  <div class="text-center">
    <div class="d-flex justify-content-between">
      <div class="form-check mb-2 text-left d-block pl-1">
        <input class="form-check-input" type="checkbox" name="remember" id="remember">
        <label for="remember" class="form-check-label dark-grey-text">Remember me</label>
      </div>

      <a class="btn-link pt-0 dark-grey-text font-italic mr-1" href="{{ route('password.request') }}">
          {{ __('Forgot Your Password?') }}
      </a>
    </div>

    <button class="btn btn-indigo btn-block mt-3">Login</button>
  </div>

  <div class="text-center mt-3">
    email: <strong>admin1@smartlend.biz</strong> | password: <strong>user1234</strong>
  </div>
{!! Form::close() !!}