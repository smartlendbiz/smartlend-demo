@extends('layouts.home')

@section('content')
@include('components.alert')
@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif
<h3 class="dark-grey-text text-center">
    <strong>Send Reset Password Form</strong>
</h3>
<form method="POST" action="{{ route('password.email') }}">
    @csrf
    <div class="form-group">
        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Registered email address">
    </div>

    <button type="submit" class="btn btn-primary btn-block">
        {{ __('Send Password Reset Link') }}
    </button>
</form>
@endsection