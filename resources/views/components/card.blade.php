<div class="card {{ isset($class)? $class: '' }}">
	@if(isset($title))
  	<div class="card-header">
		<div class="card-title text-light d-inline">
			<h5 class="p-0 m-0 d-inline-block">{{ $title }}</h5>
		</div>
		{{ isset($header)? $header: '' }}
  	</div>
  	@endif
	<div class="card-body">
		{{ $slot }}
	</div>
</div>