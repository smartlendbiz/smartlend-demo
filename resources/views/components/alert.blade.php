@if (session('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
  {!! session('success') !!}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if (session('warning'))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
  {!! session('warning') !!}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if (session('danger'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
  {!! session('danger') !!}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif


@if(count($errors->all()))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
  @foreach ($errors->all() as $message)
	 <span class="d-block">{!! $message !!}</span>
  @endforeach
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif