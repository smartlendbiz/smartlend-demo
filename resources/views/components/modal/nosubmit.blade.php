<!-- nosubmit Modal -->
<div class="modal fade" id="{{ $trigger }}Modal" tabindex="-1" role="dialog" aria-labelledby="nosubmitModalLabel" aria-hidden="true">
  <div class="modal-dialog {{ isset($size)? $size: '' }}" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="nosubmitModalLabel">{{ $title }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function() {
    nosubmitModal('{{ $trigger }}')
  })
</script>