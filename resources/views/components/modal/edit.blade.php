<!-- Edit Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="POST" action="{{ $url }}" class="form-edit">
      @csrf 
      @method('PUT')
      <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">{{ $title }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
	    $('.openEdit').on('click',function(){
	        var dataURL = $(this).attr('data-href');
	        $('.modal-body').load(dataURL,function(){
	            $('#editModal').modal({show:true,backdrop:'static'});
              $('.form-edit').validate()
	        });
	    }); 
	});	
</script>