<div class="card">
	<div class="card-body text-center pt-3 pb-4 pl-1 pr-1">
		{{ $slot }}
	</div>
</div>