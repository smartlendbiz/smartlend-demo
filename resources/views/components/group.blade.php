@section('group')
	@if(user()->groups()->count())
	{!! Form::open(['url' => '/default_group', 'method' => 'POST', 'class' => 'd-inline', 'id' => 'group']) !!}
	<select class="form-control form-control-sm" name="gid" onchange="$('#group').submit()">
		@foreach(user()->groups as $g)
		<option value="{{ $g->id }}" 
				{{ user()->default_group_id == $g->id? 'selected': '' }}
		>{{ $g->name }}</option>
		@endforeach
	</select>
	{!! Form::close() !!}		
	@endif
@endsection