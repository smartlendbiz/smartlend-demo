<table class="table table-sm table-bordered table-striped table-hover
			  {{ (isset($dt) && $dt != false) || !isset($dt)? 'dataTable': '' }} 
			  {{ isset($class)? $class: '' }}" 
       width="100%">
	<thead> 
		{{ $thead }}
	</thead>
	<tbody>
		{{ $tbody }}
	</tbody>
</table>