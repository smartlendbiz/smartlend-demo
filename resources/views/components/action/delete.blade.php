{!! Form::open(['url' => $url, 'method' => 'DELETE', 'class' => 'd-inline']) !!}
    {{ $slot }}
{!! Form::close() !!}