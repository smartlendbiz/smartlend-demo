@php
$style_list = [
	'datatable' => 'css/addons/datatables.min.css',
	'icon_font' => 'css/addons/icon_font.css',
	'bootstrap-duallistbox' => 'css/addons/bootstrap-duallistbox.min.css',
	'bootstrap-datepicker' => 'css/addons/bootstrap-datepicker.min.css',
];
@endphp

@foreach($styles as $style)
<link rel="stylesheet" type="text/css" href="{{ asset($style_list[$style]) }}">
@endforeach