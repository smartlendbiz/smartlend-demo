    <!-- Sidebar navigation -->
@if(auth::check())    
    <div id="slide-out" class="side-nav fixed">
      <ul class="custom-scrollbar">
        <!-- Logo -->
        <li class="waves-effect">
          <div class="text-dark">
            <img src="{{ asset('/images/smartlend.png') }}" width="20px" class="ml-3">
            <a href="/" class="d-inline">SmartLend</a>
          </div>
        </li>
        <!--/. Logo -->
        <!--/.Search Form-->
        <!-- Side navigation links -->
        <li>
          <ul class="collapsible collapsible-accordion mt-0">
            <!-- Simple link -->
            <li>
              <a href="/dashboard" class="collapsible-header waves-effect {{ isActiveRoute('dashboard.index') }}">
                <i class="fas fa-tachometer-alt"></i> Dashboard
              </a>
            </li>            
            @if(user()->hasGroup())           
            <li>
              <a href="/cash_in" class="collapsible-header waves-effect {{ isActiveRoute('cash_in.index') }}">
                <i class="fas fa-download"></i> Cash In
              </a>
            </li>
            <li>
              <a href="/cash_out" class="collapsible-header waves-effect {{ isActiveRoute('cash_out.index') }}">
                <i class="fas fa-upload"></i> Cash Out
              </a>
            </li>         
            <li>
              <a href="/send_funds" class="collapsible-header waves-effect {{ isActiveRoute('send_funds.index') }}">
                <i class="fas fa-donate"></i> Send Funds
              </a>
            </li>   
            <li>
              <a href="/contributions" class="collapsible-header waves-effect {{ isActiveRoute('contributions.index') }}">
                <i class="fas fa-donate"></i> Contributions
              </a>
            </li>
            <li>
              <a href="/loans" class="collapsible-header waves-effect {{ isActiveRoute('loans.index') }}">
                <i class="fas fa-hand-holding-usd"></i> Loans
              </a>
            </li>
            <li>
              <a href="/guarantor_loans" class="collapsible-header waves-effect {{ isActiveRoute('guarantor_loans.index') }}">
                <i class="fas fa-handshake"></i> Guarantor Loans
              </a>
            </li>            
            <li>
              <a href="/balance_history" class="collapsible-header waves-effect {{ isActiveRoute('balance_history.index') }}">
                <i class="fas fa-list-ul"></i> Balance History
              </a>
            </li>            
              @if(user()->isCurrentGroupAdmin())
                @include('layouts.menu.overview')
                @include('layouts.menu.manage')
              @endif
            @endif

            @if(user()->hasRole('Super Admin'))
            <li>
              <a class="collapsible-header waves-effect arrow-r 
              {{ isMultiMenuActive(['users.index', 'roles.index', 'groups.index', 'cash_transactions.index']) }}">
                <i class="fas fa-tools"></i> 
                Administration
                <i class="fas fa-angle-down rotate-icon"></i>
              </a>
              <div class="collapsible-body">
                <ul>
                  <li class="current-menu-item">
                    <a href="/users" class="waves-effect {{ isActiveRoute('users.index') }}">Users</a>
                  </li>
                  <li>
                    <a href="/roles" class="waves-effect {{ isActiveRoute('roles.index') }}">Roles</a>
                  </li>
                  <li>
                    <a href="/groups" class="waves-effect {{ isActiveRoute('groups.index') }}">Groups</a>
                  </li>
                  <li>
                    <a href="/cash_transactions" class="waves-effect {{ isActiveRoute('cash_transactions.index') }}">Cash Transactions</a>
                  </li>   
                </ul>
              </div>
            </li>
            @endif
          </ul>
        </li>
        <!--/. Side navigation links -->
      </ul>
      <div class="sidenav-bg mask-strong"></div>
    </div>
    <!--/. Sidebar navigation -->
@endif