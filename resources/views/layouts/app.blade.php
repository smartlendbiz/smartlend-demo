
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>@yield('title')</title>
  <link rel="shortcut icon" type="image/png" href="/images/smartlend.png">
  <!-- Font Awesome -->
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="/css/bootstrap4.min.css">
  <link href="{{ asset('/css/bootstrap-utilities.min.css') }}" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link rel="stylesheet" href="/css/mdb.min.css">
  <link rel="stylesheet" href="/css/loader.css">

  <!-- Your custom styles (optional) -->
  <link rel="stylesheet" href="{{ volatile_asset('/css/custom.css') }}">
  @yield('style')
</head>

<body class="fixed-sn white-skin">
  <div class="sticky-top text-center text-light bg-danger">Demo System</div>
  <!--Main Navigation-->
  <header>
    <div id="cover-spin"></div>
  	@include('layouts.sidebar')
    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg scrolling-navbar double-nav">
      <!-- SideNav slide-out button -->
      <div class="float-left d-flex">
        <a href="#" data-activates="slide-out" class="button-collapse black-text">
          <i class="fas fa-bars"></i>
        </a>
        <span class="ml-3">
          @yield('group') 
        </span>
      </div  
      <!--Navbar links-->
      <ul class="nav navbar-nav nav-flex-icons ml-auto">
        @auth
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle waves-effect" href="#" id="userDropdown" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user"></i> 
            <span class="clearfix d-none d-sm-inline-block">
            	{{ (auth()->user())? auth()->user()->name: '' }}
            	<small>({{ user()? user()->displayRoles(): '' }})</small>
            </span>
          </a>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="/profile">My Profile</a>
            <form action="/logout" method="POST" class="d-inline">
              @csrf
              <button class="dropdown-item" type="submit">Log Out</button>
            </form>
          </div>
        </li>
        @endauth
      </ul>
      <!--/Navbar links-->
    </nav>
    <!-- /.Navbar -->

  </header>
  <!--Main Navigation-->

  <!--Main layout-->
  <main>
    <div class="container-fluid m-0 p-0">

      <!--Section: Basic examples-->
      <section>

        <div class="row">

          <div class="col-md-12">
            @yield('content')
          </div>

        </div>

      </section>
      <!--Section: Basic examples-->

    </div>
  </main>
  <!--Main layout-->

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script src="/js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="/js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="/js/bootstrap.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="/js/mdb.min.js"></script>
  @yield('scripts')
  <!--Custom scripts-->
  <script type="text/javascript" src="/js/custom.js"></script>
</body>

</html>
