<li>
  <a class="collapsible-header waves-effect arrow-r 
  {{ isMultiMenuActive(['users.index', 'cash_transactions.index', 'payment_schedules.index', 'bank_details.index', 'group_settings.index']) }}">
    <i class="fas fa-tools"></i> 
    Manage Group
	    <i class="fas fa-angle-down rotate-icon"></i>
  </a>
  <div class="collapsible-body">
    <ul>
      <li class="current-menu-item">
        <a href="/manage/users" class="waves-effect {{ isActiveRoute('users.index') }}">Users</a>
      </li>
      <li>
        <a href="/manage/cash_transactions" class="waves-effect {{ isActiveRoute('cash_transactions.index') }}">Cash Transactions</a>
      </li>  
      <li>
        <a href="/manage/bank_details" class="waves-effect {{ isActiveRoute('bank_details.index') }}">Bank Details</a>
      </li>        
      <li>
        <a href="/manage/payment_schedules  " class="waves-effect {{ isActiveRoute('payment_schedules.index') }}">Payment Schedules</a>
      </li>   
      <li>
        <a href="/manage/group_settings  " class="waves-effect {{ isActiveRoute('group_settings.index') }}">Settings</a>
      </li>       
    </ul>
  </div>
</li>