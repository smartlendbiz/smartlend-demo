<li>
  <a class="collapsible-header waves-effect arrow-r 
  {{ isMultiMenuActive(['contribution_lists.index', 'balance_transactions.index', 'user_holdings.index', 'cash_out_fees.index', 'summaries.index', 'loan_lists.index']) }}">
    <i class="far fa-eye"></i> 
    Group Overview
	<i class="fas fa-angle-down rotate-icon"></i>
  </a>
  <div class="collapsible-body">
    <ul>
      <li class="current-menu-item">
        <a href="/overview/summaries" class="waves-effect {{ isActiveRoute('summaries.index') }}">Summaries</a>
      </li>      
      <li class="current-menu-item">
        <a href="/overview/release_summaries" class="waves-effect {{ isActiveRoute('release_summaries.index') }}">Release Summaries</a>
      </li>      
      <li class="current-menu-item">
        <a href="/overview/contribution_lists" class="waves-effect {{ isActiveRoute('contribution_lists.index') }}">Contribution Lists</a>
      </li>
      <li class="current-menu-item">
        <a href="/overview/loan_lists" class="waves-effect {{ isActiveRoute('loan_lists.index') }}">Loan Lists</a>
      </li>      
      <li class="current-menu-item">
        <a href="/overview/balance_transactions" class="waves-effect {{ isActiveRoute('balance_transactions.index') }}">Balance Transactions</a>
      </li>      
      <li class="current-menu-item">
        <a href="/overview/user_holdings" class="waves-effect {{ isActiveRoute('user_holdings.index') }}">User Holdings</a>
      </li>    
      <li class="current-menu-item">
        <a href="/overview/cash_out_fees" class="waves-effect {{ isActiveRoute('cash_out_fees.index') }}">Cash Out Fees Summary</a>
      </li>   

    </ul>
  </div>
</li>