
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>@yield('title')</title>
  <!-- Font Awesome -->
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <!-- Material Design Bootstrap -->
  <link rel="stylesheet" href="/css/mdb.min.css">

  <!-- Your custom styles (optional) -->
  <link rel="stylesheet" href="/css/custom.css">
  @yield('style')
</head>

<body class="fixed-sn white-skin">

  <!--Main Navigation-->
  <header>
    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg scrolling-navbar double-nav">
      <!-- SideNav slide-out button -->
      <div class="float-left">
        <a href="#" data-activates="slide-out" class="button-collapse black-text"><i class="fas fa-bars"></i></a>
      </div>
      <!-- Breadcrumb-->
      <div class="breadcrumb-dn mr-auto">
        <p>@yield('title')</p>
      </div>
    </nav>
    <!-- /.Navbar -->

  </header>
  <!--Main Navigation-->

  <!--Main layout-->
  <main>
    <div class="container-fluid mb-5">

      <!--Section: Basic examples-->
      <section>

        <div class="row">

          <div class="col-md-12">

            <div class="card">
              <div class="card-body">
                @yield('content')
              </div>
            </div>

          </div>

        </div>

      </section>
      <!--Section: Basic examples-->

    </div>
  </main>
  <!--Main layout-->



  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script src="/js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="/js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="/js/bootstrap.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="/js/mdb.min.js"></script>
</body>

</html>
