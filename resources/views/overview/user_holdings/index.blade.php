@extends('layouts.app')
@section('title', 'User Holdings')

@section('style')
@include('css.styles', ['styles' => ['datatable']])
@endsection

@include('components.group')
@section('content')
	@component('components.card', ['title' => 'User Holdings'])
		<div class="row">
			<div class="col-md-12 table-responsive">
				<div class="d-flex justify-content-between float-left" style="width:10rem">
					<label class="m-0">Show</label>
					<form action="{{ Request::url() }}?perPage={{ $users->perPage() }}">
						<select class="form-control-sm" name="perPage" onchange="$(this).parent().submit()">
							<option value="10" {{ $users->perPage()==10? 'selected':'' }}>10</option>
							<option value="25" {{ $users->perPage()==25? 'selected':'' }}>25</option>
							<option value="50" {{ $users->perPage()==50? 'selected':'' }}>50</option>
							<option value="{{ $users->total() }}" {{ $users->perPage()==$users->total()? 'selected':'' }}>All</option>
						</select>
					</form>
					<label class="m-0">entries</label>
				</div>
				<table class="table table-sm table-striped table-bordered dataTable">
					<thead>
						<tr>
							@foreach($columns as $column)
								<th>{{ $column['title'] }}</th>
							@endforeach
						</tr>
					</thead>
					<tbody>
						@foreach($users as $user)
							<tr>
								<td>{{ $user->name }}</td>
								<td>{{ $user->noOfshare() }}</td>
								<td>{{ dn($user->balance()) }}</td>
								<td>{{ dn($user->getPendingCash(1)) }}</td>
								<td>{{ dn($user->getPendingCash(2)) }}</td>
								<td>{{ dn($user->getTotalContributions()) }}</td>
								<td>{{ dn($user->totalGroupDividend()) }}</td>
								<td>{{ dn($user->totalPersonalInterest()) }}</td>
								<td>{{ dn($user->totalGuarantorInterest()) }}</td>
								<td>{{ dn($user->loanGuarantorLimit()) }}</td>
								<td>{{ dn($user->loanBalance()) }}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="col-md-12 d-flex justify-content-between">
				<label>Showing {{ $users->firstItem() }} to {{ $users->lastItem() }} of {{ $users->total() }} entries</label>
				{{ $users->links() }}
			</div>
		</div>

	@endcomponent
@endsection


@section('scripts')
@include('js.scripts', ['scripts' => ['datatable']])
<script type="text/javascript">
$.extend( true, $.fn.dataTable.defaults, {
    "paging": false,
    "bInfo": false
} )	
</script>
@endsection