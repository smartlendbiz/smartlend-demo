@extends('layouts.app')
@section('title', 'Contribution Lists')

@section('style')
@include('css.styles', ['styles' => ['datatable']])
@endsection

@include('components.group')
@section('content')
@component('components.card', ['title' => 'Contribution Lists'])
	<div class="row">
		<div class="col-md-12 table-responsive">
			<table class="table table-striped dataTable table-sm table-bordered table-hover dataTable" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Members</th>
						<th>No of Shares</th>
						<th>To Pay</th>
						<th>Paid</th>
						<th>Complete Status</th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $user)
						<tr>
							@php
								$current_contribution = $user->getCurrentContribution();
								@endphp
							<td>{{ $user->name }}</td>
							@if (!!$current_contribution)
								<td>{{ $user->noOfShare() }}</td>
								<td>{{ dn($current_contribution->toPayContribution()) }}</td>
								<td>{{ dn($current_contribution->totalPaid()) }}</td>
								<td>{{ percent($current_contribution->completeStatus()) }}</td>
							@else
								<td class="text-warning" colspan="5">No Active Contribution</td>
							@endif
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endcomponent
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['datatable']])
<script type="text/javascript">
$.extend( true, $.fn.dataTable.defaults, { paging: false, ordering: false });
</script>
@endsection
