@extends('layouts.app')
@section('title', 'Cash Out Fees Summary')

@section('style')
@include('css.styles', ['styles' => ['datatable']])
@endsection

@include('components.group')
@section('content')
@component('components.card', ['title' => 'Cash Out Fees Summary'])
	<div class="row">
		<div class="col-md-12 table-responsive">
			<span class="float-right mt-2">
				<b>Total cashout fees: <span class="text-success">{{ dn($total_cashout_fees) }}</span></b>
			</span>
			<table class="table table-striped dataTable table-sm table-bordered table-hover" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Date</th>
						<th>Member</th>
						<th>Released By</th>
						<th>Cashout Amount</th>
						<th>Released Amount</th>
						<th>Fees</th>
					</tr>	
				</thead>
				<tbody>
					@foreach($cashouts as $cashout)
					<tr>
						<td>{{ $cashout->created_at  }}</td>
						<td>{{ $cashout->user->name  }}</td>
						<td>{{ $cashout->performedBy->name }}</td>
						<td>{{ dn($cashout->amount) }}</td>
						<td>{{ dn($cashout->getReleasedAmount()) }}</td>
						<td>{{ $cashout->getCashoutFee() }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>	
@endcomponent
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['datatable']])
@endsection