@extends('layouts.app')
@section('title', 'Balance Transactions')

@section('style')
@include('css.styles', ['styles' => ['datatable']])
@endsection

@include('components.group')
@section('content')
@component('components.card', ['title' => 'Balance Transactions'])
<div class="row">
	<div class="col-md-12 table-responsive">
		@include('components.alert')
		<a class="btn btn-success btn-sm openAdd float-right" data-href="/overview/balance_transactions/create">
			Transfer Group Balance
		</a>		
		@component('components.table')
			@slot('thead')
				<tr>
					<th>Admin</th>
					<th>Total Cash In</th>
					<th>Total Cash Out</th>
					<th>Total Cash Out Fee</th>
					<th>Transfer / Adjustments</th>
					<th>Group Balance</th>
				</tr>
			@endslot
			@slot('tbody')
				@foreach($admin_balances as $admin)
				<tr>
					<td>{{ $admin['name'] }}</td>
					<td>{{ $admin['total_cash_in'] }}</td>
					<td>{{ $admin['total_cash_out'] }}</td>
					<td>{{ $admin['total_cash_out_fee'] }}</td>
					<td>{{ $admin['transfer'] }}</td>
					<td>{{ $admin['remaining_balance'] }}</td>
				</tr>
				@endforeach
			@endslot
		@endcomponent
	</div>
</div>	
@endcomponent 
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['datatable', 'validate']])
@include('components.modal.add', ['title' => 'Transfer group balance to', 'url' => '/overview/balance_transactions'])
@endsection