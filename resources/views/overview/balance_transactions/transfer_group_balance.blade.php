<div class="form-group">
	<label for="amount">Amount</label>
	<input type="number" class="form-control" id="amount" name="amount" placeholder="Enter an amount" required value="{{ old('amount') }}">
</div>

<div class="form-group">
	<label for="user_id">Transfer to</label>
	<select class="form-control" name="user_id" required>
		<option value="">Select</option>
		@foreach($admins as $admin)
			@if(user()->id != $admin->user->id)
			<option value="{{ $admin->user->id }}">{{ $admin->user->name }}</option>
			@endif
		@endforeach
	</select>
</div>

<div class="form-group">
	<label for="notes">Notes <small class="font-italic">(Not required)</small></label>
	<textarea class="form-control" name="notes">{{ old('notes') }}</textarea>
</div>