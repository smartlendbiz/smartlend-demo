@extends('layouts.app')
@section('title', 'Loan Lists')

@section('style')
@include('css.styles', ['styles' => ['datatable']])
@endsection

@include('components.group')
@section('content')
@component('components.card', ['title' => 'Loan Lists'])
<div class="row">
	<div class="col-md-12 table-responsive">
		@component('components.table')
			@slot('thead')
				<tr>
					<th>#</th>
					<th>Member</th>
					<th>Released Date</th>
					<th>Type</th>
					<th>Amount</th>
					<th>Total Interest</th>
					<th>Total Paid</th>
					<th>Total Balance</th>
					<th>Status</th>
				</tr>
			@endslot
			@slot('tbody')
				@foreach($loans as $loan)
					<tr>
						<td>{{ $loan->id }}</td>
						<td>{{ $loan->user->name }}</td>
						<td>{{ $loan->released_date }}</td>
						<td>{{ $loan->type }}</td>
						<td>{{ dn($loan->amount) }}</td>
						<td>{{ dn($loan->totalInterest()) }}</td>
						<td>{{ dn($loan->totalPaid()) }}</td>
						<td>{{ dn($loan->totalBalance()) }}</td>
						<td>{{ $loan->status }}</td>
					</tr>
				@endforeach
			@endslot
		@endcomponent
	</div>
</div>	
@endcomponent 
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['datatable']])
@endsection