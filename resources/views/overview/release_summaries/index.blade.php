@extends('layouts.app')
@section('title', 'Release Summaries')

@include('components.group')
@section('style')
@include('css.styles', ['styles' => ['datatable']])
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
		@component('components.card', ['title' => 'Account Summaries', 'class' => 'mb-2'])
		<table class="table table-sm table-bordered table-hover dataTable">
			<thead>
                <tr>
					<th>Members</th>
					<th>Total Contributions</th>
					<th>Group Dividend</th>
					<th>Personal Dividend</th>
					<th>Guarantor Dividend</th>
					<th>Loan Balance</th>
					<th>Guarantor Balance</th>
					<th>Total Receivables</th>
				</tr>
            </thead>
            <tbody>
                @php
                    $total_amount_to_release = 0;
                @endphp
                @foreach ($users as $user)    
                @php
                    $total_amount_to_release += $user->total_receivables;
                @endphp
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td class="text-success">{{ dn($user->paid_contribution) }}</td>
                        <td class="text-success">{{ dn($user->group_dividend) }}</td>
                        <td class="text-success">{{ dn($user->personal_dividend) }}</td>
                        <td class="text-success">{{ dn($user->guarantor_dividend) }}</td>
                        <td class="text-{{ ($user->loan_balance > 0? 'danger': 'success') }}">{{ dn($user->loan_balance) }}</td>
                        <td class="text-{{ ($user->guarantor_balance > 0? 'danger': 'success') }}">{{ dn($user->guarantor_balance) }}</td>
                        <td class="text-{{ $user->total_receivables>0? 'success':'danger' }}">{{ dn($user->total_receivables) }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
		@endcomponent 
	</div>	
</div>	    
@endsection

@section('scripts')
@include('js.scripts', ['scripts' => ['datatable']])
<script type="text/javascript">
    $.extend( true, $.fn.dataTable.defaults, { paging: false });	
    $(document).ready(function() {
        setTimeout(function() {
            $('.dataTables_info').parent().next().html('<span class="float-right">{{dn($total_amount_to_release)}}</span>');
        },1000)
    })
</script>
@endsection