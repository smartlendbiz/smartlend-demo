@extends('layouts.app')
@section('title', 'Summaries')

@include('components.group')
@section('content')
<div class="row">
	<div class="col-md-12">
		@component('components.card', ['title' => 'Balance', 'class' => 'mb-2'])
		<table class="table table-sm table-bordered table-hover">
			<tbody>
				<tr>
					<td>Pending Cash In</td>
					<td class="w-25">{{ dn($pending_cash_in) }}</td>
				</tr>
				<tr>
					<td>Pending Cash Out</td>
					<td class="w-25">{{ dn($pending_cash_out) }}</td>
				</tr>
				<tr>
					<td>Total Cash In</td>
					<td class="w-25">{{ dn($total_cash_in) }}</td>
				</tr>
				<tr>
					<td>Total Cash Out</td>
					<td class="w-25">{{ dn($total_cash_out_released) }}</td>
				</tr>
				<tr>
					<td>Total Cash Out Fees</td>
					<td class="w-25">{{ dn($cash_out_fees) }}</td>
				</tr>	
				<tr>
					<td>Members Available Balance</td>
					<td class="w-25">{{ dn($users_available_balance) }}</td>
				</tr>								
				<tr>
					<td>Total</td>
					<td class="w-25">{{ dn($total) }}</td>
				</tr>				
			</tbody>
		</table>
		@endcomponent 
	</div>
	<div class="col-md-12">
		@component('components.card', ['title' => 'Contributions', 'class' => 'mb-2'])
		<table class="table table-sm table-bordered table-hover">
			<tbody>
				<tr>
					<td>Total Paid</td>
					<td class="w-25">{{ dn($total_paid_contributions) }}</td>
				</tr>
				<tr>
					<td>Unpaid</td>
					<td class="w-25">{{ dn($total_unpaid_contributions) }}</td>
				</tr>
				<tr>
					<td>Total</td>
					<td class="w-25">{{ dn($total_contributions) }}</td>
				</tr>						
			</tbody>
		</table>
		@endcomponent 
	</div>	
	<div class="col-md-12">
		@component('components.card', ['title' => 'Loans', 'class' => 'mb-2'])
		<table class="table table-sm table-bordered table-hover">
			<tbody>
				<tr>
					<td>Total Loans + Total Interest</td>
					<td class="w-25">{{ dn($total_loans) }}</td>
				</tr>
				<tr>
					<td>Total Interest</td>
					<td class="w-25">{{ dn($total_interests) }}</td>
				</tr>
				<tr>
					<td>Total Loan Paid</td>
					<td class="w-25">{{ dn($total_loan_paid) }}</td>
				</tr>				
				<tr>
					<td>Total Balance</td>
					<td class="w-25">{{ dn($total_loan_balance) }}</td>
				</tr>							
			</tbody>
		</table>
		@endcomponent 
	</div>		
</div>	
@endsection
