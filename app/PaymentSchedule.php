<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;
use App\Balance;

class PaymentSchedule extends Model
{
    protected $fillable = ['payment_date', 'group_id'];
    protected $appends = ['due_date', 'amount_per_share', 'user_share'];

    public function group()
    {
    	return $this->belongsTo('App\Group');
    }

    public function getDueDateAttribute()
    {
        $payment_date = Carbon::parse($this->payment_date);
        return $payment_date->addDay($this->group->grace_period)->format('Y-m-d');        
    }

    public function getAmountPerShareAttribute()
    {
    	return $this->group->amount_per_share;
    }

    public function getUserShareAttribute()
    {
    	return auth()->user()->share($this->group->id);
    }

    public function getAmountToPayAttribute() 
    {
    	return $this->amount_per_share * $this->user_share;
    }

    public function contribution()
    {
        return $this->hasOne('App\Contribution');
    }     

    public function balance($user_id = null)
    {
        if(!$user_id) $user_id = user()->id;

        return Balance::join('contributions', 'contributions.balance_id', '=', 'balances.id')
               ->select('balances.*')
               ->where('balances.user_id', $user_id)->where('balances.type', 3)
               ->where('contributions.payment_schedule_id', $this->id)->first();
    }

    public function isPaid($user_id = null)
    {
        return $this->balance($user_id)? true: false;
    }

    public function getExpectedTotalPayment()
    {
        return $this->amount_per_share * currentUserGroup()->totalShares();   
    }

    public function getTotalPaidContribution()
    {
        $contributions = $this->contribution()->where('payment_schedule_id', $this->id)->get();

        $total_amount = 0;
        foreach ($contributions as $contribution) {
            $total_amount += $contribution->balance? $contribution->balance->amount: 0;
        }

        return abs($total_amount);
    }
}
