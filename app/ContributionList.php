<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContributionList extends Model
{
    protected $table = 'contribution_lists';
    protected $fillable = ['user_id', 'group_id', 'year', 'no_of_share', 'amount_per_share', 'total_share', 'status'];

    public function contributionPayments()
    {
    	return $this->hasMany('App\ContributionListPayment');
    }

    public function totalPaid()
    {
    	return $this->contributionPayments->sum('amount');
    }

    public function payPerShare()
    {
        return $this->amount_per_share * $this->no_of_share;
    }

    public function toPayContribution()
    {
        return $this->payPerShare() * 24;
    }

    public function completeStatus()
    {
        return ($this->totalPaid() / $this->toPayContribution()) * 100;
    }
}
