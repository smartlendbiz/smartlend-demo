<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupBalance extends Model
{
    protected $fillable = ['group_id', 'user_id', 'amount', 'notes'];
}
