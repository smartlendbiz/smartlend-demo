<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;

class Loan extends Model
{
    protected $fillable = ['user_id', 'group_id', 'amount', 'status', 'released_date', 'type', 'notes'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function group()
    {
        return $this->belongsTo('App\Group');
    }

    public function loanPayments()
    {
    	return $this->hasMany('App\LoanPayment');
    }

    public function totalPaid($until_date = null)
    {
        if($until_date) {
            $until_date_convert  = Carbon::parse($until_date);
            $until_date_plus_one = $until_date_convert->addDay();
            return $this->loanPayments()->where('created_at', '<', df($until_date_plus_one))->sum('amount');
        }

    	return $this->loanPayments()->sum('amount');
    }

    public function loanInterests()
    {
    	return $this->hasMany('App\LoanInterest');
    }

    public function totalInterest()
    {
    	return $this->loanInterests()->sum('amount');
    }

    public function totalBalance()
    {
        return $this->amount + $this->totalInterest() - $this->totalPaid();
    }

    public function isPaid()
    {
        return $this->totalBalance() == 0;
    }

    public function myLoan()
    {
        return $this->user->loans()->where('id', $this->id)->exists();
    }

    public function loanGuarantors()
    {
        return $this->hasMany('App\LoanGuarantor');
    }

    public function hasGuarantor()
    {
        return $this->loanGuarantors()->count() > 0;
    }

    public function totalGuarantorAmount()
    {
        return $this->loanGuarantors()->sum('amount');
    }

    public function guarantorAmountReady()
    {
        return $this->amount == $this->totalGuarantorAmount();
    }

    public function guarantorAmount($user_id)
    {
        $guarantor = $this->loanGuarantors()->where('user_id', $user_id)->first();
        return $guarantor? $guarantor->amount: 0;
    }
}
