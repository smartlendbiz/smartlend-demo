<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReleaseReceivable extends Model
{
    protected $fillable = ['user_id', 'balance_id', 'amount'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function balance()
    {
    	return $this->belongsTo('App\Balance');
    }
}
