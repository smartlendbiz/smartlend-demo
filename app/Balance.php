<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    protected $fillable = ['user_id', 'amount', 'notes', 'method', 'type', 'performed_by', 'status', 'account_identifier', 'purpose'];
	public $balance_types = [
		1 => 'Cash In',
		2 => 'Cash Out',
		3 => 'Contribution Payment',
        4 => 'Loan Payment',
        5 => 'Loan Release',
        6 => 'Release Receivable',
        7 => 'Send Funds',
        8 => 'Receive Funds'
	];    

    public function file()
    {
        return $this->hasOne('App\File');
    }

    public function typeLabel()
    {
    	return $this->balance_types[$this->type];
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function performedBy()
    {
    	return $this->belongsTo('App\User', 'performed_by');
    }    

    public function contribution()
    {
        return $this->hasOne('App\Contribution');
    }        

    public function getAmountAttribute($value)
    {
        return abs($value);
    }

    public function loanPayment()
    {
        return $this->hasOne('App\LoanPayment');
    }

    public function balanceBankDetail()
    {
        return $this->hasOne('App\BalanceBankDetail');
    }

    public function bankDetail()
    {
        return $this->balanceBankDetail? $this->balanceBankDetail->bankDetail: null;   
    }

    public function source()
    {
        $detail = $this->bankDetail();
        if($detail) {
            return "($detail->source - $detail->account_no) $detail->holder";
        }

        return 'Cash';
    }

    public function getCashoutFee()
    {
        // $processing_fee =  df($this->created_at) < df('2020-01-01 00:00:00')? 1: currentUserGroup()->processing_fee;

        if(df($this->created_at) < df('2020-01-01 00:00:00')) {
            return calculatePercentage($this->amount, 1);
        } else {
            return 50;
        }
    }

    public function getReleasedAmount()
    {
        return $this->amount - $this->getCashoutFee();
    }
}
