<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContributionListPayment extends Model
{
    protected $table = 'contribution_list_payments';
    protected $fillable = ['contribution_list_id', 'amount'];
}

