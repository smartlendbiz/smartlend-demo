<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankDetail extends Model
{
    protected $fillable = ['account_no', 'holder', 'status', 'group_id', 'source'];   

    public function groups()
    {
    	return $this->belongsTo('App\Group');
    }

    public function balanceBankDetails()
    {
    	return $this->hasMany('App\BalanceBankDetail');
    }

    public function totalCashIn()
    {
    	$sum = 0;
    	foreach ($this->balanceBankDetails as $balance_bank_detail) {
    		$sum += $balance_bank_detail->cashIn();
    	}

    	return $sum;
    }

    public function totalCashOut()
    {
    	$sum = 0;
    	foreach ($this->balanceBankDetails as $balance_bank_detail) {
    		$sum += $balance_bank_detail->cashOut();
    	}

    	return $sum;
    }    

    public function totalCash()
    {
    	return $this->totalCashIn() - $this->totalCashOut();
    }
}
