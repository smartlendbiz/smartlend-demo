<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Traits\InterestTrait;

class UpdateInterest extends Command
{
    use InterestTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smartlend:updateInterest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will calculate the latest interest of a loan.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->calculateLoanInterest();           
    }
}
