<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use App\Balance;
use App\ReleaseReceivable;
use DB;

class ReleaseReceivablesCmd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'receivable:release';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add available balance and record release receivables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(Storage::has('release_summary.php')) {
            $data = json_decode(Storage::get('release_summary.php'));
        } else {
            $this->error("No data found in release_summary.php");
            return false;
        }

        DB::beginTransaction();
        $this->output->progressStart(count($data));
        $this->info("Job Starting".PHP_EOL);
        foreach($data as $user) {
            $balance = Balance::firstOrCreate([
                'user_id' => $user->id,
                'amount' => $user->total_receivables,
                'notes' => '2019 Receivables',
                'method' => 'System Generated',
                'type' => 6,
                'performed_by' => 2,
                'status' => 'Confirmed'
            ]);
    
            ReleaseReceivable::firstOrCreate([
                'user_id' => $user->id,
                'balance_id' => $balance->id,
                'amount' => $user->total_receivables
            ]);

            $this->output->progressAdvance();
        }

        $this->output->progressFinish();
        $this->info("Job is done".PHP_EOL);

        DB::commit();
    }
}
