<?php

namespace App\Http\Middleware;

use Closure;

class CheckGroup
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!user()->hasGroup()) {
            return redirect('/dashboard');
        }

        return $next($request);
    }
}
