<?php

namespace App\Http\Controllers\manage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentScheduleController extends Controller
{
	protected $data = [];

	public function index()
	{
		$this->data['payment_schedules'] = currentUserGroup()->paymentSchedules;
		return view('manage.payment_schedules.index', $this->data);
	}

	public function create()
	{
	 return view('manage.payment_schedules.create');
	}

	public function store(Request $request)
	{

		$redirect = redirect()->back();
		$payment_dates = explode(',', $request->payment_date);
		if(count($payment_dates)) {
			$ctr = 0;
			foreach ($payment_dates as $payment_date) {
				if($payment_date) {
					$ctr++;
					currentUserGroup()->paymentSchedules()->firstOrCreate(['payment_date' => $payment_date]);
				}
			}

			if($ctr) {
            	return $redirect->with('success', 'Payment schedule was added successfully.');  	
			} else {
				return $redirect->with('danger', 'No date was selected.');				
			}
		}

       	return $redirect->with('danger', 'Unable to add payment schedule.');

	}

    public function destroy($id)
    {
        $delete_success = currentUserGroup()->paymentSchedules()->where('id', $id)->delete();
        $redirect = redirect()->back();

        if($delete_success) {
            return $redirect->with('success', 'Schedule was deleted successfully.');   
        } else {
            return $redirect->with('danger', 'Schedule was not deleted.');   
        }
    }	
}
