<?php

namespace App\Http\Controllers\manage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Balance;
use App\Traits\BalanceTrait;

class CashTransactionController extends Controller
{
    use BalanceTrait;

    protected $data = [];

    public function index()
    {
        return view('manage.cash_transactions.index');
    }

    public function show($page)
    {
        if(in_array($page, ['cash_in', 'cash_out'])) {
            $status = input('status');
            if($page == 'cash_in') $balances = $this->cashEntries(1, $status);
            if($page == 'cash_out') $balances = $this->cashEntries(2, $status);

            return view('manage.cash_transactions.show', compact('balances', 'page'));
        }
    }

    public function update(Request $request, $id)
    {
        $redirect = '/manage/cash_transactions#'.$request->hash;
        $type = $request->hash == 'cash_in'? 'Cash in': 'Cash out';

        if(in_array($request->action, ['confirmed', 'declined'])) {
            $action = $request->action;
            $balance = Balance::find($id);

            if(!!!$balance) {
                redirect($redirect)->with('danger', "Item not found.");
            }

            if($balance->purpose == 'contributionPayment' && $request->action == 'confirmed') {
                $current_contribution = $balance->user->getCurrentContribution();
                $no_of_share = floor($balance->amount / $current_contribution->payPerShare());
                $this->payContribution($current_contribution, $no_of_share, 'Auto Generated', $balance->user_id);
            }

            $balance->update(['status' => ucfirst($action), 'performed_by' => user()->id]);

            // $this->notification($balance);
            return redirect($redirect)->with('success', "$type was $action.");
        }

        return redirect($redirect)->with('danger', 'Unable to process the action.');

    }
}
