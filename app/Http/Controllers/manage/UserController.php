<?php

namespace App\Http\Controllers\manage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
	protected $data = [];

    public function index()
    {
    	$this->data['users'] = currentUserGroup()->users;
    	
    	return view('manage.users.index', $this->data);
    }
}
