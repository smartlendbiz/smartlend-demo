<?php

namespace App\Http\Controllers\manage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\MasterController;
use App\BankDetail;
use DB;

class BankDetailController extends MasterController
{
   	function __construct()
   	{
   		$this->model = new BankDetail;
	    $this->redirect = '/manage/bank_details';
	    $this->view_folder = 'manage.bank_details';
	    $this->variable_name = 'bank_details';
	    $this->model_label = 'BankDetail';
	    $this->single_model = 'bank_detail';
	    $this->rules = [
	        'account_no' => 'required|unique:bank_details', 
	        'holder' => 'required', 
	        'status' => 'required', 
	        'source' => 'required', 
	    ];   		
   	} 

    public function store(Request $request)
    {
        $data = $request->only(array_keys($this->rules));
        $validate = $this->validator($data, $this->rules);

        if (!$validate->fails()) {
        	currentUserGroup()->bankDetails()->create($data);
        } else {
            return redirect($this->redirect)->withErrors($validate)->withInput();
        }

        return redirect($this->redirect)->with('success', $this->getLabel().' was added successfully.');
    }     

    public function update(Request $request, $id)
    {
        $id = $request->id;

        $this->rules['account_no'] .= ($id ? ",id,$id" : '');
        $data = $request->only(array_keys($this->rules));
        $validate = $this->validator($data, $this->rules);

        if (!$validate->fails() && isset($id)) {
        	$has_duplicate_role = BankDetail::where('account_no', $data['account_no'])->whereNotIn('id', [$id])->exists();

            if(!$has_duplicate_role)  {
                BankDetail::where('id', $id)->update($data);
            } else {
                return redirect()->back()->with('danger', 'Account no. is already taken.');    
            }
        } else {
            return redirect()->back()->withErrors($validate)->withInput();
        }

        return redirect()->back()->with('success', 'Bank Detail was updated successfully.');
    }     	
}
