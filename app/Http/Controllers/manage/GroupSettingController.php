<?php

namespace App\Http\Controllers\manage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\MasterController;
use App\Group;

class GroupSettingController extends Mastercontroller
{
    protected $rules = [
        'name' => 'required',
        'amount_per_share' => 'required',
        'grace_period' => 'required',
        'limit' => 'required',
        'status' => 'required',
        'started' => 'required',
        'processing_fee' => 'required'
    ];

    public function index()
    {
    	$group = currentUserGroup();
    	return view('manage.group_settings.index', compact('group'));
    }

    public function update(Request $request, $id)
    {
    	if($id != 'update') return redirect()->back();

        $data = $request->only(array_keys($this->rules));
        $validate = $this->validator($data, $this->rules);

        if (!$validate->fails()) {
        	$group = currentUserGroup();
        	$find_duplicate = Group::where('name', $data['name'])->where('id', '<>', $group->id)->first();

        	if($find_duplicate) {
        		return redirect()->back()->with('danger', 'Group "'.$data['name'].'" is not available.');
        	}

    		$group->update($data);
        } else {
            return redirect()->back()->withErrors($validate)->withInput();
        }

        return redirect()->back()->with('success', 'Group was updated successfully.');
    }
}
