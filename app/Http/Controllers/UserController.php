<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use DB;

class UserController extends MasterController
{
    protected $data = [];
    protected $rules = [
        'name' => 'required', 
        'email' => 'required|email|unique:users', 
        'contact_no' => 'required', 
        'gender'  => 'required',
        'role_id' => 'required',
    ];

    public function index()
    {
        $this->setData();
        return view('users.index', $this->data);
    }

    public function create()
    {
        $roles = $this->getRoles();
        return view('users.create', compact('roles'));
    }

    public function store(Request $request)
    {
        $data = $request->only(array_keys($this->rules));
        $validate = $this->validator($data, $this->rules);

        if (!$validate->fails()) {
            if(!validMobileNumber($request->contact_no)) {
                return redirect()->back()->with("danger", "Invalid mobile number.");   
            }

            DB::beginTransaction();
            $data['password'] = bcrypt('user1234');
            $role_id = $data['role_id'];
            unset($data['role_id']);
            $user = User::create($data);

            $role = Role::find($role_id);

            if($role) {
                $user->roles()->attach($role->id);
            }
            DB::commit();
        } else {
            return redirect('/users')->withErrors($validate)->withInput();
        }

        return redirect('/users')->with('success', 'User was added successfully.');
    }

    public function edit($id)
    {
        $user = User::find($id);
        $roles = $this->getRoles();
        return view('users.edit', compact('user', 'roles'));
    }

    public function update(Request $request, $id)
    {
        $id = $request->id;
        $this->rules['status'] = 'required';
        $this->rules['email'] .= ($id ? ",id,$id" : '');

        $data = $request->only(array_keys($this->rules));
        $validate = $this->validator($data, $this->rules);

        if (!$validate->fails() && isset($id)) {
            if(!validMobileNumber($request->contact_no)) {
                return redirect()->back()->with("danger", "Invalid mobile number.");   
            }
            
            $has_duplicate_email = User::where('email', $data['email'])->whereNotIn('id', [$id])->exists();
            if(!$has_duplicate_email)  {
                $role_id = $data['role_id'];
                unset($data['role_id']);

                $user = User::find($id);
                $user->roles()->detach();
                $user->roles()->attach($role_id);
                $user->update($data);
            } else {
                return redirect('/users')->with('danger', 'Email address is already taken.');    
            }
        } else {
            return redirect('/users')->with('danger', 'Unable to update the user. Try again.');
        }

        return redirect('/users')->with('success', 'User was updated successfully.');
    }

    public function destroy($id)
    {
        $delete_success = User::destroy($id);
        if($delete_success) {
            return redirect('/users')->with('success', 'User was deleted successfully.');   
        } else {
            return redirect('/users')->with('danger', 'User was not deleted.');   
        }
    }

    public function setData()
    {
        $this->data['users'] = User::all();
    }

    public function defaultGroup(Request $request)
    {
        user()->update(['default_group_id' => $request->gid]);
        return redirect()->back();
    }

    public function getRoles()
    {
        return Role::where('name', '<>', 'Super Admin')->get();
    }
}
