<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
	protected $data = [];

    public function index()
    {
    	$group = currentUserGroup();
        $user  = user();

        $this->data['available_balance'] = $user->balance();
        $this->data['transactions']      = $user->getAllBalances()->orderBy('created_at', 'desc')->take(5)->get();
        $this->data['loans']             = $user->loans()->orderBy('created_at', 'desc')->take(3)->get();

    	if($group) {
            $this->data['paid_contributions'] = $user->getTotalActiveContributions();
            $this->data['loan_balance']       = $user->loanBalance();
            $this->data['contribution_lists'] = $user->getContributionLists;
            $this->data['group_dividend']         = $user->totalGroupDividend();
            $this->data['personal_dividend']      = $user->totalPersonalInterest();
            $this->data['guarantor_dividend']     = $user->totalGuarantorInterest();


	    // 	$this->data['total_loan_balance']     = $group->totalLoan(user()->id);
        //     $this->data['total_share']            = $group->totalNoOfShare();
        //     $this->data['latest_dividend']        = $group->latestDividend();
        //     $this->data['amount_for_loan']        = $group->amountForLoan();

        //     $this->data['expected_contributions'] = $user->expectedTotalContribution();
        //     $this->data['no_of_share']            = $user->noOfshare();
        //     $this->data['loan_guarantor_limit']   = $user->loanGuarantorLimit();
        //     $this->data['guarantor_balance']      = $user->guarantorLoanBalance();
        //     $this->data['release_receivable']     = $user->releaseReceivable;

        //     $this->setLoanSummary();
        //     $this->setAccountSummary();
    	}

    	return view('dashboard.index', $this->data);
    }

    private function setAccountSummary() {
        $this->data['total_receivables'] = $this->data['paid_contributions']
                                         + $this->data['personal_dividend']
                                         + $this->data['group_dividend']
                                         + $this->data['guarantor_dividend']
                                         - $this->data['loan_balance'];
    }

    private function setLoanSummary()
    {
        $group = currentUserGroup();

        $this->data['loan_details']['Total'] = [
            'total_amount' => 0,
            'total_interest' => 0,
            'group_interest' => 0,
            'personal_interest' => 0,
            'guarantor_interest' => 0
        ];

        foreach (['Regular', 'Guarantor'] as $type) {
            $total_amount = $group->totalLoan(null, $type);
            $total_interest = $group->totalInterest(null, $type);

            if($type == 'Regular') {
                $group_interest = calculatePercentage($total_interest, config('interest.group_rate'));
                $personal_interest = calculatePercentage($total_interest, config('interest.personal_rate'));
                $guarantor_interest = 0;
            }

            if($type == 'Guarantor') {
                $group_interest = calculatePercentage($total_interest, config('interest.group_rate_guarantor'));
                $personal_interest = 0;
                $guarantor_interest = calculatePercentage($total_interest, config('interest.guarantor_rate'));
            }

            $this->data['loan_details'][$type] = [
                'total_amount' => $total_amount,
                'total_interest' => $total_interest,
                'group_interest' => $group_interest,
                'personal_interest' => $personal_interest,
                'guarantor_interest' => $guarantor_interest
            ];

            $this->data['loan_details']['Total']['total_amount'] += $total_amount;
            $this->data['loan_details']['Total']['total_interest'] += $total_interest;
            $this->data['loan_details']['Total']['group_interest'] += $group_interest;
            $this->data['loan_details']['Total']['personal_interest'] += $personal_interest;
            $this->data['loan_details']['Total']['guarantor_interest'] += $guarantor_interest;
        }

        $this->data['loan_details'] = array_reverse($this->data['loan_details'], true);
    }
}
