<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\BalanceTrait;
use App\Balance;

use DB;

class CashInController extends MasterController
{
    use BalanceTrait;

    function __construct()
    {
        $this->model = new Balance;
        $this->redirect = '/cash_in';
        $this->view_folder = 'cash_ins';
        $this->variable_name = 'cash_ins';
        $this->model_label = 'Cash In';
        $this->rules = [
            'amount' => 'required|integer|min:1',
            'notes' => '',
            'method' => 'required',
            'screenshot' => 'required|mimes:jpeg,jpg,png',
            'purpose' => 'required'
        ];   
    } 

    public function index()
    {
        $cash_ins = $this->userCashInData();
        return view($this->view_folder.'.index', compact('cash_ins'));
    }    

    public function store(Request $request)
    {
        $is_fund_transfer = $request->method == 'Fund Transfer';
        if($is_fund_transfer) {
            $this->rules['bank_detail_id'] = 'required';
        }

        $data = $request->only(array_keys($this->rules));
        $validate = $this->validator($data, $this->rules);
        
        if (!$validate->fails()) {
            if($is_fund_transfer) {
                $bank_detail = currentUserGroup()->bankDetails()->find($request->bank_detail_id);            

                if(!$bank_detail) {
                    return redirect()->back()->with('danger', 'Something went wrong. Contact admin.');
                }
            }

            if(!$data['notes']) $data['notes'] = '';
            $result = $this->cashIn($data, $request);

            if($result == 'no_active_contribution') {
                return redirect()->back()->with('danger', 'No Active Contribution. Set number of share. <a href="/contributions">Click Here</a>');
            }

            if($result == 'not_valid_auto') {
                return redirect()->back()->with('warning', 'Amount is not valid for auto contribution payment. You may select TopUp Balance to proceed.');
            }
        } else {
            return redirect($this->redirect)->withErrors($validate)->withInput();
        }

        return redirect($this->redirect)->with('success', 'Cash in was added successfully. This will be processed as soon as possible.');
    }

    public function show($id)
    {
        $balance = Balance::find($id);
        $screenshot = $balance? $balance->file->path: '';

        $screenshot = str_replace('public', '/storage', $screenshot);

        return view('cash_ins.preview', compact('screenshot'));
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        $delete_success = $this->model::find($id);
        if(isset($delete_success->file)) $delete_success->file->delete();
        $delete_success->delete();
        DB::commit();

        if($delete_success) {
            return redirect($this->redirect)->with('success', $this->getLabel().' was deleted successfully.');   
        } else {
            return redirect($this->redirect)->with('danger', $this->getLabel().' was not deleted.');   
        }
    }    
}
