<?php

namespace App\Http\Controllers\overview;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\MasterController;
use App\GroupBalance;

class BalanceTransactionController extends MasterController
{
	protected $data = [];
    protected $rules = [
        'group_id' => 'required',
        'user_id' => 'required',
        'amount' => 'required|integer|min:1',
        'notes' => ''
    ];

    public function index()
    {
    	$group = currentUserGroup();
    	$this->data['bank_details'] = $group->bankDetails;
    	$this->setTotalGroupBalance();
        $this->setAdminTransactions();

    	return view('overview.balance_transactions.index', $this->data);
    }

    public function create()
    {
        // Transfer group balance from one admin to another
        $admins = currentUserGroup()->userAdmins();
        return view('overview.balance_transactions.transfer_group_balance', compact('admins'));
    }

    public function store(Request $request)
    {
        $data = $request->only(array_keys($this->rules));
        $data['group_id'] = currentUserGroup()->id;
        $validate = $this->validator($data, $this->rules);

        $to_user = user()->where('id', $data['user_id'])->first();

        if(!$to_user) {
            return redirect()->back()->with('danger', 'Unable to find user.');
        }

        if (!$validate->fails()) {
            $from = $to = $data;

            $from['amount'] *= -1;
            $from['user_id'] = user()->id;

            GroupBalance::create($from);
            GroupBalance::create($to);
        } else {
            return redirect()->back()->withErrors($validate)->withInput();
        }

        return redirect()->back()->with('success', $data['amount'].' was transferred to '.$to_user->name);
    }

    private function setTotalGroupBalance()
    {
    	$this->data['total_group_balance'] = 0;
    	foreach ($this->data['bank_details'] as $bank_detail) {
    		$this->data['total_group_balance'] += $bank_detail->totalCash();
    	}
    }

    private function setAdminTransactions()
    {
        $users = currentUserGroup()->users()->get();
        $cashout_fee = 50;

        $cash_transactions = [];
        foreach ($users as $user) {
            $balances = $user->getAllBalances()->whereIn('type', [1,2])->where('status', 'Confirmed')->get();
            foreach ($balances as $balance) {

                $key = $balance->performedBy->name.'||'.$balance->performedBy->id;
                if(!isset($cash_transactions[$key])) {
                    $cash_transactions[$key][1] = 0;
                    $cash_transactions[$key][2] = 0;
                    $cash_transactions[$key][3] = 0; // Cashout fee
                }

                $cash_transactions[$key][3] += $balance->type == 2? $cashout_fee: 0;
                $cash_transactions[$key][$balance->type] += abs($balance->amount);
            }

        }

        $admin_balances = [];
        foreach ($cash_transactions as $admin => $ct) {
            list($user_name, $user_id) = explode('||', $admin);

            $total_cashout_minus_fee = $ct[2] - $ct[3];
            $total_cashout_fee = $ct[3];
            $user = user()->find($user_id);
            $transfer = $user? $user->groupAdjustmentBalance(): 0;
            $remaining_balance = ($ct[1] + $transfer) - $total_cashout_minus_fee;

            $admin_balances[] = [
                'name' => $user_name,
                'total_cash_in' => dn($ct[1]),
                'total_cash_out' => dn($total_cashout_minus_fee),
                'total_cash_out_fee' => dn($total_cashout_fee),
                'transfer' => dn($transfer),
                'remaining_balance' => dn($remaining_balance)
            ];
        }

        $this->data['admin_balances'] = $admin_balances;
    }
}
