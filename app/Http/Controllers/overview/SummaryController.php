<?php

namespace App\Http\Controllers\overview;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;

class SummaryController extends Controller
{
	protected $data = [];

    public function index()
    {
    	$this->data['balances'] = currentUserGroup()->balanceList();

    	$this->setBalance();
    	$this->setContribution();
    	$this->setLoan();

    	return view('overview.summaries.index', $this->data);
    }

    private function setBalance()
    {
    	//Cashin
    	$cashin = $this->data['balances']->where('type', 1);
    	$total_cash_in = $cashin->where('status', 'Confirmed')->sum('amount');
    	$pending_cash_in = $cashin->where('status', 'Pending')->sum('amount');

    	$this->data['total_cash_in'] = $total_cash_in;
    	$this->data['pending_cash_in'] = $pending_cash_in;

    	//Cashout
    	$cashout = $this->data['balances']->where('type', 2);
    	$total_cash_out = abs($cashout->where('status', 'Confirmed')->sum('amount'));
    	$pending_cash_out = abs($cashout->where('status', 'Pending')->sum('amount'));

    	$cash_out_fees = calculatePercentage($total_cash_out, currentUserGroup()->processing_fee);
    	$total_cash_out_released = $total_cash_out - $cash_out_fees;


    	$this->data['total_cash_out_released'] = $total_cash_out_released;
    	$this->data['pending_cash_out'] = $pending_cash_out;
    	$this->data['cash_out_fees'] = $cash_out_fees;    

        $this->data['users_available_balance'] = currentUserGroup()->usersAvailbleBalance();            	

    	//Total
    	$this->data['total'] = $this->data['total_cash_in'] - $this->data['total_cash_out_released'];    	
    }

    private function setContribution()
    {
    	$group = currentUserGroup();
    	$this->data['total_paid_contributions'] = $group->totalContributions();
    	$this->data['total_contributions'] = $group->totalNoOfShare() * $group->amount_per_share * $group->paymentSchedules()->count();
    	$this->data['total_unpaid_contributions'] = $this->data['total_contributions'] - $this->data['total_paid_contributions'];

    }

    private function setLoan()
    {
    	$group = currentUserGroup();
    	$base = $group->totalLoan();
    	$interest = $group->totalInterest();
    	$paid = $group->totalLoanPaid();
    	$total = $base + $interest;


    	$this->data['total_loans'] = $total;
    	$this->data['total_interests'] = $interest;
    	$this->data['total_loan_paid'] = $paid;
    	$this->data['total_loan_balance'] = $total - $paid;

    }
}
