<?php

namespace App\Http\Controllers\overview;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContributionListController extends Controller
{
	protected $data = [];

    public function index()
    {
    	$this->data['users'] = currentUserGroup()->users;
    	return view('overview.contribution_lists.index', $this->data);
    }

}
