<?php

namespace App\Http\Controllers\overview;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoanListController extends Controller
{
    public function index()
    {
    	$loans = currentUserGroup()->loans->where('status', '<>', 'Pending');
    		
    	return view('overview.loan_lists.index', compact('loans'));
    }
}
