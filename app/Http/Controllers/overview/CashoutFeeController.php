<?php

namespace App\Http\Controllers\overview;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\BalanceTrait;

class CashoutFeeController extends Controller
{
	use BalanceTrait;
	protected $data = [];

    public function index()
    {
    	$this->data['cashouts'] = $this->cashEntries(2, 'Confirmed');
        // $this->data['total_cashout_fees'] = currentUserGroup()->totalCashoutFees();
        $this->calculateCashoutFees();
    	return view('overview.cash_out_fees.index', $this->data);
    }

    private function calculateCashoutFees() {
        $sum = 0;
        foreach($this->data['cashouts'] as $cash_out) {
            $sum += $cash_out->getCashoutFee();
        }

        $this->data['total_cashout_fees'] = $sum;    
    }
}
