<?php

namespace App\Http\Controllers\overview;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Yajra\DataTables\Html\Builder;

class UserHoldingController extends Controller
{
	protected $data = [];

	public function index()
	{
      $perPage = input('perPage')? input('perPage'): 10;

      $users = currentUserGroup()->users()->paginate($perPage);
      $columns = [
            ['title' => 'Member'],
            ['title' => 'Share'],
            ['title' => 'Available Balance'],
            ['title' => 'Pending Cash In'],
            ['title' => 'Pending Cash Out'],
            ['title' => 'Total Contributions'],
            ['title' => 'Group Dividend'],
            ['title' => 'Personal Dividend'],
            ['title' => 'Guarantor Dividend'],
            ['title' => 'Loan / Guarantor Limit'],
            ['title' => 'Loan Balance'],
      ];

	    return view('overview.user_holdings.index', compact('columns', 'users'));    	
	}
}
