<?php

namespace App\Http\Controllers\overview;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ReleaseSummaryController extends Controller
{
    protected $data = [];

    public function index()
    {
        $data = [];

        if(Storage::has('release_summary.php')) {
            $data = Storage::get('release_summary.php');
        } 

        $this->data['users'] = json_decode($data);
    	return view('overview.release_summaries.index', $this->data);
    }    

    protected function calculateTotalReceivables($data)
    {
        return  $data['paid_contribution'] 
                + $data['personal_dividend']
                + $data['group_dividend']
                + $data['guarantor_dividend'] 
                - $data['loan_balance'];
    }

    public function create() 
    {
        $users = currentUserGroup()->users()->get();
        $data = [];

        foreach($users as $key => $user) {
            $data[$key] = [
                'id' => $user->id,
                'name' => $user->name,
                'paid_contribution' => $user->getTotalContributions(),
                'group_dividend' => $user->totalGroupDividend(),
                'personal_dividend' => $user->totalPersonalInterest(),
                'guarantor_dividend' => $user->totalGuarantorInterest(),
                'loan_balance' => $user->loanBalance(),    
                'guarantor_balance' => $user->guarantorLoanBalance(),     
            ];

            $data[$key]['total_receivables'] = $this->calculateTotalReceivables($data[$key]);

        }

        Storage::disk('local')->put('release_summary.php', json_encode($data));
        return 'created';
    }

    private function setData() {

    }
}
