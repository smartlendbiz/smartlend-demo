<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends MasterController
{
    protected $rules = [
        'name' => 'required', 
        'contact_no' => 'required', 
        'gender'  => 'required',
    ];

    public function index()
    {
    	return view('profile.index', ['user' => user()]);
    }

    public function update(Request $request, $id)
    {
		if($request->password != $request->password_confirmation) {
	    	return redirect()->back()->with("danger", "Password confirmation doesn't match.");
		}

        if(!validMobileNumber($request->contact_no)) {
            return redirect()->back()->with("danger", "Invalid mobile number.");   
        }

		$password_change = trim($request->password)? true: false;
		if($password_change) {
			$data['password'] = $request->password;
			$this->rules['password'] ='required|string|min:6';
		}

        $data = $request->only(array_keys($this->rules));
        $validate = $this->validator($data, $this->rules);

        if (!$validate->fails()) {
        	if($password_change) $data['password'] = bcrypt($data['password']);
            user()->update($data);
        } else {
            return redirect()->back()->withErrors($validate)->withInput();
        }

        return redirect()->back()->with('success', 'User Profile was updated successfully.');
    }

}
