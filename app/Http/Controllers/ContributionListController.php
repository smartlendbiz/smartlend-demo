<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\BalanceTrait;

class ContributionListController extends Controller
{
	use BalanceTrait;

	protected $data = [];

    public function index(Request $request)
    {
        $gid = user()->default_group_id;
        $this->setPaidContributions();
        $this->setCurrentContribution();
        $this->setContributionList();
        $this->setTotalActiveContributions();

    	return view('contributions.index', $this->data);
    }

    public function store(Request $request) 
    {
        $gid = user()->default_group_id;
    	if($gid) {
            $no_of_share = $request->no_of_share;
            if(!!!$no_of_share || $no_of_share < 0 || !is_numeric($no_of_share) || strpos($no_of_share, '.')>-1) {
                return redirect()->back()->with('danger', 'Invalid input!');       			
            }
            $current_contribution = user()->getCurrentContribution();

            if($request->new == 'true' && !!!$current_contribution) {
                user()->createContributionList($no_of_share);
                return redirect()->back()->with('success', 'Contribution Contract Added');       			
            }

            if(!!!$current_contribution) {
                return redirect()->back()->with('danger', 'No active contribution');       			
            }            

    		if(user()->balance() < ($current_contribution->payPerShare() * $no_of_share)) {
        		return redirect()->back()->with('danger', 'Available balance is not enough.');       			
    		}

    		if($this->payContribution($current_contribution, $no_of_share)) {
        		return redirect()->back()->with('success', 'Payment successful.');       			
    		}
    	}

        return redirect()->back()->with('danger', 'Unable to pay. Please try again. If the problem persists please contact admin.');   
    }

    private function setCurrentContribution()
    {
    	$this->data['current_contribution'] = user()->getCurrentContribution();
    }

    private function setContributionList()
    {
    	$this->data['contribution_lists'] = user()->getContributionLists;
    }

    private function setPaidContributions()
    {
        $this->data['paid_contributions'] = user()->getContributionListPayments();
    }

    private function setTotalActiveContributions()
    {
        $this->data['total_active_contributions'] = user()->getTotalActiveContributions();
    }
}
