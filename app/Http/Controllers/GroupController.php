<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group;
use App\User;
use App\Role;

class GroupController extends MasterController
{

    protected $rules = [
        'name' => 'required|unique:groups',
        'amount_per_share' => 'required',
        'grace_period' => 'required',
        'limit' => 'required',
        'status' => 'required',
        'year' => 'required'
    ];

    public function index()
    {
        $groups = Group::all();
        return view('groups.index', compact('groups'));
    }

    public function create()
    {
        return view('groups.create');
    }

    public function store(Request $request)
    {
        $data = $this->getInput($request);
        $validate = $this->validator($data, $this->rules);

        if (!$validate->fails()) {
            $group = Group::create($data);
        } else {
            return redirect('/groups')->withErrors($validate);
        }

        return redirect('/groups/'.$group->id.'/edit#users');
    }

    public function show(Request $request, $content_view)
    {
        $data = [];
        $input = $request->all();

        if(isset($input['gid'])) {
            $data['group'] = Group::find($input['gid']);
            $data['users'] = User::where('id','<>', 1)->get();
            $data['roles'] = Role::where('name', 'Admin')->get();
        }

        if(in_array($content_view, ['group_info','users', 'user_shares', 'payment_schedules']) && count($data)) {
            return view("groups.tabs.$content_view", $data);
        }

        return null;
    }

    public function edit($id)
    {
        $group = Group::find($id);
        return view('groups.edit', compact('group'));
    }

    public function update(Request $request, $id)
    {
        switch ($request->type) {
            case 'group_info': return $this->updateGroupInfo($request, $id); break;
            case 'users': return $this->updateUsers($request, $id); break;
            case 'user_shares': return $this->updateUserShares($request, $id); break;
            case 'payment_schedules': return $this->updatePaymentSchedules($request, $id); break;
            
            default:
                return redirect()->back()->with('danger', 'Failed to udpate.');
                break;
        }        
    }

    private function updateGroupInfo($request, $id)
    {
        $data = $this->getInput($request);
        $group = Group::find($id);
        $group->update($data);

        return redirect("groups/$group->id/edit#".$request->type)->with('success', 'Group was udpated successfully.');
    }

    private function updateUsers($request, $id)
    {
        $group = Group::find($id);

        $current_user_shares = $group->userShares()->pluck('user_share','user_id')->all();
        $group->users()->detach();

        $users = [];
        foreach ((array) $request->users as $user_id) {
            $user_share = isset($current_user_shares[$user_id])? $current_user_shares[$user_id]: 1;
            $users[] = [
                'user_id' => $user_id,
                'user_share' => $user_share
            ];
        }        
        
        $group->userShares()->createMany($users);

        $group->userRoles()->delete();
        foreach ((array)$request->group_user_roles as $user_id => $role_ids) {
            foreach ($role_ids as $role_id) {
                $group->userRoles()->create(['user_id' => $user_id, 'role_id' => $role_id]);
            }
        }

        return redirect("groups/$group->id/edit#".$request->type)->with('success', 'Group users were updated successfully.');
    }

    private function updateUserShares($request, $id)
    {
        $group = Group::find($id);
        foreach ((array)$request->user_shares as $user_id => $user_share) {
            $group->userShares()->where('user_id', $user_id)->update(['user_share' => $user_share]);
        }

        return redirect("groups/$group->id/edit#".$request->type)->with('success', 'User shares were updated successfully.');
    }

    private function updatePaymentSchedules($request, $id)
    {
        if($request->payment_date) {
            $group = Group::find($id);
            $group->paymentSchedules()->firstOrCreate(['payment_date' => $request->payment_date]);
            return redirect("groups/$group->id/edit#".$request->type)->with('success', 'Payment schedule was added successfully.');            
        } else {
            return redirect("groups/$group->id/edit#".$request->type)->with('danger', 'Update Failed.');
        }
    }

    public function destroy($id)
    {
        $delete_success = Group::destroy($id);
        if($delete_success) {
            return redirect('/groups')->with('success', 'Group was deleted successfully.');   
        } else {
            return redirect('/groups')->with('danger', 'Group was not deleted.');   
        }
    }

    private function getInput($request)
    {
        return $request->only(array_keys($this->rules));
    }
}
