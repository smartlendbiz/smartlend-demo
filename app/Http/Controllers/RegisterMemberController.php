<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\GroupToken;
use App\GroupUser;
use App\Role;
use App\User;
use DB;

class RegisterMemberController extends Controller
{
    protected $rules = [
        'name' => 'required', 
        'email' => 'required|email|unique:users', 
        'contact_no' => 'required', 
        'gender'  => 'required',
        // 'user_share' => 'required|numeric|min:1',
        'password' => 'required|string|min:6',
        'token' => 'required',
    ];


    public function index(Request $request)
    {
    	$token = $request->token;
    	$link = $this->getLink($token);
    	return view('register_members.index', compact('link', 'token'));
    }

    public function generate(Request $request)
    {
		$token = $this->generateToken();
		$registered_token = GroupToken::firstOrCreate([
			'group_id' => currentUserGroup()->id, 
			'user_id' => user()->id,
			'registered_user_id' => null,
			'token' => $token
		]);

        $this->getLink($token);
    	return redirect()->back()->with('success', 'Registration Link:<br><span id="link">'.$this->getLink($token).'</span> <i onclick="copy('."'link'".')" class="far fa-copy pointer"></i>');
    }

    public function register($token)
    {
    	$registered_token = $this->validToken($token);

   		$valid = $registered_token? true: false;
    	return view('register_members.form', compact('valid', 'token'));
    }

    public function submit(Request $request)
    {

        $data = $request->only(array_keys($this->rules));
        $validate = Validator::make($data, $this->rules);

        if (!$validate->fails()) {
    		$token = GroupToken::where('token', $data['token'])->whereNull('registered_user_id')->first();
    		if(!$token) {
				return redirect()->back();    			
    		}

            if($request->password != $request->confirm_password) {
                return redirect()->back()->with('danger', 'Password confirmation mismatch.')->withInput();
            }

    		unset($data['token']);
    		$this->createUser($data, $token);

    		return redirect('/');
        }

        return redirect()->back()->withErrors($validate)->withInput();
    }

    private function createUser($new_user, $token)
    {
        $role = Role::where('name', 'Member')->first();
        // $user_share = $new_user['user_share'];
        $user_share = 0;
        // unset($new_user['user_share']);

        $new_user['password'] = bcrypt($new_user['password']);
        $new_user['defaul_group_id'] = $token->group_id;

        DB::beginTransaction();
        $user = User::create($new_user);    

        $token->update(['registered_user_id' => $user->id]);

        if($role) {
            $user->roles()->attach($role->id);
        }

        GroupUser::firstOrCreate([
        	'group_id' => $new_user['defaul_group_id'],
        	'user_id' => $user->id,
        	'user_share' => $user_share
        ]);

        DB::commit();
    }

    private function validToken($token)
    {
    	return GroupToken::where('token', $token)->whereNull('registered_user_id')->first();
    }

    private function getLink($token)
    {
    	return $token? $this->siteURL().'register_members/'.$token: '';
    }

	private function generateToken()
	{
	    return md5(rand(1, 10) . microtime());
	}

	private function siteURL()
	{
	    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
	    $domainName = $_SERVER['HTTP_HOST'].'/';
	    return $protocol.$domainName;
	}	
}
