<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\BalanceTrait;
use App\Balance;

class CashOutController extends MasterController
{
    use BalanceTrait;

    function __construct()
    {
        $this->model = new Balance;
        $this->redirect = '/cash_out';
        $this->view_folder = 'cash_outs';
        $this->variable_name = 'cash_outs';
        $this->model_label = 'Cash Out';
        $this->rules = [
            'amount' => 'required|integer|min:1',
            'account_identifier' => 'required',
            'notes' => 'required',
            'method' => 'required',
        ];   
    } 

    public function index()
    {
        $cash_outs = $this->userCashOutData();
        return view($this->view_folder.'.index', compact('cash_outs'));
    }    

    public function store(Request $request)
    {
        $data = $request->only(array_keys($this->rules));
        $validate = $this->validator($data, $this->rules);

        if (!$validate->fails()) {
            if($data['amount'] < 100) {
                return redirect($this->redirect)->with('danger', 'Minimum cash out is 100.');
            }

            if(user()->balance() < $data['amount']) {
                return redirect($this->redirect)->with('danger', 'Cash out failed. Your available balance is not enough.');                
            }

            $this->cashOut($data);
        } else {
            return redirect($this->redirect)->withErrors($validate)->withInput();
        }

        return redirect($this->redirect)->with('success', 'Cash out was added successfully. This will be processed within Monday-Friday.');
    }
}
