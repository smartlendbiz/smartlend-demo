<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

class RoleController extends MasterController
{
   	function __construct()
   	{
   		$this->model = new Role;
	    $this->redirect = '/roles';
	    $this->view_folder = 'roles';
	    $this->variable_name = 'roles';
	    $this->model_label = 'Role';
	    $this->single_model = 'role';
	    $this->rules = [
	        'name' => 'required|unique:roles', 
	    ];   		
   	} 

    public function update(Request $request, $id)
    {
        $id = $request->id;
        $data = $request->only(array_keys($this->rules));
        $validate = $this->validator($data, $this->rules);

        if (!$validate->fails() && isset($id)) {
            $has_duplicate_role = Role::where('name', $data['name'])->whereNotIn('id', [$id])->exists();
            if(!$has_duplicate_role)  {
                Role::where('id', $id)->update($data);
            } else {
                return redirect('/roles')->with('danger', 'Role name is already taken.');    
            }
        } else {
            return redirect('/roles')->with('danger', 'Unable to update the role. Try again.');
        }

        return redirect('/roles')->with('success', 'Role was updated successfully.');
    }   	
}
