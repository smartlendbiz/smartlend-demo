<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\BalanceTrait;

class ContributionController extends Controller
{
	use BalanceTrait;

	protected $data = [];

    public function index(Request $request)
    {
        $gid = user()->default_group_id;
    	$this->setGroup();
        $this->setPaymentSchedules($gid);  
        $this->setPaidContributions($gid);
        $this->setTotalContributions($gid);
        
    	return view('contributions.index', $this->data);
    }

    public function show($page)
    {
    	if($page == 'pay') {
    		$group = user()->group(user()->default_group_id);
    		$payment_schedule = $this->getPaymentSchedule($group->id);
        		return view('contributions.pay', compact('balance', 'payment_schedule', 'group'));
    	}

    	return null;
    }

    public function store(Request $request) 
    {
        $gid = user()->default_group_id;
    	if($gid) {
    		$payment_schedule = $this->getPaymentSchedule($gid);

    		if(user()->balance() < $payment_schedule->amount_to_pay) {
        		return redirect()->back()->with('warning', 'Available balance is not enough.');       			
    		}

    		if($this->payContribution($payment_schedule)) {
        		return redirect()->back()->with('success', 'Payment successful.');       			
    		}
    	}

        return redirect()->back()->with('danger', 'Unable to pay. Please try again. If the problem persists please contact admin.');   
    }

    private function getPaymentSchedule($gid)
    {
    	$payment_schedules = user()->getPaymentSchedules($gid);
    	return $payment_schedules->first();
    }

    private function setPaymentSchedules($gid)
    {
    	$this->data['payment_schedules'] = user()->getPaymentSchedules($gid);
    }

    private function setPaidContributions($gid)
    {
    	$this->data['paid_contributions'] = user()->getPaidContributions($gid);
    }

    private function setTotalContributions($gid)
    {
        $this->data['total_contributions'] = user()->getTotalContributions($gid);
    }

    private function setGroup()
    {
    	$this->data['group'] = user()->group(user()->default_group_id);
    }
}
