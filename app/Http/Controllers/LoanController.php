<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\LoanTrait;
use App\Traits\BalanceTrait;
use App\Loan;
use App\LoanGuarantor;

class LoanController extends Controller
{
	protected $data = [];

	use LoanTrait, BalanceTrait;

    public function index()
    {
    	$group = currentUserGroup();
    	$this->data['group'] = $group;
    	$this->data['loans'] = user()->getGroupLoans($group->id);

        $this->data['available_balance'] = user()->balance();
        $this->data['loan_guarantor_limit'] = user()->loanGuarantorLimit();
        $this->data['loan_balance'] = user()->loanBalance();
        $this->data['guarantor_balance'] = user()->guarantorLoanBalance();
        $this->data['amount_for_loan'] = $group->amountForLoan();

    	return view('loans.index', $this->data);
    }

    public function show(Request $request, $page)
    {
    	$group = currentUserGroup();
    	if(in_array($page, ['pending', 'reservation', 'guarantor_await', 'guarantor_lists', 'pay_loan', 'interest_lists'])) {
            $loan = Loan::find($request->lid);
    		switch ($page) {
    			case 'pending':
    				$this->data['loans'] = $group->pendingLoans(user()->id);
    				break;
    			case 'reservation': 
    				$this->data['loans'] = $group->reservationLoans(); 
    			break;
                case 'guarantor_await': 
                    $this->data['loans'] = $group->guarantorLoans(); 
                break;  
                case 'guarantor_lists': 
                    $this->data['loan'] = $loan;
                break;  
                case 'pay_loan': 
                    $this->data['loan'] = $loan;
                break;  
                case 'interest_lists': 
                    $this->data['loan'] = $loan;
                break;                  
    			default:
    				break;
    		}

    		return view('loans.tabs.'.$page, $this->data);
    	}

    	return null;
    } 

    public function create(Request $request)
    {
    	$group = currentUserGroup();
    	return view('loans.create', compact('group'));
    }

    public function store(Request $request)
    {
    	$group = user()->group(user()->default_group_id);

    	$amount = $request->amount;
    	if(!isAmountMultipleBy($amount, $group->amount_per_share) || $amount <= 0) {
      		return redirect()->back()->with('danger', $this->getReturnMessage('multiple_number'));
    	}

        $data = ['group_id' => $group->id, 'amount' => $amount, 'notes' => $request->notes];

        if(!user()->loanQualified($group->id, $amount)) $data['type'] = 'Guarantor';

    	if($this->createLoan($data)) {
    		return redirect()->back()->with('success', 'Loan was reserved successfully.');	
    	}

    	return redirect()->back()->with('danger', 'Something went wrong. Please try again.');	
    }

    public function update(Request $request, $id)
    {
    	$loan = Loan::find($id);

        switch ($request->a) {
            case 'release':
                if($loan->amount <= $loan->group->amountForLoan()) {
                    if($this->releaseLoan($id)) {
                        return redirect()->back()->with('success', 'Loan was released successfully.');
                    }
                }

                return redirect()->back()->with('danger', 'Unable to release.');
                break;

            case 'guarantor':
                $result = $this->adjustGuarantorAmount($id, $request->amount);
                $redirect = 'loans#guarantor_await';
                if($result == 1) return redirect($redirect)->with('success', 'Guarantor amount was udpated successfully.');  
                if($result == 0) return redirect($redirect)->with('danger', $this->getReturnMessage('multiple_number'));
                if($result == -1) return redirect($redirect)->with('danger', 'Unable to update. The amount is too high than what is needed.');

                if($result == 2) return redirect($redirect)->with('danger', 'Unable to update. Guarantor amount limit is reached.');
                break;

            case 'pay':
                $result = $this->payLoan($id, $request->amount);
                if($result == 1) return redirect()->back()->with('success', 'Payment was successful.');
                if($result == 0) return redirect()->back()->with('danger', 'Available balance is not enough.');
                if($result == -1) return redirect()->back()->with('danger', 'Invalid payment.');
                if($result == 2) return redirect()->back()->with('danger', 'Payment should not exceed the remaining balance of the loan.');
                break;
            default:
                # code...
                break;
        }


    }    


    public function destroy($id)
    {
        $delete_success = Loan::destroy($id);
        if($delete_success) {
            LoanGuarantor::where('loan_id', $id)->delete();
            return redirect()->back()->with('success', 'Loan was cancelled successfully.');   
        } else {
            return redirect()->back()->with('danger', 'Loan was not cancelled.');   
        }
    }
}
