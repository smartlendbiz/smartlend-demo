<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PaymentSchedule;

class PaymentScheduleController extends Controller
{
   	function __construct()
   	{
	    $this->rules = [
	        'group_id' => 'required', 
	        'payment_date' => 'required', 
	    ];   		
   	} 

   	public function index(Request $request)
   	{
   		$group_id = $request->gid;
   		$this->data['payment_schedules'] = $group_id
   										 ? PaymentSchedule::where('group_id', $group_id)->get()
   										 : PaymentSchedule::all();

   		return view('payment_schedules.index', $this->data);
   	}

      public function create()
      {
         return view('payment_schedules.create');
      }

      public function destroy(Request $request, $id)
      {
         $path = url()->previous();
         if($request->type == 'payment_schedules') {
            $path .= '#payment_schedules';
         }
         

         $delete_success = PaymentSchedule::destroy($id);
         if($delete_success) {
            return redirect($path)->with('success', 'Payment schedule was deleted successfully.');   
         } else {
            return redirect($path)->with('danger', 'Payment schedule was not deleted.');   
         }
      }      
}
