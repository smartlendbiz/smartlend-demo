<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GuarantorLoanController extends Controller
{
	protected $data = [];

    public function index()
    {
    	$this->data['guarantor_loans'] = user()->guarantorLoans;
		$this->data['guarantor_loan_balance'] = user()->guarantorLoanBalance();

    	return view('guarantor_loans.index', $this->data);
    }
}
