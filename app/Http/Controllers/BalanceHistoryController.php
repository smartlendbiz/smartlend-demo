<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BalanceHistoryController extends Controller
{
    public function index()
    {
    	$balances = user()->getAllBalances;

    	return view('balance_history.index', compact('balances'));
    }
}
