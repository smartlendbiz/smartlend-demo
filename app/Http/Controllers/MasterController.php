<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MasterController extends Controller
{
    protected $model;
    protected $data = [];
    protected $redirect;
    protected $view_folder;
    protected $rules;    
    protected $variable_name;
    protected $model_label;
    protected $single_model;

    protected function validator(array $data, array $rules)
    {
        return Validator::make($data, $rules);
    }    

    public function index()
    {
        $this->setData();
        return view($this->view_folder.'.index', $this->data);
    }    

    public function store(Request $request)
    {
        $data = $request->only(array_keys($this->rules));
        $validate = $this->validator($data, $this->rules);

        if (!$validate->fails()) {
            $this->model::create($data);
        } else {
            return redirect($this->redirect)->withErrors($validate)->withInput();
        }

        return redirect($this->redirect)->with('success', $this->getLabel().' was added successfully.');
    }     

    public function create()
    {
        return view($this->view_folder.'.create');
    }    

    public function edit($id)
    {
        $this->data[$this->single_model] = $this->model::find($id);
        return view($this->view_folder.'.edit', $this->data);
    }

    public function destroy($id)
    {
        $delete_success = $this->model::destroy($id);
        if($delete_success) {
            return redirect($this->redirect)->with('success', $this->getLabel().' was deleted successfully.');   
        } else {
            return redirect($this->redirect)->with('danger', $this->getLabel().' was not deleted.');   
        }
    }

    public function setData()
    {
        $this->data[$this->variable_name] = $this->model::all();
    }   

    public function getLabel()
    {
    	return $this->model_label? $this->model_label: $this->variable_name;
    }
}