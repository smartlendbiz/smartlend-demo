<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\BalanceTrait;
use App\Balance;

class CashTransactionController extends Controller
{
	use BalanceTrait;

    public function index()
    {
    	return view('cash_transactions.index');
    }

    public function show($page)
    {
    	if(in_array($page, ['pending', 'confirmed', 'declined', 'all'])) {

    		$balance_lists = $page=='all'? $this->getBalanceList(): $this->getBalanceList(ucfirst($page));
    		return view('cash_transactions.show', compact('balance_lists', 'page'));
    	}
    } 

    public function update(Request $request, $id)
    {
    	$redirect = '/cash_transactions#'.$request->hash;

    	if(in_array($request->action, ['confirmed', 'declined'])) {
    		Balance::where('id', $id)->update([
    				'status' => ucfirst($request->action),
    				'performed_by' => $this->userId()
    			]);
    		return redirect($redirect)->with('success', 'Action was successful.');   
    	}

    	return redirect($redirect)->with('danger', 'Unable to process the action.');   

    }

    public function destroy(Request $request, $id)
    {
    	$redirect = '/cash_transactions#'.$request->hash;
        $delete_success = Balance::destroy($id);
        if($delete_success) {
            return redirect($redirect)->with('success', 'Cash transaction was deleted successfully.');   
        } else {
            return redirect($redirect)->with('danger', 'Cash transaction was not deleted.');   
        }    	
    }      
}
