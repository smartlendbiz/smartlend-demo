<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BalanceReceiver;
use App\Traits\BalanceTrait;
use App\User;
use DB;

class SendFundController extends MasterController
{
    use BalanceTrait;

    protected $data = [];
    protected $rules = [
        'amount' => 'required|integer|min:1',
        'receiver_id' => 'required',
        'email' => ''
    ];

    public function index()
    {
        $this->dataSet();
        return view('send_funds.index', $this->data);

    }

    private function dataSet()
    {
        $this->data['receivers'] = user()->balanceReceivers;
        $this->data['send_funds_list'] = user()->getAllBalances()->whereIn('type',[7,8])->get();
    }

    public function store(Request $request)
    {
        $data = $request->only(array_keys($this->rules));
        $validate = $this->validator($data, $this->rules);
        
        $receiver = User::find($data['receiver_id']);
        if(!!!$receiver) {
            $receiver = User::where('email', $data['email'])->first();
            if(!!!$receiver) {
                return redirect()->back()->with("danger", "User not found. Can't send the fund");   
            }

            BalanceReceiver::firstOrCreate([
                'user_id' => user()->id,
                'receiver_id' => $receiver->id
            ]);
        }

        if($receiver->email == user()->email) {
            return redirect()->back()->with("danger", "You cannot send funds to your own balance.");   
        }

        if(!user()->IsValidDeduction($data['amount'])) {
            return redirect()->back()->with("danger", "Available balance is not enough to send ".dn($data['amount']))->withInput();   
        }
        
        $this->sendFunds($receiver, $data['amount']);

        return redirect()->back()->with('success', dn($data['amount']).' was sent successfully to '.$receiver->name);

    }
}
