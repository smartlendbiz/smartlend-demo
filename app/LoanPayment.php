<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanPayment extends Model
{
    protected $fillable = ['loan_id', 'balance_id', 'amount'];

    public function balance()
    {
    	return $this->belongsTo('App\Balance');
    }
}
