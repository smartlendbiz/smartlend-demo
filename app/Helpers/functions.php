<?php

function dn($value) {
	return '₱'.number_format($value, 0);
}

function user() {
	return auth()->user();
}

function dateToday() {
	return today()->format('Y-m-d');
}

function df($date) {
	return date_format(date_create($date), 'Y-m-d');
}

function calculatePercentage($number, $percentage) {
	return ($percentage / 100) * $number;
}

function isAmountMultipleBy($amount, $amount_per_share) {
	return $amount % $amount_per_share == 0;
}

function currentUserGroup() {
	$u = user();
	return $u->group($u->default_group_id);
}

function isActiveRoute($route, $output = "active")
{
    if (Route::currentRouteName() == $route) return $output;
}

function isMultiMenuActive($menus = [])
{
	return in_array(Route::currentRouteName(), $menus)? 'active': '';
}

function input($input)
{
	return app('request')->input($input);
}

function volatile_asset($src) {
	return asset($src.'?_='.rand());
}

function validMobileNumber($number) {
	return preg_match("/^(09|\+639)\d{9}$/", $number);
}

function percent($number) {
	return round($number,0).'%';
}
