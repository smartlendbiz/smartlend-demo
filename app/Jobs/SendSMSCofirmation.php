<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Confirmation;

class SendSMSCofirmation 
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $details;

    public function __construct($details = [])
    {
        $this->details = $details;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $details = $this->details;

        //Trigger only in production
        if(config('app.env') != 'production') {
            Confirmation::create([
                'type' => $details['type'],
                'type_id' => $details['type_id'],
                'response' => json_encode($details)
            ]);
            
            return;
        }

        //Skip invalid mobile number.
        if(!validMobileNumber($details['mobile'])) return;

        $ch = curl_init();
        $parameters = [
            'apikey' => config('sms.apikey'),
            'number' => $details['mobile'],
            'message' => $details['message'],
            'sendername' => config('sms.sender')
        ];

        curl_setopt( $ch, CURLOPT_URL, config('sms.endpoint'));
        curl_setopt( $ch, CURLOPT_POST, 1 );

        //Send the parameters set above with the request
        curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query( $parameters ) );

        // Receive response from server
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $output = curl_exec( $ch );
        curl_close ($ch);

        //Show the server response
        // echo $output;
        
        Confirmation::create([
            'type' => $details['type'],
            'type_id' => $details['type_id'],
            'response' => json_encode($output)
        ]);
    }
}
