<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;

class LoanInterest extends Model
{
    protected $fillable = ['loan_id', 'amount', 'date_added'];
    protected $appends = ['until_date'];

    public function getUntilDateAttribute()
    {
    	return Carbon::parse($this->date_added)->addDays(config('interest.days'));
    }
}
