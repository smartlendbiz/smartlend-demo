<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = ['balance_id', 'path'];

    public function balance()
    {
    	return $this->belongsTo('App\Balance');
    }
}
