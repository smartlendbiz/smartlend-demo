<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BalanceReceiver extends Model
{
    protected $table = 'balance_receivers';
    protected $fillable = ['user_id','receiver_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'receiver_id');
    }
}
