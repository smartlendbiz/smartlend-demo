<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupToken extends Model
{
    protected $fillable = ['group_id', 'user_id', 'token', 'registered_user_id'];
}
