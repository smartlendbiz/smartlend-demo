<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contribution extends Model
{
    protected $fillable = ['balance_id', 'payment_schedule_id'];

    public function balance()
    {
    	return $this->belongsTo('App\Balance');
    }

    public function paymentSchedule()
    {
    	return $this->belongsTo('App\PaymentSchedule');	
    }

    public function paidAmount()
    {
    	return $this->balance->amount;
    }
}
