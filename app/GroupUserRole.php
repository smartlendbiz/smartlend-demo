<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupUserRole extends Model
{
    protected $fillable = ['group_id', 'user_id', 'role_id'];

    public function user() 
    {
    	return  $this->belongsTo('App\User');
    }
}
