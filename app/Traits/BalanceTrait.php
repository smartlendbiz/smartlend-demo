<?php

namespace App\Traits;

use App\Jobs\SendSMSCofirmation;
use App\Balance;
use Illuminate\Support\Facades\DB;

trait BalanceTrait
{
    public function cashIn($data, $request = null)
    {
        $data = $this->getOwnership($data);
        $data['type'] = 1;
        
        $is_fund_transfer = isset($data['bank_detail_id']);

        if($is_fund_transfer) {
            $bank_detail_id = $data['bank_detail_id'];
            unset($data['bank_detail_id']);
        }

        $file = $request->file('screenshot');
        if($file) {
            unset($data['screenshot']);
        }

        try {
            DB::beginTransaction();

            if($data['purpose'] == 'contributionPayment') {
                $current_contribution = user()->getCurrentContribution();
    
                if(!!!$current_contribution) {
                    return 'no_active_contribution';
                }

                if((int)$data['amount'] % $current_contribution->payPerShare() != 0) {
                    return 'not_valid_auto';
                }
                
            }

            $balance = Balance::create($data);
    
            if($is_fund_transfer) {
                $balance->balanceBankDetail()->create(['bank_detail_id' => $bank_detail_id]);
            }
    
            if($file) {
                $path = $file->store('public/screenshots/'.$balance->id);
                $balance->file()->create(['path' => $path]);
            }        
    
            DB::commit();
        } catch(Exception $e) {
            DB::rollBack();
        }
    }

    public function cashOut($data)
    {
        $data = $this->getOwnership($data);
        $data['type'] = 2;
        $data['amount'] *= -1;
        
        Balance::create($data);
    }

    private function getOwnership($data, $user_id = null)
    {        
        $data['user_id'] = $user_id? $user_id: user()->id;
        $data['performed_by'] = user()->id;     

        return $data;
    }

    public function userCashInData()
    {
    	$user_id = auth()->user()->id;
    	return Balance::where('user_id', $user_id)->where('type', 1)->whereIn('type', [1, 2])->get();
    }

    public function userCashOutData()
    {
    	$user_id = auth()->user()->id;
    	return Balance::where('user_id', $user_id)->where('type', 2)->whereIn('type', [1, 2])->get();
    }    

    public function getBalanceList($status = null)
    {
        return $status? Balance::where('status', $status)->whereIn('type', [1, 2])->get(): 
                        Balance::all();
    }     

    public function payContribution($current_contribution, $no_of_share, $method='Manual Payment', $user_id = null)
    {
        $amount = $current_contribution->payPerShare() * $no_of_share;
        $payment_data = [
            'amount' => $amount * -1,
            'type' => 3,
            'status' => 'Confirmed',
            'notes' => 'Contribution (Manual Payment)',
            'method' => $method,
        ];

        $payment_data = $this->getOwnership($payment_data, $user_id);

        DB::beginTransaction();
        $balance = Balance::create($payment_data);
        $current_contribution->contributionPayments()->create([
            'contribution_list_id' => $current_contribution->id,
            'amount' => $amount
        ]);
        DB::commit();

        return $balance? true: false;
    }

    public function payLoan($loan_id, $amount)
    {
        $loan = user()->loans()->find($loan_id);
        $user_remaining_balance = user()->balance();
        $loan_remaining_balance = $loan->totalBalance();

        if($amount <= 0) return -1;
        if($loan_remaining_balance < $amount) return 2;

        if($user_remaining_balance >= $amount) {
            $payment_data = [
                'amount' => $amount * -1,
                'type' => 4,
                'status' => 'Confirmed',
                'notes' => 'Loan paid by user',
                'performed_by' => user()->id,
                'method' => 'Loan'
            ];

            DB::beginTransaction();
            $balance = user()->balances()->create($payment_data);
            $balance->loanPayment()->create(['loan_id' => $loan_id, 'amount' => $amount]);
            DB::commit();

            return 1;
        }

        return 0;

    }

    public function cashEntries($type, $status)
    {
        $users = currentUserGroup()->users;        
        if(!$status) $status = 'Pending';
        
        $cash_transactions = [];
        foreach ($users as $user) {
            $query = $user->getAllBalances()->where('type', $type);

            if(in_array($status, ['Pending', 'Confirmed', 'Declined'])) {
                $balances = clone($query)->where('status', $status)->get();                
            } else {
                $balances = clone($query)->get();                
            }

            foreach ($balances as $balance) {
                $cash_transactions[] = $balance;
            }
        }

        return $cash_transactions;
    }

    public function notification($balance, $type = 'SMS')
    {
        if($type == 'SMS') {
            $amount = $balance->type == 2? $balance->getReleasedAmount(): $balance->amount;
            $message = dn(abs($amount)).' '.$balance->typeLabel().' was '.strtolower($balance->status).'.';
            $message.= ' Your current total available balance is '.dn($balance->user->balance()).'.';

            $details = [
                'type' => 'balances',
                'type_id' => $balance->id,
                'mobile' =>  $balance->user->contact_no,
                'message' => $message
            ];

            SendSMSCofirmation::dispatch($details)->onQueue('SMS');
        }
    }

    public function sendFunds($receiver, $amount)
    {
        $user = user();
        $sender_data = [
            'amount' => $amount * -1,
            'user_id' => $user->id,
            'type' => 7,
            'status' => 'Confirmed',
            'performed_by' => $user->id,
            'method' => 'Sent to '.$receiver->name
        ];

        $receiver_data = [
            'amount' => $amount,
            'user_id' => $receiver->id,
            'type' => 8,
            'status' => 'Confirmed',
            'performed_by' => $user->id,
            'method' => 'Received from '.$user->name
        ];

        DB::beginTransaction();
        Balance::create($sender_data);
        Balance::create($receiver_data);
        DB::commit();
    }
}