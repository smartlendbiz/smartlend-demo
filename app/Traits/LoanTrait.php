<?php

namespace App\Traits;

use App\Loan;
use App\Traits\InterestTrait;
use DB;

trait LoanTrait {
	use InterestTrait;

	public function createLoan($data)
	{
		return user()->loans()->create($data);
	}

	public function releaseLoan($loan_id)
	{
		DB::beginTransaction();
		$loan = Loan::find($loan_id);
		$loan->update(['status' => 'Released', 'released_date' => dateToday()]);
		
		if($loan) {
			$this->addInterest($loan);
			$this->addBalance($loan);
		}

		DB::commit();

		return $loan? true: false;
	}

	public function adjustGuarantorAmount($loan_id, int $amount)
	{
		$user_id = user()->id;
		$group = currentUserGroup();
		$loan = Loan::find($loan_id);
		$remaining_amount = $loan->amount - ($loan->totalGuarantorAmount() + $amount);
		$guarantor_exists = $loan->loanGuarantors()->where('user_id', $user_id)->first();
		$limit = user()->loanGuarantorLimit();

		if($guarantor_exists) {
			$limit += $guarantor_exists->amount;
			if($amount > $limit) return 2;

			$guarantor_exists->update(['amount' => $amount]);
			$remaining_amount = $loan->amount - $amount;
		}

		if($amount > $limit) return 2;

		if($amount == 0) {
			$loan->loanGuarantors()->where('user_id', $user_id)->delete();
		} elseif($remaining_amount < 0) {
			return -1;
		} elseif(isAmountMultipleBy($amount, $group->amount_per_share) && $amount > 0 && !$guarantor_exists) {
			$loan->loanGuarantors()->firstOrCreate(['user_id' => $user_id, 'amount' => $amount]);
		} elseif(!$guarantor_exists) {
			return 0;
		}

		return 1;
	}

	public function getReturnMessage($type) 
	{
		switch ($type) {
			case 'multiple_number':
				return 'Amount should be multiple by '.currentUserGroup()->amount_per_share.'.';
				break;
			
			default:
				return '';
				break;
		}
	}

    public function addBalance($loan)
    {
        $payment_data = [
            'amount' => $loan->amount,
            'type' => 5,
            'status' => 'Confirmed',
            'notes' => 'Loan Released',
            'performed_by' => user()->id,
            'method' => 'Loan'
        ];

        return user()->balances()->create($payment_data);
    }	
}