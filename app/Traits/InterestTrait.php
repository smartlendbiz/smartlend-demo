<?php

namespace App\Traits;

use App\LoanInterest;
use App\Loan;
use Carbon;
use DB;

trait InterestTrait
{
	public function display()
	{
		$interest = $this->getLoanInterest(5000);
	}

	public function calculateLoanInterest()
	{
		$loans = Loan::where('status', 'Released')->get();

		foreach ($loans as $loan) {		
			$total_paid = 0;

			DB::beginTransaction();
			$loan->loanInterests()->delete();

			$ranges = $this->getInterestDateRanges($loan);

			$prev_from = $prev_to = '';
			foreach ($ranges as $key => $date_range) {
				extract($date_range);

				if($key == 0) {
					$this->addInterest($loan, $loan->amount);
				} else {
					$remaining_principal = $this->remainingPrincipalAmount($loan, $prev_to->toDateTimeString());
					if($remaining_principal != 0) {
						$this->addInterest($loan, $remaining_principal, $from->toDateTimeString(), $to->toDateTimeString());
					}
				}

				$prev_from = $from;
				$prev_to = $to;
			}			
			DB::commit();
			
		}
	}

	public function getInterestDateRanges($loan)
	{
		$today = Carbon::today();
		// $today = Carbon::parse('2019-06-08');
		$dates = [];

		$release_date  = Carbon::parse($loan->released_date);
		$no_of_days    = $today->diffInDays($release_date);
		$no_of_intrest = max(ceil($no_of_days / config('interest.days')), 1);

		$from = $release_date;
		foreach (range(1, $no_of_intrest) as $no) {
			$to = $from->copy()->addDays(config('interest.days'));

			if($from->lt($today)) {
				$dates[] = compact('from', 'to');
			} else {
				break;				
			}

			$from = $to->copy()->addDay();
		}		

		return $dates;
	}

	public function addInterest($loan, $amount = null, $added = null, $until_date = null)
	{
		$principal_amount = $amount? $amount: $this->remainingPrincipalAmount($loan, $until_date);
		$date_added = $added? $added: $loan->released_date;

		$loan->LoanInterests()->create([
			'amount' => $this->getLoanInterest($principal_amount), 
	   		'date_added' => $date_added
		]);
	}

	private function remainingPrincipalAmount($loan, $until_date = null)
	{
		$total_paid = $loan->totalPaid($until_date);
		$principal_balance = $loan->amount - $total_paid;

		return max($principal_balance, 0);
	}

	private function getLoanInterest($amount)
	{
		return calculatePercentage($amount, config('interest.rate'));
	}
}