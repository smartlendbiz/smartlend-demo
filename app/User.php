<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'contact_no', 'gender', 'password', 'status', 'default_group_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getNameAttribute($value)
    {
        return ucwords(strtolower($value));
    }

    public function share($group_id)
    {
        $user = $this->hasMany('App\GroupUser')->where('group_id', $group_id)->first();
        return $user? $user->user_share: 0;
    }

    public function getAllBalances()
    {
        return $this->hasMany('App\Balance');
    }

    public function balances()
    {
        return $this->getAllBalances()->whereRaw("status = 'Confirmed' or status = 'Pending' and type=2")->where('user_id', $this->id);
    }

    public function balance()
    {
        return floor($this->balances()->sum('amount'));
    }

    public function groups()
    {
        return $this->belongsToMany('App\Group');
    }

    public function group($gid = null)
    {
        $group = $this->groups()->find($gid);

        if(!$group) {
            $select_first_group = $this->groups()->first();

            if($select_first_group) {
                user()->update(['default_group_id' => $select_first_group->id]);
            }
        }

        return $this->groups()->find(user()->default_group_id);
    }

    public function getPaymentSchedules($gid, $paid = false, $user_id = null)
    {
        $group = $this->group($gid);
        $payment_schedules = $group->paymentSchedules()->orderBy('payment_date')->get();

        $data = $payment_schedules->filter(function($payment_schedule) use ($paid, $user_id) {
            return $payment_schedule->isPaid($user_id) == $paid;
        });

        return $data;
    }

    public function getPaidContributions($gid, $user_id = null)
    {
        $payment_schedules = $this->getPaymentSchedules($gid, true, $user_id);

        $paid_contributions = [];
        foreach ($payment_schedules as $payment_schedule) {
            $balance = $payment_schedule->balance($user_id);
            if($balance) {
                $paid_contributions[] = $balance->contribution;
            }
        }

        return $paid_contributions;
    }

    public function getTotalContributions($gid = null, $user_id = null)
    {
        if(!$user_id) $user_id = $this->id;
        if(!$gid) $gid = currentUserGroup()->id;
        $paid_contributions = $this->getPaidContributions($gid, $user_id);

        $total_contributions = 0;
        foreach ($paid_contributions as $contribution) {
            if(isset($contribution->balance)) {
                $total_contributions += $contribution->balance->amount;
            }
        }

        return $total_contributions;

    }

    public function createContributionList($no_of_share)
    {
        $group = currentUserGroup();
        $this->getContributionLists()->create([
            'group_id' => $group->id,
            'year' => $group->year,
            'no_of_share' => $no_of_share,
            'amount_per_share' => $group->amount_per_share,
            'total_share' => $group->amount_per_share * $no_of_share * 24
        ]);
    }

    public function getContributionLists($gid = null, $user_id = null)
    {
        if(!$user_id) $user_id = $this->id;
        if(!$gid) $gid = currentUserGroup()->id;

        return $this->hasMany('App\ContributionList')->where('group_id', $gid)->where('user_id', $user_id);
    }

    public function getContributionListPayments($gid = null, $user_id = null)
    {
        if(!$user_id) $user_id = $this->id;
        if(!$gid) $gid = currentUserGroup()->id;

        $payments = collect([]);
        $contribution_lists = $this->getContributionLists($gid, $user_id)->get();
        foreach($contribution_lists as $list) {
            $payments = $payments->merge($list->contributionPayments);
        }

        return $payments->sortByDesc('created_at');
    }

    public function getCurrentContribution($gid = null, $user_id = null)
    {
        if(!$user_id) $user_id = $this->id;
        if(!$gid) $gid = currentUserGroup()->id;

        return $this->getContributionLists($gid, $user_id)->where('status','ongoing')->first();
    }

    public function getTotalActiveContributions($gid = null, $user_id = null)
    {
        if(!$user_id) $user_id = $this->id;
        if(!$gid) $gid = currentUserGroup()->id;

        $total = 0;
        $contribution_lists = $this->getContributionLists($gid, $user_id)->get();
        foreach($contribution_lists as $list) {
            $total += $list->status == 'ongoing'? $list->totalPaid(): 0;
        }

        return $total;
    }

    public function loans()
    {
        return $this->hasMany('App\Loan');
    }

    public function guarantorLoans()
    {
        return $this->hasMany('App\LoanGuarantor');
    }

    public function guarantorLoanBalance()
    {
        $total = 0;
        foreach ($this->guarantorLoans as $guarantor_loan) {
            $total += $guarantor_loan->guarantorBalance();
        }

        return $total;
    }

    public function getGroupLoans($gid, $status = 'Released')
    {
        return $this->loans()->where('group_id', $gid)->where('status', $status)->get();
    }

    public function expectedTotalContribution($gid = null)
    {
        if(!$gid) $gid = currentUserGroup()->id;
        $group = $this->group($gid);
        // $no_of_contributions = $group->paymentSchedules()->count();
        // $user_share = $this->noOfShare($gid);
        // return $no_of_contributions * $group->amount_per_share * $user_share;

        $currrent_contribution = $this->getCurrentContribution();
        $amount_per_share = $currrent_contribution? $currrent_contribution->amount_per_share: 0;
        $no_of_contributions = 24;
        $user_share = $this->noOfShare($gid);
        return $no_of_contributions * $amount_per_share * $user_share;
    }

    public function noOfShare($gid = null)
    {
        // if(!$gid) $gid = currentUserGroup()->id;
        // $group = $this->group($gid);
        // return $group->userShares()->where('user_id', $this->id)->first()->user_share;

        $current_contribution = $this->getCurrentContribution();
        return !!$current_contribution? $current_contribution->no_of_share: 0;
    }

    public function loanGuarantorLimit($gid = null)
    {
        if(!$gid) $gid = currentUserGroup()->id;
        $group = $this->group($gid);

        $limit_percent = $this->isCurrentGroupAdmin()? 100: $group->limit;
        $limit_amount = calculatePercentage($this->expectedTotalContribution($gid), $limit_percent);
        $loan_balance = $this->loanBalance();
        $guarantor_balance = $this->guarantorLoanBalance();

        return $limit_amount - ($loan_balance + $guarantor_balance);
    }

    public function loanBalance($gid = null)
    {
        if(!$gid) $gid = currentUserGroup()->id;
        $group = $this->group($gid);
        $total_loan_amount = $group->totalLoan($this->id);
        $total_interest = $group->totalInterest($this->id);
        $total_loan_paid = $group->totalLoanPaid($this->id);

        return ($total_loan_amount + $total_interest) - $total_loan_paid;
    }

    public function loanQualified($gid, $amount)
    {
        $limit = $this->loanGuarantorLimit($gid);
        return ($limit - $amount) > -1;
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_roles', 'user_id', 'role_id');
    }

    public function hasRole($name)
    {
        return $this->roles()->where('name', $name)->exists();
    }

    public function displayRoles()
    {
        $roles = $this->roles->pluck('name')->toArray();
        return implode(', ', $roles);
    }

    public function hasGroup()
    {
        return $this->groups()->count() > 0;
    }

    public function getPendingCash($type)
    {
        return ($type == 1? 1: -1) *
               $this->getAllBalances()->where('type', $type)->where('status', 'Pending')->sum('amount');
    }

    public function isCurrentGroupAdmin()
    {
        return currentUserGroup()->userRoles()->where('user_id', $this->id)->where('role_id', 2)->count()? true: false;
    }

    public function totalGroupDividend()
    {
        $gid = $this->default_group_id;
        $latest_dividend = $this->group($gid)->latestDividend();
        $no_of_share     = $this->noOfshare($gid);

        return $latest_dividend * $no_of_share;
    }

    public function totalPersonalInterest($gid = null)
    {
        if(!$gid) $gid = $this->default_group_id;
        $loans = $this->group($gid)->releasedLoans($this->id);

        $total_interest = 0;
        foreach ($loans as $loan) {
            if(!$loan->hasGuarantor()) {
                $total_interest += $loan->totalInterest();
            }
        }


        return floor(calculatePercentage($total_interest , config('interest.personal_rate')));
    }


    public function totalGuarantorInterest($gid = null)
    {
        if(!$gid) $gid = $this->default_group_id;
        $loans = $this->group($gid)->releasedLoans()->where('user_id', '<>', $this->id);

        $total_interest = 0;
        foreach ($loans as $loan) {
            $guarantor_amount = $loan->loanGuarantors()->where('user_id', $this->id)->sum('amount');
            $total_interest += calculatePercentage($guarantor_amount, config('interest.rate'));
        }

        return floor(calculatePercentage($total_interest , config('interest.guarantor_rate')));
    }

    public function groupBalances()
    {
        return $this->hasMany('App\GroupBalance');
    }

    public function groupAdjustmentBalance($gid = null)
    {
        if(!$gid) $gid = currentUserGroup()->id;
        return $this->groupBalances->where('group_id', $gid)->sum('amount');
    }

    public function releaseReceivable()
    {
    	return $this->hasOne('App\ReleaseReceivable');
    }

    public function IsValidDeduction($amount)
    {
        return $this->balance() >= $amount;
    }

    public function balanceReceivers()
    {
        return $this->hasMany('App\BalanceReceiver');
    }
}
