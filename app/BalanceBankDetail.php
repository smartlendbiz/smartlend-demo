<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BalanceBankDetail extends Model
{
    protected $fillable = ['balance_id', 'bank_detail_id'];

    public function bankDetail()
    {
    	return $this->belongsTo('App\BankDetail');
    }

    public function balance()
    {
    	return $this->belongsTo('App\Balance');
    }

    public function cashIn()
    {
    	return $this->balance()->where('type', 1)->where('status', 'Confirmed')->sum('amount');
    }

    public function cashOut()
    {
    	return $this->balance()->where('type', 2)->where('status', 'Confirmed')->sum('amount');
    }    
}
