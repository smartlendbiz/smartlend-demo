<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Group extends Model
{
    protected $fillable = ['name', 'amount_per_share', 'grace_period', 'status', 'limit', 'started', 'processing_fee', 'year'];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function groupUsers()
    {
        return User::from('users as u')
                ->join('group_user as gu', 'gu.user_id', '=', 'u.id')
                ->where('gu.group_id', $this->id);
    }

    public function balanceList()
    {
        return $this->groupUsers()
                ->leftjoin('groups as g', 'g.id', '=', 'gu.group_id')
                ->join('balances as b', 'b.user_id', '=', 'u.id')
                ->get();
}

    public function userRoles()
    {
        return $this->hasMany('App\GroupUserRole');
    }

    public function roles($user_id)
    {
         return $this->userRoles()->where('user_id', $user_id)->pluck('role_id')->toArray();
    }

    public function isMember($user_id)
    {
    	return in_array($user_id, $this->users->pluck('id')->toArray());
    }

    public function paymentSchedules()
    {
        return $this->hasMany('App\PaymentSchedule');
    }

    public function userShares()
    {
        return $this->hasMany('App\GroupUser');
    }

    public function totalNoOfShare()
    {
        // return $this->userShares()->sum('user_share');
        return $this->contributionLists()->sum('no_of_share');
    }

    public function contributionLists()
    {
        return $this->hasMany('App\ContributionList');
    }

    public function contributionListsOngoing()
    {
        return $this->hasMany('App\ContributionList')->where('status', 'ongoing');
    }

    public function totalContributions()
    {
        // return abs($this->groupUsers()
        //        ->join('balances as b', 'b.user_id', '=', 'gu.user_id')
        //        ->join('contributions as c', 'c.balance_id', '=', 'b.id')
        //        ->where('b.status', 'Confirmed')
        //        ->where('b.type', 3)
        //        ->sum('b.amount'));

        $sum = 0;
        foreach($this->contributionListsOngoing as $contribution_list) {
            $sum += $contribution_list->totalPaid();
        }

        return $sum;
    }

    public function totalShares($user_id = null)
    {
        // if($user_id) return $this->userShares()->where('user_id', $user_id)->sum('user_share');
        // return $this->userShares()->sum('user_share');

        if($user_id) return $this->contributionListsOngoing()->where('user_id', $user_id)->sum('no_of_share');
        return $this->contributionListsOngoing()->sum('no_of_share');
    }

    public function totalGroupLatestInterest()
    {
        $regular_loan_interest = calculatePercentage($this->totalInterest(null, 'Regular'), config('interest.group_rate'));
        $guarantor_loan_interest = calculatePercentage($this->totalInterest(null, 'Guarantor'), config('interest.group_rate_guarantor'));

        return $regular_loan_interest + $guarantor_loan_interest;
    }

    public function latestDividend()
    {
        if($this->totalShares() == 0) {
            return 0;
        }
        
        return floor($this->totalGroupLatestInterest() / $this->totalShares());
    }

    public function loans()
    {
        return $this->hasMany('App\Loan');
    }

    public function releasedLoans($user_id = null, $type = null)
    {
        $loans = $this->loans()->where('status', 'Released');

        if($user_id) {
            return $loans->where('user_id', $user_id)->get();
        }

        if($type) $loans = $loans->where('type', $type);

        return $loans->get();
    }

    public function pendingLoans($user_id)
    {
        $reservation_loans = $this->reservationLoans()->where('user_id', $user_id);
        $guarantor_loans = $this->guarantorLoans()->where('user_id', $user_id);

        return $reservation_loans->merge($guarantor_loans);
    }

    public function reservationLoans()
    {
        $reservation_loans = $this->loans()->where('status', 'Pending')->orderBy('id')->get();
        $amount_for_loan = $this->amountForLoan();

        return $reservation_loans->filter(function($loan) use (&$amount_for_loan) {
            if($loan->type == 'Guarantor' && !$loan->guarantorAmountReady()) {
                return false;
            }

            $loan->ready = $amount_for_loan >= $loan->amount;
            $amount_for_loan -= $loan->amount;

            return true;
        });
    }

    public function guarantorLoans()
    {
        $guarantor_loans = $this->loans()->where('status', 'Pending')->where('type', 'Guarantor')->get();

        return $guarantor_loans->filter(function($loan) {
            $loan->ready = false;
            return !$loan->guarantorAmountReady();
        });
    }

    public function totalLoan($user_id = null, $type = null)
    {
        $loans = $this->releasedLoans($user_id, $type);
        if($user_id) {
             return $loans->where('user_id', $user_id)->sum('amount');
        }

        return $loans->sum('amount');
    }

    public function totalLoanPaid($user_id = null)
    {
        $total = 0;
        foreach ($this->releasedLoans($user_id) as $loan) {
            $total += $loan->loanPayments()->sum('amount');
        }

        return $total;
    }

    public function totalInterest($user_id = null, $type = null)
    {
        $loans = $this->releasedLoans($user_id, $type);

        $total = 0;
        foreach ($loans as $loan) {
            $total += $loan->loanInterests()->sum('amount');
        }

        return $total;
    }

    public function amountForLoan()
    {
        $total_contributions = $this->totalContributions();
        $total_loan = $this->totalLoan();
        $total_loan_paid = $this->totalLoanPaid();

        return ($total_contributions + $total_loan_paid) - $total_loan;

    }

    public function bankDetails()
    {
        return $this->hasMany('App\BankDetail');
    }

    public function userAdmins()
    {
        return $this->userRoles->where('role_id', 2);
    }

    public function usersAvailbleBalance()
    {
        return $this->groupUsers()->join('balances as b', 'b.user_id', '=', 'gu.user_id')
                    ->whereRaw("b.status = 'Confirmed' or b.status = 'Pending' and b.type=2")
                    ->sum('b.amount');
    }

    public function totalCashoutFees()
    {
        $total_cashout = abs($this->groupUsers()->join('balances as b', 'b.user_id', '=', 'gu.user_id')
                        ->where('b.type', 2)->where('b.status', 'Confirmed')->sum('b.amount'));

        return calculatePercentage($total_cashout, $this->processing_fee);
    }
}
