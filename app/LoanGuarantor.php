<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanGuarantor extends Model
{
    protected $fillable = ['loan_id', 'user_id', 'amount'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function loan()
    {
    	return $this->belongsTo('App\Loan');	
    }

    public function percentShare()
    {
    	return ($this->amount / $this->loan->amount) * 100;
    }

    public function guarantorBalance()
    {
    	if($this->loan) return calculatePercentage($this->loan->totalBalance(), $this->percentShare());
        else 0;
    }
}
