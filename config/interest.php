<?php

return [
	'rate' => 5,
	'days' => 30,
	'personal_rate' => 20,
	'guarantor_rate' => 40,
	'group_rate' => 60,
	'group_rate_guarantor' => 40,
	'system_rate' => 20,
];