<?php

return [
	'apikey' => env('SMS_APIKEY',''),
	'sender' => env('SMS_SENDER',''),
	'endpoint' => env('SMS_ENDPOINT','')
];