// SideNav Initialization
$(".button-collapse").sideNav();

var container = document.querySelector('.custom-scrollbar');
Ps.initialize(container, {
  wheelSpeed: 2,
  wheelPropagation: true,
  minScrollbarLength: 20
});

$(document).ready(function() {
	setInterval(function() {
		$('.dataTable').each(function() {
			if ($.fn.DataTable && !$.fn.DataTable.isDataTable($(this))) {
				$('.dataTable').DataTable();
			}
		})

		if($('.form-validate').length > 0) {
			$('.form-validate').validate({
				invalidHandler: function(form, validator) {
					var errors = validator.numberOfInvalids();
					if (errors) {
						$('#cover-spin').hide();
					}
				}
			})
		}

		if($('.dual_list').length > 0) {
			$('.dual_list').bootstrapDualListbox({ moveOnSelect: false });
		}

		if($('.loader-trigger').length > 0) {
			$('.loader-trigger').click(function() {
				$('#cover-spin').show();
			});
		}
	}, 100)

	$('.onDelete').on('click', function() {
		deleteConfirm($(this).parent('form'))	
	})


	$('.nav-item').on('click', function() {
		window.location.hash = $(this).find('a').attr('href')
	})	


	$('.load').each(function() {
		var self = $(this)
		loadContent(self.data('url'), self)
	})
	
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})	
})

//Function helpers
function loadContent(source, target, callback = null) {
	$(target).html('<i class="fas fa-spinner fa-spin"></i>')
	$(target).load(source, function(response, status, xhr) {
		if(callback && status == 'success') callback()
	})
}

function loadContentAjax(source, target) {
	setTimeout(function() { 
		$.ajax({
		  url: source,
		  cache: false
		}).done(function( html ) {
		    $(target).append( html );
		});	
	}, 200)
}

function setDatePicker(multidate = false) {
	$('.datepicker').datepicker({ multidate: multidate, format: 'yyyy-mm-dd' });
}

function deleteConfirm(form) {
	bootbox.confirm("Are you sure you want to delete this?", function(result) {
		if(result) form.submit()
	}); 	
}

function confirmAlert(form, message = 'Action') {
	bootbox.confirm(message, function(result) {
		if(result) {
			form.submit()
			$('#cover-spin').show();
			$('.form-validate').validate();
		}

		$('#cover-spin').hide();
	}); 	
}

function confirm(form, message = 'Action') {
	bootbox.confirm('Proceed ' + message + '?', function(result) {
		if(result) form.submit()
	}); 	
}

function setDatatable(target) {
    let set = setInterval(function() { 
      if($(target).length>0) {
      	$(target).DataTable();
      }
    }, 1)
}

function nosubmitModal(trigger) {
    $('.'+trigger).on('click',function() {
    	loadNoSubmitmodal($(this), trigger)
    }); 
}

function loadNoSubmitmodal(self, trigger) {
    var dataURL = self.attr('data-href');
    $('#'+trigger+'Modal .modal-body').load(dataURL,function(response, status, xhr) {
        $('#'+trigger+'Modal').modal('show');
    });
}