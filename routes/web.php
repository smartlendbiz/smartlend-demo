<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('main'); });

Auth::routes(['login', 'logout', 'reset']);

Route::get('register_members/{token}', 'RegisterMemberController@register');
Route::get('register_members', function() { return redirect('/'); });
Route::post('register_members', 'RegisterMemberController@submit');

Route::group(['middleware' => ['auth']], function () {

	Route::resource('/dashboard', 'DashboardController');
	Route::resource('/profile', 'ProfileController');

	Route::group(['middleware' => 'administrator'], function() {	
		Route::resource('/payment_schedules', 'PaymentScheduleController');
		Route::resource('/cash_transactions', 'CashTransactionController');  
		Route::resource('/users', 'UserController');
		Route::resource('/groups', 'GroupController');
		Route::resource('/roles', 'RoleController');
	});

	Route::group(['middleware' => 'has_group'], function() {
		Route::resource('/files', 'FileController');
		Route::resource('/cash_in', 'CashInController');
		Route::resource('/cash_out', 'CashOutController');
		// Route::resource('/contributions', 'ContributionController');  
		Route::resource('/send_funds', 'SendFundController')->only(['index','store']);  
		Route::resource('/contributions', 'ContributionListController');  
		Route::resource('/loans', 'LoanController');  
		Route::resource('/guarantor_loans', 'GuarantorLoanController');
		Route::resource('/balance_history', 'BalanceHistoryController');
		Route::post('/default_group', 'UserController@defaultGroup');
		Route::post('/register_members/generate', 'RegisterMemberController@generate');

		Route::group(['prefix' => 'manage', 'middelware' => 'group_admin'], function() {
			Route::resource('/users', 'manage\UserController');
			Route::resource('/cash_transactions', 'manage\CashTransactionController');
			Route::resource('/payment_schedules', 'manage\PaymentScheduleController');
			Route::resource('/bank_details', 'manage\BankDetailController');
			Route::resource('/group_settings', 'manage\GroupSettingController');
		});		

		Route::group(['prefix' => 'overview', 'middelware' => 'group_admin'], function() {
			Route::resource('/summaries', 'overview\SummaryController');
			Route::resource('/release_summaries', 'overview\ReleaseSummaryController');
			Route::resource('/contribution_lists', 'overview\ContributionListController');
			Route::resource('/loan_lists', 'overview\LoanListController');
			Route::resource('/balance_transactions', 'overview\BalanceTransactionController');
			Route::resource('/user_holdings', 'overview\UserHoldingController');
			Route::resource('/cash_out_fees', 'overview\CashoutFeeController');
		});				
	});
});