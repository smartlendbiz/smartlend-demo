<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBalanceBankDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balance_bank_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('balance_id');
            $table->foreign('balance_id')->references('id')->on('balances')->onDelete('cascade');
            $table->unsignedInteger('bank_detail_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balance_bank_details');
    }
}
