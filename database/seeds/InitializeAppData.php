<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\UserRole;

class InitializeAppData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createSuperAdmin();
        $this->createRoles();
        $this->attachSuperUserRole();
    }

    public function createSuperAdmin()
    {
    	$user = User::create([
    		'name' => 'Smartlend Admin', 
    		'email' => 'admin@smartlend.com', 
    		'contact_no' => '4531368012', 
    		'gender' => 'Male',
    		'password' => bcrypt('user1234'), 
    		'status' => 'Active'
    	]);

    }

    public function createRoles()
    {
    	$roles = [
    		['name' => 'Super Admin'],
    		['name' => 'Admin'],
    		['name' => 'Member']
    	];

    	foreach ($roles as $role) {
    		Role::create($role);
    	}
    }

    public function attachSuperUserRole() 
    {
    	$user = User::where('email', 'admin@smartlend.com')->first();
    	$role = Role::where('name', 'Super Admin')->first();

    	$user->roles()->attach($role->id);
    }
}
